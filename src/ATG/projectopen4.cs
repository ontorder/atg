namespace AtgCore
{
    public class projectopen4 {
        public class Pages{
            public class _None : Pages{}
            public class _Login : Pages{}

            public abstract class DaystampPages : Pages{ public DateTime Daystamp; }

            public class _Calendar : DaystampPages{}

            public class _TimesheetHours : DaystampPages{}

            public class _TimesheetHoursProject : DaystampPages{ public int ProjectId; }

            static public _None                  None                  = new _None();
            static public _Login                 Login                 = new _Login();
            static public _Calendar              Calendar              = new _Calendar();
            static public _TimesheetHours        TimesheetHours        = new _TimesheetHours();
            static public _TimesheetHoursProject TimesheetHoursProject = new _TimesheetHoursProject();
        }

        public class configuration{
            public string user;
            public string pass;
            public string addr;
            public controlbrowser cb;
        }

        private configuration config;
        private Pages currentPage = Pages.None;
        private AutoResetEvent mtx = new AutoResetEvent(false);
        private int UserId;

        public Action<EventBase> Events;

        // ----------------

        public projectopen4(configuration pCfg) {
            config = pCfg;
            config.cb.AddNavigatedCallback(NavigatedCallback);
        }

        public void Stop() => mtx.Set();

        public void put_ora(action_put_ora_projectopen apo){
            string __method = $"[{nameof(projectopen4)}.{nameof(put_ora)}]";

            var inputs = LoadPageTimesheetHours(apo.giorno, apo.progetto);
            if (inputs == null)
                throw new Exception($"{__method} progetto {apo.progetto} non trovato");

            var input_riunioni_ore = GetTimesheetHoursInputDurata(inputs, apo.progetto);
            input_riunioni_ore.SetText(apo.durata.TotalHours.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture));

            var input_riunioni_desc = GetTimesheetHoursInputNota(inputs, apo.progetto);
            if(string.IsNullOrEmpty(apo.nota))
            {
                apo.nota = input_riunioni_desc.GetText();
                if(string.IsNullOrEmpty(apo.nota))
                    apo.nota = "(nessuna nota)";
            }
            input_riunioni_desc.SetText(apo.nota);

            //var id_riunioni_intr = "internal_notes0." + apo.progetto;
            //var input_riunioni_intr = inputs.GetFirstByName(id_riunioni_intr);
            //input_riunioni_intr.SetText("test internal");

            var submit = inputs.Last();
            submit.Click();
            mtx.WaitOne();

            Events(new EventPutHour(apo.progetto, apo.giorno, apo.durata, apo.nota));
        }

        public void put_commento(action_put_commento_projectopen apc){
            string __method = $"[{nameof(projectopen4)}.{nameof(put_commento)}]";

            var inputs = LoadPageTimesheetHours(apc.giorno, apc.progetto);
            if (inputs == null)
                throw new Exception($"{__method} progetto {apc.progetto} non trovato");

            var input_riunioni_desc = GetTimesheetHoursInputNota(inputs, apc.progetto);
            if(string.IsNullOrEmpty(apc.nota))
                apc.nota = "(nessuna nota)";
            input_riunioni_desc.SetText(apc.nota);

            //var id_riunioni_intr = "internal_notes0." + apo.progetto;
            //var input_riunioni_intr = inputs.GetFirstByName(id_riunioni_intr);
            //input_riunioni_intr.SetText("test internal");

            var submit = inputs.Last();
            submit.Click();
            mtx.WaitOne();

            Events(new EventPutComment(apc.progetto, apc.giorno, apc.nota));
        }

        private controlbrowser.ElementsCollection GetTimesheetHoursInputs() =>
            config.cb.GetElementsByTagName("input");

        private controlbrowser.DomElement GetTimesheetHoursInputDurata(controlbrowser.ElementsCollection inputs, int id) =>
            inputs.GetFirstByName("hours0." + id);

        private controlbrowser.DomElement GetTimesheetHoursInputNota(controlbrowser.ElementsCollection inputs, int id) =>
            inputs.GetFirstByName("notes0." + id);

        private string GetUrl(Pages forPage){
            switch (forPage){
                case Pages._Login    _: return Combine(config.addr, "register");
                case Pages._Calendar c: return GetTimesheetUrl(c.Daystamp);
                case Pages._TimesheetHours th: return GetTimesheetHoursUrl(JulianDate(th.Daystamp));
                case Pages._TimesheetHoursProject thp: return GetTimesheetHoursProjectUrl(JulianDate(thp.Daystamp), thp.ProjectId);
                default: throw new Exception("what");
            }
        }

        private controlbrowser.ElementsCollection LoadPageTimesheetHours(DateTime pDay, int projectId){
            ReachPage(new Pages._TimesheetHours(){ Daystamp = pDay });

            // controllare se ci sono textbox per projectId
            // se no selezionare da combo e navigare alla pagina

            var inputs = GetTimesheetHoursInputs();
            var input_durata = GetTimesheetHoursInputDurata(inputs, projectId);
            if(input_durata != null)
                return inputs;

            ReachPage(new Pages._TimesheetHoursProject(){ ProjectId = projectId, Daystamp = pDay });

            inputs = GetTimesheetHoursInputs();
            input_durata = GetTimesheetHoursInputDurata(inputs, projectId);
            return input_durata != null ? inputs : null;
        }

        private string GetTimesheetHoursProjectUrl(int julianTimestamp, int projectId) =>
            Combine(config.addr, $"intranet-timesheet2/hours/new?julian_date={julianTimestamp}&show_week_p=0&project_id={projectId}");

        private bool isLogin() => ! config.cb.GetElementById("login").IsNull();

        private int JulianDate(DateTime dt){
            const int julian2000 = 2451545;
            var ori = new DateTime(2000, 1, 1);
            var jul = dt.Subtract(ori).TotalDays;
            return (int) Math.Truncate(jul) + julian2000;
        }

        private string Combine(params string[] crumbs){
            var frags = crumbs.Select(crumb => crumb.Trim('/'));
            return string.Join("/", frags);
        }

        private string GetTimesheetHoursUrl(int julianTimestamp) =>
            Combine(config.addr, "intranet-timesheet2/hours/new?show_week_p=&julian_date=" + julianTimestamp);

        private string GetTimesheetUrl(DateTime start){
            string day = start.ToShortDateString();
            return Combine(config.addr, "intranet-timesheet2/hours/index?date=2019-03-22");
        }

        private bool PagesMatch(Pages page1, Pages page2){
            if (! Type.Equals(page1, page2))
                return false;

            switch(page1){
                case Pages._None     _: return false;
                case Pages._Login    _: return true;
                case Pages._Calendar c: return c.Daystamp == ((Pages._Calendar)page2).Daystamp;
                case Pages._TimesheetHours th: return th.Daystamp == ((Pages._TimesheetHours)page2).Daystamp;
                case Pages._TimesheetHoursProject thp:{
                    var thp2 = (Pages._TimesheetHoursProject)page2;
                    return thp.Daystamp == thp2.Daystamp && thp.ProjectId == thp2.ProjectId;
                }
                default: throw new Exception("wtf");
            }
        }

        public void IsLoginFailed(){
            var divs = config.cb.GetElementsByTagName("div");
            var isErr = divs.GetFirstByName("form-error");
            if (isErr != null)
                throw new Exception($"[projectopen4.IsLoginFailed] login fail: '{isErr.GetText()}'");

            var user_settings = config.cb.GetElementById("header_settings");
            var a_profile = user_settings.GetChildren()[0];
            var href = a_profile.GetAttribute("href");
            var id = href.Substring(href.LastIndexOf('=') + 1);
            UserId = int.Parse(id);
        }

        private void ReachPage(Pages pPage){
            if (PagesMatch(currentPage, pPage)){
                config.cb.Refresh();
                mtx.WaitOne();
            }

            if (currentPage is Pages._None || currentPage is Pages._Login){
                Login();
                IsLoginFailed();
                Events(new EventLoggedId(UserId));
            }

            var url = GetUrl(pPage);
            SyncNavigate(url);

            if (isLogin()){
                if (
                    ! ( currentPage is Pages._None )
                    &&
                    ! ( currentPage is Pages._Login )
                )
                    Events(new EventSessionEnded());

                Login();
                IsLoginFailed();
                Events(new EventLoggedId(UserId));

                SyncNavigate(url);
            }

            currentPage = pPage;
            Events(new EventNavigatedTo(currentPage));
        }

        private void SyncNavigate(string url){
            mtx.Reset();
            config.cb.Navigate(url);
            mtx.WaitOne();
        }

        private void NavigatedCallback() => mtx.Set();

        private void Login()
        {
            mtx.Reset();
            var txt_email = config.cb.GetElementById("email");
            var txt_pass = config.cb.GetElementById("password");
            var btn_ok = config.cb.GetElementsByTagName("input").GetFirstByName("formbutton:ok");

            txt_email.SetText(config.user);
            txt_pass.SetText(config.pass);
            btn_ok.Click();
            mtx.WaitOne();
        }

        // ---------

        public class EventBase{}

        public class EventNavigatedTo : EventBase{
            public Pages Page;
            public EventNavigatedTo(Pages pPage) => Page = pPage;
        }

        public class EventLoggedId : EventBase{
            public int UserId;
            public EventLoggedId(int pUserId) => UserId = pUserId;
        }

        public class EventSessionEnded : EventBase{}

        public class EventPutHour : EventBase{
            public TimeSpan Durata;
            public int ProgettoId;
            public string Nota;
            public DateTime Giorno;

            public EventPutHour(int pProgettoId, DateTime pGiorno, TimeSpan pDurata, string pNota){
                ProgettoId = pProgettoId;
                Durata = pDurata;
                Nota = pNota;
                Giorno = pGiorno;
            }
        }

        public class EventPutComment : EventBase{
            public int ProgettoId;
            public string Nota;
            public DateTime Giorno;

            public EventPutComment(int pProgettoId, DateTime pGiorno, string pNota){
                ProgettoId = pProgettoId;
                Nota = pNota;
                Giorno = pGiorno;
            }
        }
    }
}
