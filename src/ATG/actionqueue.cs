using System.Collections;

namespace AtgCore
{
    public class actionqueue {
        private Queue actions = new Queue();
        private AutoResetEvent trigger = new AutoResetEvent(false);
        private bool running = true;
        private Thread call_thread;
        public Action<object> queue_event;
        public Action<string, bool> Log;

        // --------------

        public actionqueue(){
            call_thread = new Thread(consume_thread);
            call_thread.Start();
        }

        public void enqueue(object action){
            lock(actions)
                actions.Enqueue(action);
            trigger.Set();
        }

        private void consume_thread()
        {
            int queue_len;
            object action;

            wait:{
                lock(actions)
                    queue_len = actions.Count;

                if(queue_len == 0)
                    trigger.WaitOne();

                action = null;
                lock(actions){
                    if(actions.Count > 0)
                        action = actions.Dequeue();
                    else
                        if(running)
                            goto wait;
                }

                if(action != null)
                    try{
                        queue_event(action);
                    }
                    catch(Exception e){
                        var err = exception_logger.recursive(e);
                        Log(err, true);
                    }

                if(running)
                    goto wait;
            }
        }

        public void Stop() {
            running = false;
            call_thread = null;
            trigger.Set();
        }
    }
}
