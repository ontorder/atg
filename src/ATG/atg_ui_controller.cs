﻿namespace AtgCore
{
    public class atg_ui_controller {
        private atg_form form;
        public atg_controller atg;
        public logger logger;

        public atg_ui_controller() {
        }

        public void add_form(atg_form p_form){
            var x = new atg_form_controller(null, p_form, null, null);
            x.set_commands();
        }

        public void ingresso_eventi(object eventoBase)
        {
            dispatch(eventoBase);
        }

        private void dispatch(object @event)
        {
            //atg.evento_termini_handler.Add(raised_event_termini);
            //atg.evento_gerarchie_handler.Add(raised_event_gerarchie);
            //atg.evento_gerarchie_preferite_handler.Add(raised_event_gerarchie_preferite);
            //atg.evento_filter_handler.Add(event_filter_change);
            //atg.evento_load_default_gerarchie_handler.Add(raised_event_default_gerarchie_loaded);
            //
            //var tp = atg.get_active_timerpage();
            //tp.eventhandler_add_timerbar.Add(raised_event_added_timerbar);
            //tp.eventhandler_del_timerbar.Add(raised_event_delete_timerbar);
            //tp.eventhandler_clean_selection.Add(raised_event_clean_selection);
            //tp.eventhandler_status.Add(raised_event_timerpage_status);
            //
            //atg.evento_load_user.Add(event_user_data);
        }
    }
}
