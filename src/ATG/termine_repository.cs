﻿namespace AtgCore
{
    public class termine_repository
    {
        private atg_context _ctx;
        public bool is_modified;

        public termine_repository(atg_context driver)
        {
            _ctx = driver;
        }

        public async Task<termine_ctx[]> get_termini_async()
        {
            var x = await _ctx.termini.get_async();
            return x.ToArray();
        }

        public async Task aggiungi_termine_async(int id, string nome)
            => await _ctx.termini.add_async(new termine_ctx(id, nome));

        private async Task<int> termini_primo_id_libero_async()
        {
            int x;

            var list_termini = await _ctx.termini.get_async();
            var termini = new HashSet<string>(list_termini.Select(termine => termine.nome));
            var list_id_term = new List<int>(list_termini.Select(termine => termine.termine_id));

            if (list_id_term.Count == 0)
                return 1;

            x = 1;
            while (
                x < list_id_term.Count
                &&
                // se c'è un gap >1 tra due termini allora c'è spazio da completare
                (list_id_term[x] - list_id_term[x - 1]) == 1
            )
            {
                ++x;
            }

            if (x == list_id_term.Count)
                return list_id_term[x - 1] + 1;
            else
                return list_id_term[x] - 1;
        }

        public async Task inserisci_termini_non_esistenti_async(string[] gerarchia_test)
        {
            var list_termini = await _ctx.termini.get_async();
            var termini = new HashSet<string>(list_termini.Select(termine => termine.nome));

            // c'è un ciclo, quindi servirebbe un lock...
            foreach (string termine in gerarchia_test)
            {
                if (!termini.Contains(termine))
                {
                    // questo non dovrebbe venir deciso qui
                    int id = await termini_primo_id_libero_async();

                    await aggiungi_termine_async(id, termine);

                    // questo non sarebbe corretto, ad esempio
                    termini.Add(termine);
                }
            }
        }
    }
}
