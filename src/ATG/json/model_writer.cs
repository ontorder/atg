﻿namespace AtgCore
{
    public class model_writer
    {
        List<Tuple<simpletype_types, object>> simple_model;

        public model_writer() => simple_model = new List<Tuple<simpletype_types, object>>();

        public List<Tuple<simpletype_types, object>> get_root_object() => simple_model;

        public List<Tuple<simpletype_types, object>> create_object(List<Tuple<simpletype_types, object>> father, string name)
        {
            var new_obj = new simpletype_object(name);

            father.Add(new Tuple<simpletype_types, object>(simpletype_types.@object, new_obj));

            return new_obj.childs;
        }

        public List<Tuple<simpletype_types, object>> create_array(List<Tuple<simpletype_types, object>> father, string field_name)
        {
            var new_array = new simpletype_array(field_name);

            father.Add(new Tuple<simpletype_types, object>(simpletype_types.array, new_array));

            return new_array.childs;
        }

        public List<Tuple<simpletype_types, object>> create_root_array(List<Tuple<simpletype_types, object>> father)
        {
            var new_array = new simpletype_array(null);

            father.Add(new Tuple<simpletype_types, object>(simpletype_types.base_array, new_array));

            return new_array.childs;
        }

        public void add_field(List<Tuple<simpletype_types, object>> father, string name)
        {
            var new_value = new simpletype_value(name);

            father.Add(new Tuple<simpletype_types, object>(simpletype_types.value, new_value));
        }

        public string write(structured_value_writer svw, object source)
        {
            var start_obj = svw.start_write();

            recursive_write(svw, start_obj, simple_model, source);

            return svw.end_write();
        }

        private void recursive_write(structured_value_writer svw, object svw_item, List<Tuple<simpletype_types, object>> model_father, object source)
        {
            foreach (var model_item in model_father)
            {
                switch (model_item.Item1)
                {
                    case simpletype_types.base_array:
                    {
                        var decoded = (simpletype_array)model_item.Item2;
                        var new_arr = svw.create_array(svw_item, null);

                        var obj_arr = (Array)source;
                        var arr_len = obj_arr.Length;

                        for (int i = 0; i < arr_len; ++i)
                        {
                            recursive_write(svw, new_arr, decoded.childs, obj_arr.GetValue(i));
                        }
                        break;
                    }

                    case simpletype_types.array:
                    {
                        var decoded = (simpletype_array)model_item.Item2;
                        var new_arr = svw.create_array(svw_item, decoded.name);

                        var field = source.GetType().GetField(decoded.name).GetValue(source);
                        var obj_arr = (Array)field;
                        var arr_len = obj_arr.Length;

                        for (int i = 0; i < arr_len; ++i)
                        {
                            recursive_write(svw, new_arr, decoded.childs, obj_arr.GetValue(i));
                        }
                        break;
                    }

                    case simpletype_types.@object:
                    {
                        var decoded = (simpletype_object)model_item.Item2;
                        var new_obj = svw.create_object(svw_item);

                        recursive_write(svw, new_obj, decoded.childs, source);
                        break;
                    }

                    case simpletype_types.value:
                    {
                        var decode = (simpletype_value)model_item.Item2;
                        var value = source.GetType().GetField(decode.name).GetValue(source);

                        svw.write_value(svw_item, decode.name, value);
                        break;
                    }
                }
            }
        }
    }
}
