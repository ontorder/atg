﻿namespace AtgCore;

public enum simpletype_types {
    @base_array,
    @array,
    @value,
    @object
}
