﻿using seritem = System.Tuple<int, string, object>;
using sertype = System.Collections.Generic.List<System.Tuple<int, string, object>>;

namespace AtgCore
{
    public class json_fuckin_writer : structured_value_writer
    {
        enum types { @value, @object, @array }
        // questo dovrebbe essere un seritem, come nelle classi precedenti
        // ma Tuple è di sola lettura :(
        sertype data;

        // ----------------- //

        public json_fuckin_writer() => data = new sertype();

        public object start_write() => data;

        public object create_object(object father)
        {
            var interpret = (sertype)father;
            var childs = new sertype();
            var new_obj = new seritem((int)types.@object, null, childs);

            interpret.Add(new_obj);
            return childs;
        }

        public object create_array(object father, string field_name)
        {
            var interpret = (sertype)father;
            var childs = new sertype();
            var new_obj = new seritem((int)types.array, field_name, childs);

            interpret.Add(new_obj);
            return childs;
        }

        public void write_value(object father, string name, object value)
        {
            var interpret = (sertype)father;
            var obj_value = new seritem((int)types.value, name, value.ToString());

            interpret.Add(obj_value);
        }

        public string end_write()
        {
            var res = serialize_list(data, null);

            return string.Concat(res);
        }

        private string escape_json_pair_string(string name) => name;

        private string[] serialize_list(sertype local_object, types? parent_type)
        {
            var str_out = new List<string>();

            // sbagliato
            foreach (var item in local_object)
            {
                str_out.AddRange(serialize_value(item, parent_type));
            }

            return str_out.ToArray();
        }

        private string[] serialize_value(seritem local_item, types? parent_type)
        {
            var str_out = new List<string>();

            switch ((types)local_item.Item1)
            {
                case types.array:
                {
                    // a cazzo: array non passa per value, quindi bisogna aggiungere il nome a mano
                    if (parent_type.HasValue)
                        if (parent_type.Value == types.@object)
                        {
                            if (string.IsNullOrEmpty(local_item.Item2))
                                throw new Exception("missing pair name");

                            var name_esc = escape_json_pair_string(local_item.Item2);
                            str_out.Add($"\"{name_esc}\":");
                        }

                    str_out.Add("[");

                    var local_array = (sertype)local_item.Item3;
                    var count = local_array.Count;
                    for (var i = 0; i < count; ++i)
                    {
                        var arr_item = local_array[i];

                        str_out.AddRange(serialize_value(arr_item, types.array));
                        if (i < (count - 1))
                            str_out.Add(",");
                    }
                    str_out.Add("]");
                    break;
                }
                case types.@object:
                {
                    str_out.Add("{");

                    var new_object = (sertype)local_item.Item3;
                    var count = new_object.Count;
                    for (var i = 0; i < count; ++i)
                    {
                        var pair = new_object[i];

                        str_out.AddRange(serialize_value(pair, types.@object));
                        if (i < (count - 1))
                            str_out.Add(",");
                    }

                    str_out.Add("}");
                    break;
                }
                case types.value:
                {
                    if (!parent_type.HasValue || parent_type.Value == types.array)
                    {
                        if (!string.IsNullOrEmpty(local_item.Item2))
                            throw new Exception("field name in array item");

                        var esc_value = escape_json_pair_string(local_item.Item3.ToString());
                        str_out.Add($"\"{esc_value}\"");
                    }
                    else
                    if (parent_type.HasValue && parent_type.Value == types.@object)
                    {
                        if (string.IsNullOrEmpty(local_item.Item2))
                            throw new Exception("missing field name in object member");

                        var esc_value = escape_json_pair_string(local_item.Item3.ToString());
                        var esc_name = escape_json_pair_string(local_item.Item2);
                        str_out.Add($"\"{esc_name}\":\"{esc_value}\"");
                    }
                    else
                        throw new Exception("wtf");
                    break;
                }
            }

            return str_out.ToArray();
        }
    }
}
