﻿namespace AtgCore;

public interface structured_value_writer {
    object start_write();
    object create_object(object father);
    object create_array(object father, string field_name);
    void write_value(object father, string name, object value);
    string end_write();
}
