﻿namespace AtgCore;

public class simpletype_array {
    public string name;
    public List<Tuple<simpletype_types, object>> childs = new();

    public simpletype_array(string field_name) => name = field_name;
}
