﻿namespace AtgCore
{
    partial class atg_form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.flp_main = new System.Windows.Forms.FlowLayoutPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TbAutoreToolStripMenuItem = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.ricaricaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.timerDiDefaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allarmiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertiGerarchiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.layoutManualeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.istanzeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aggiungiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gerarchiaVuotaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.eliminaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sommeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commentiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.progettoSingoloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ss_main = new System.Windows.Forms.StatusStrip();
            this.tss_status = new System.Windows.Forms.ToolStripStatusLabel();
            this.tss_filtertot = new System.Windows.Forms.ToolStripStatusLabel();
            this.cms_flp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.aggiungiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.annullaSelezioneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmr_tot = new System.Windows.Forms.Timer(this.components);
            this.sc_main = new System.Windows.Forms.SplitContainer();
            this.btnFiltra = new System.Windows.Forms.Button();
            this.mt_ora_inizio = new System.Windows.Forms.MaskedTextBox();
            this.mt_ora_fine = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_fine = new System.Windows.Forms.DateTimePicker();
            this.dtp_inizio = new System.Windows.Forms.DateTimePicker();
            this.tc_main = new System.Windows.Forms.TabControl();
            this.tp_flp_1 = new System.Windows.Forms.TabPage();
            this.tp_flp_2 = new System.Windows.Forms.TabPage();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.ss_main.SuspendLayout();
            this.cms_flp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sc_main)).BeginInit();
            this.sc_main.Panel1.SuspendLayout();
            this.sc_main.Panel2.SuspendLayout();
            this.sc_main.SuspendLayout();
            this.tc_main.SuspendLayout();
            this.tp_flp_1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flp_main
            // 
            this.flp_main.BackColor = System.Drawing.SystemColors.Control;
            this.flp_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flp_main.Location = new System.Drawing.Point(3, 3);
            this.flp_main.Name = "flp_main";
            this.flp_main.Size = new System.Drawing.Size(737, 316);
            this.flp_main.TabIndex = 0;
            this.flp_main.MouseClick += new System.Windows.Forms.MouseEventHandler(this.flp_main_MouseClick);
            this.flp_main.MouseMove += new System.Windows.Forms.MouseEventHandler(this.flp_main_MouseMove);
            this.flp_main.MouseUp += new System.Windows.Forms.MouseEventHandler(this.flp_main_MouseUp);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.istanzeToolStripMenuItem,
            this.aggiungiToolStripMenuItem,
            this.eliminaToolStripMenuItem,
            this.reportToolStripMenuItem,
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(751, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoreToolStripMenuItem,
            this.toolStripMenuItem2,
            this.ricaricaToolStripMenuItem,
            this.toolStripMenuItem1,
            this.timerDiDefaultToolStripMenuItem,
            this.allarmiToolStripMenuItem,
            this.convertiGerarchiaToolStripMenuItem,
            this.layoutManualeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.fileToolStripMenuItem.Text = "Configurazione";
            // 
            // autoreToolStripMenuItem
            // 
            this.autoreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TbAutoreToolStripMenuItem});
            this.autoreToolStripMenuItem.Name = "autoreToolStripMenuItem";
            this.autoreToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.autoreToolStripMenuItem.Text = "Autore";
            // 
            // TbAutoreToolStripMenuItem
            // 
            this.TbAutoreToolStripMenuItem.Name = "TbAutoreToolStripMenuItem";
            this.TbAutoreToolStripMenuItem.Size = new System.Drawing.Size(152, 23);
            this.TbAutoreToolStripMenuItem.Text = "cianil";
            this.TbAutoreToolStripMenuItem.Validated += new System.EventHandler(this.TbAutoreToolStripMenuItem_Validated);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(169, 6);
            // 
            // ricaricaToolStripMenuItem
            // 
            this.ricaricaToolStripMenuItem.Name = "ricaricaToolStripMenuItem";
            this.ricaricaToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.ricaricaToolStripMenuItem.Text = "Ricarica";
            this.ricaricaToolStripMenuItem.Click += new System.EventHandler(this.ricaricaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(169, 6);
            // 
            // timerDiDefaultToolStripMenuItem
            // 
            this.timerDiDefaultToolStripMenuItem.Name = "timerDiDefaultToolStripMenuItem";
            this.timerDiDefaultToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.timerDiDefaultToolStripMenuItem.Text = "Timer di default";
            this.timerDiDefaultToolStripMenuItem.Click += new System.EventHandler(this.timerDiDefaultToolStripMenuItem_Click);
            // 
            // allarmiToolStripMenuItem
            // 
            this.allarmiToolStripMenuItem.Name = "allarmiToolStripMenuItem";
            this.allarmiToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.allarmiToolStripMenuItem.Text = "Allarmi";
            // 
            // convertiGerarchiaToolStripMenuItem
            // 
            this.convertiGerarchiaToolStripMenuItem.Name = "convertiGerarchiaToolStripMenuItem";
            this.convertiGerarchiaToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.convertiGerarchiaToolStripMenuItem.Text = "Converti Gerarchia";
            // 
            // layoutManualeToolStripMenuItem
            // 
            this.layoutManualeToolStripMenuItem.Name = "layoutManualeToolStripMenuItem";
            this.layoutManualeToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.layoutManualeToolStripMenuItem.Text = "Layout Manuale";
            this.layoutManualeToolStripMenuItem.Click += new System.EventHandler(this.layoutManualeToolStripMenuItem_Click);
            // 
            // istanzeToolStripMenuItem
            // 
            this.istanzeToolStripMenuItem.Name = "istanzeToolStripMenuItem";
            this.istanzeToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.istanzeToolStripMenuItem.Text = "Istanze";
            // 
            // aggiungiToolStripMenuItem
            // 
            this.aggiungiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gerarchiaVuotaToolStripMenuItem,
            this.toolStripMenuItem3});
            this.aggiungiToolStripMenuItem.Name = "aggiungiToolStripMenuItem";
            this.aggiungiToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.aggiungiToolStripMenuItem.Text = "Aggiungi";
            this.aggiungiToolStripMenuItem.Click += new System.EventHandler(this.aggiungiToolStripMenuItem_Click);
            // 
            // gerarchiaVuotaToolStripMenuItem
            // 
            this.gerarchiaVuotaToolStripMenuItem.Name = "gerarchiaVuotaToolStripMenuItem";
            this.gerarchiaVuotaToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.gerarchiaVuotaToolStripMenuItem.Text = "Gerarchia Vuota";
            this.gerarchiaVuotaToolStripMenuItem.Click += new System.EventHandler(this.gerarchiaVuotaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(155, 6);
            // 
            // eliminaToolStripMenuItem
            // 
            this.eliminaToolStripMenuItem.Name = "eliminaToolStripMenuItem";
            this.eliminaToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.eliminaToolStripMenuItem.Text = "Elimina";
            this.eliminaToolStripMenuItem.Click += new System.EventHandler(this.eliminaToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sommeToolStripMenuItem,
            this.commentiToolStripMenuItem,
            this.progettoSingoloToolStripMenuItem});
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.reportToolStripMenuItem.Text = "Report";
            // 
            // sommeToolStripMenuItem
            // 
            this.sommeToolStripMenuItem.Name = "sommeToolStripMenuItem";
            this.sommeToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.sommeToolStripMenuItem.Text = "Tot per Gerarchia";
            this.sommeToolStripMenuItem.ToolTipText = "somma di tutti gli intervalli per gerarchia";
            this.sommeToolStripMenuItem.Click += new System.EventHandler(this.sommeToolStripMenuItem_Click);
            // 
            // commentiToolStripMenuItem
            // 
            this.commentiToolStripMenuItem.Name = "commentiToolStripMenuItem";
            this.commentiToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.commentiToolStripMenuItem.Text = "Intervalli Espliciti";
            this.commentiToolStripMenuItem.ToolTipText = "mostra ogni intervallo col suo commento";
            this.commentiToolStripMenuItem.Click += new System.EventHandler(this.commentiToolStripMenuItem_Click);
            // 
            // progettoSingoloToolStripMenuItem
            // 
            this.progettoSingoloToolStripMenuItem.Name = "progettoSingoloToolStripMenuItem";
            this.progettoSingoloToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.progettoSingoloToolStripMenuItem.Text = "Progetto Singolo";
            this.progettoSingoloToolStripMenuItem.ToolTipText = "eh... grafico? ma si potrebbe in realtà fare anche per più progetti assieme";
            this.progettoSingoloToolStripMenuItem.Click += new System.EventHandler(this.progettoSingoloToolStripMenuItem_Click);
            // 
            // ss_main
            // 
            this.ss_main.AllowMerge = false;
            this.ss_main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tss_status,
            this.tss_filtertot});
            this.ss_main.Location = new System.Drawing.Point(0, 399);
            this.ss_main.Name = "ss_main";
            this.ss_main.Size = new System.Drawing.Size(751, 24);
            this.ss_main.TabIndex = 2;
            this.ss_main.Text = "*";
            // 
            // tss_status
            // 
            this.tss_status.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tss_status.Name = "tss_status";
            this.tss_status.Size = new System.Drawing.Size(654, 19);
            this.tss_status.Spring = true;
            this.tss_status.Text = "Benvenuti";
            this.tss_status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tss_filtertot
            // 
            this.tss_filtertot.AutoSize = false;
            this.tss_filtertot.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tss_filtertot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tss_filtertot.Name = "tss_filtertot";
            this.tss_filtertot.Size = new System.Drawing.Size(82, 19);
            this.tss_filtertot.Text = "00:00 / 00.000";
            // 
            // cms_flp
            // 
            this.cms_flp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aggiungiToolStripMenuItem1,
            this.annullaSelezioneToolStripMenuItem});
            this.cms_flp.Name = "cms_flp";
            this.cms_flp.Size = new System.Drawing.Size(168, 48);
            // 
            // aggiungiToolStripMenuItem1
            // 
            this.aggiungiToolStripMenuItem1.Name = "aggiungiToolStripMenuItem1";
            this.aggiungiToolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
            this.aggiungiToolStripMenuItem1.Text = "Aggiungi";
            this.aggiungiToolStripMenuItem1.Click += new System.EventHandler(this.aggiungiToolStripMenuItem1_Click);
            // 
            // annullaSelezioneToolStripMenuItem
            // 
            this.annullaSelezioneToolStripMenuItem.Name = "annullaSelezioneToolStripMenuItem";
            this.annullaSelezioneToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.annullaSelezioneToolStripMenuItem.Text = "Annulla Selezione";
            this.annullaSelezioneToolStripMenuItem.Click += new System.EventHandler(this.annullaSelezioneToolStripMenuItem_Click);
            // 
            // tmr_tot
            // 
            this.tmr_tot.Interval = 10000;
            // 
            // sc_main
            // 
            this.sc_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sc_main.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.sc_main.IsSplitterFixed = true;
            this.sc_main.Location = new System.Drawing.Point(0, 24);
            this.sc_main.Name = "sc_main";
            this.sc_main.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // sc_main.Panel1
            // 
            this.sc_main.Panel1.Controls.Add(this.btnFiltra);
            this.sc_main.Panel1.Controls.Add(this.mt_ora_inizio);
            this.sc_main.Panel1.Controls.Add(this.mt_ora_fine);
            this.sc_main.Panel1.Controls.Add(this.label2);
            this.sc_main.Panel1.Controls.Add(this.label1);
            this.sc_main.Panel1.Controls.Add(this.dtp_fine);
            this.sc_main.Panel1.Controls.Add(this.dtp_inizio);
            // 
            // sc_main.Panel2
            // 
            this.sc_main.Panel2.Controls.Add(this.tc_main);
            this.sc_main.Size = new System.Drawing.Size(751, 375);
            this.sc_main.SplitterDistance = 26;
            this.sc_main.SplitterWidth = 1;
            this.sc_main.TabIndex = 3;
            // 
            // btnFiltra
            // 
            this.btnFiltra.Location = new System.Drawing.Point(467, 2);
            this.btnFiltra.Name = "btnFiltra";
            this.btnFiltra.Size = new System.Drawing.Size(39, 21);
            this.btnFiltra.TabIndex = 13;
            this.btnFiltra.Text = "Filtra";
            this.btnFiltra.UseVisualStyleBackColor = true;
            this.btnFiltra.Click += new System.EventHandler(this.btnFiltra_ClickAsync);
            // 
            // mt_ora_inizio
            // 
            this.mt_ora_inizio.Location = new System.Drawing.Point(198, 3);
            this.mt_ora_inizio.Mask = "00:00";
            this.mt_ora_inizio.Name = "mt_ora_inizio";
            this.mt_ora_inizio.Size = new System.Drawing.Size(34, 20);
            this.mt_ora_inizio.TabIndex = 12;
            // 
            // mt_ora_fine
            // 
            this.mt_ora_fine.Location = new System.Drawing.Point(427, 3);
            this.mt_ora_fine.Mask = "00:00";
            this.mt_ora_fine.Name = "mt_ora_fine";
            this.mt_ora_fine.Size = new System.Drawing.Size(34, 20);
            this.mt_ora_fine.TabIndex = 11;
            this.mt_ora_fine.ValidatingType = typeof(System.DateTime);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(239, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "A";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Da";
            // 
            // dtp_fine
            // 
            this.dtp_fine.CustomFormat = "";
            this.dtp_fine.Location = new System.Drawing.Point(259, 3);
            this.dtp_fine.Name = "dtp_fine";
            this.dtp_fine.Size = new System.Drawing.Size(162, 20);
            this.dtp_fine.TabIndex = 8;
            // 
            // dtp_inizio
            // 
            this.dtp_inizio.Location = new System.Drawing.Point(30, 3);
            this.dtp_inizio.Name = "dtp_inizio";
            this.dtp_inizio.Size = new System.Drawing.Size(162, 20);
            this.dtp_inizio.TabIndex = 7;
            // 
            // tc_main
            // 
            this.tc_main.Controls.Add(this.tp_flp_1);
            this.tc_main.Controls.Add(this.tp_flp_2);
            this.tc_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tc_main.Location = new System.Drawing.Point(0, 0);
            this.tc_main.Name = "tc_main";
            this.tc_main.SelectedIndex = 0;
            this.tc_main.Size = new System.Drawing.Size(751, 348);
            this.tc_main.TabIndex = 0;
            // 
            // tp_flp_1
            // 
            this.tp_flp_1.Controls.Add(this.flp_main);
            this.tp_flp_1.Location = new System.Drawing.Point(4, 22);
            this.tp_flp_1.Name = "tp_flp_1";
            this.tp_flp_1.Padding = new System.Windows.Forms.Padding(3);
            this.tp_flp_1.Size = new System.Drawing.Size(743, 322);
            this.tp_flp_1.TabIndex = 0;
            this.tp_flp_1.Text = "Principale";
            this.tp_flp_1.UseVisualStyleBackColor = true;
            // 
            // tp_flp_2
            // 
            this.tp_flp_2.Location = new System.Drawing.Point(4, 22);
            this.tp_flp_2.Name = "tp_flp_2";
            this.tp_flp_2.Padding = new System.Windows.Forms.Padding(3);
            this.tp_flp_2.Size = new System.Drawing.Size(743, 322);
            this.tp_flp_2.TabIndex = 1;
            this.tp_flp_2.Text = "-";
            this.tp_flp_2.UseVisualStyleBackColor = true;
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.undoToolStripMenuItem.Text = "Undo";
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.redoToolStripMenuItem.Text = "Redo";
            // 
            // atg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 423);
            this.Controls.Add(this.sc_main);
            this.Controls.Add(this.ss_main);
            this.Controls.Add(this.menuStrip1);
            this.MaximizeBox = false;
            this.Name = "atg";
            this.Text = "A+T+G";
            this.Deactivate += new System.EventHandler(this.frm_atg_Deactivate_Async);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_atg_FormClosingAsync);
            this.Load += new System.EventHandler(this.FormEvent_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ss_main.ResumeLayout(false);
            this.ss_main.PerformLayout();
            this.cms_flp.ResumeLayout(false);
            this.sc_main.Panel1.ResumeLayout(false);
            this.sc_main.Panel1.PerformLayout();
            this.sc_main.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sc_main)).EndInit();
            this.sc_main.ResumeLayout(false);
            this.tc_main.ResumeLayout(false);
            this.tp_flp_1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flp_main;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aggiungiToolStripMenuItem;
        private System.Windows.Forms.StatusStrip ss_main;
        private System.Windows.Forms.ToolStripMenuItem eliminaToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cms_flp;
        private System.Windows.Forms.ToolStripMenuItem aggiungiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripStatusLabel tss_status;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ricaricaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem timerDiDefaultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox TbAutoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sommeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commentiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem progettoSingoloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem istanzeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allarmiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertiGerarchiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem layoutManualeToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tss_filtertot;
        private System.Windows.Forms.Timer tmr_tot;
        private System.Windows.Forms.SplitContainer sc_main;
        private System.Windows.Forms.MaskedTextBox mt_ora_inizio;
        private System.Windows.Forms.MaskedTextBox mt_ora_fine;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtp_fine;
        private System.Windows.Forms.DateTimePicker dtp_inizio;
        private System.Windows.Forms.TabControl tc_main;
        private System.Windows.Forms.TabPage tp_flp_1;
        private System.Windows.Forms.TabPage tp_flp_2;
        private System.Windows.Forms.ToolStripMenuItem gerarchiaVuotaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.Button btnFiltra;
        private System.Windows.Forms.ToolStripMenuItem annullaSelezioneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
    }
}

