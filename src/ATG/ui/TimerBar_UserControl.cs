﻿namespace AtgCore
{
    public partial class TimerBar_UserControl : UserControl {
        public Action cmd_timerbar_click;
        public Func<int, Task> cmd_intervalli_doubleclick_async;
        public Func<Task> cmd_gerarchia_doubleclick_async;
        public Action cmd_elimina_click;
        public Func<int, Task> cmd_intervalli_selection_changed_async;
        public Func<int, string, Task> cmd_salva_click_async;
        public Func<intervallo_ctx, Task> cmd_intervalli_keyup_async;
        public Func<bool, Task> cmd_playstop_click_async;

        private const string STOP_LABEL = "■";
        private const string PLAY_LABEL = "►";
        private Color PLAY_BTN_COLOR = Color.FromArgb(199, 255, 199);
        private Color STOP_BTN_COLOR = Color.FromArgb(255, 177, 99);

        private Font font_play;
        private Font font_stop;

        private Dictionary<int, intervallo_ctx> intervalli;

        private bool keydown_suppress;
        public TimerBar_UserControl()
        {
            InitializeComponent();

            txt_note.Text = "";
            lbl_tot.Text = "TOT";
            lbl_data.Text = DateTime.Now.ToString("yyyy\nMM/dd");

            cb_playstop.BackColor = STOP_BTN_COLOR;
            cb_playstop.Text = STOP_LABEL;
            font_stop = new Font("Microsoft Sans Serif", 9);
            font_play = new Font("Arial", 12);

            intervalli = new();

            //var tbar = father.atg.get_active_timerpage().get_timerbar_by_id(id);
            //tbar.evento_gerarchia.Add(raised_event_gerarchia);
            //tbar.evento_intervallo.Add(raised_event_commento);
            //tbar.evento_intervalli.Add(raised_event_intervalli);
            //tbar.evento_status.Add(raised_event_status);
            //tbar.evento_playstop.Add(raised_event_playstop);
            //tbar.evento_selezione.Add(raised_event_selezione);
            //tbar.evento_cancellazione_intervallo.Add(raised_event_cancellazione_record);
        }

        private void tmr_play_Tick(object sender, EventArgs e)
        {
            int gtc = DateTime.Now.Millisecond;
            cb_playstop.ForeColor = Color.FromArgb(
                (int)(199.0 * (0.5 + 0.5 * Math.Cos(0.00314159 * gtc))),
                (int)(255.0 * (0.5 + 0.5 * Math.Cos(0.00314159 * gtc))),
                (int)(199.0 * (0.5 + 0.5 * Math.Cos(0.00314159 * gtc)))
            );

        }

        private void raised_event_gerarchia(string[] gerarchia)
        {
            lst_gerarchia.Items.Clear();
            lst_gerarchia.Items.AddRange(gerarchia);

            var gerarchia_str = string.Join(atg_settings.SeparatoreTermini.ToString(), gerarchia);
            // TODO Program.logger.insert($"impostata gerarchia '{gerarchia_str}' per timerbar {id}");
        }

        private void raised_event_commento(intervallo_model record)
        {
            string msg;

            if (!String.IsNullOrEmpty(record.commento))
            {
                txt_note.Text = record.commento;
                msg = $"commento caricato: {txt_note.Text.Length} byte";
            }
            else
            {
                txt_note.Clear();
                msg = $"nessun commento";
            }

            if (record.fine.HasValue)
            {
                var len = record.fine.Value - record.inizio;
                msg += "; intervallo(ore): " + len.TotalHours.ToString("0.###");
            }

            // TODO Program.logger.insert(msg);
        }

        private void raised_event_intervalli(intervallo_ctx[] pIntervalli)
        {
            lst_intervalli.Items.Clear();
            intervalli.Clear();

            foreach (var record_item in pIntervalli)
            {
                int id;

                if (record_item.fine.HasValue)
                {
                    id = lst_intervalli.Items.Add(
                        record_item.inizio.ToShortTimeString() +
                        " - " +
                        record_item.fine.Value.ToShortTimeString()
                    );
                }
                else
                {
                    id = lst_intervalli.Items.Add(
                        record_item.inizio.ToShortTimeString() + " - "
                    );
                }

                intervalli.Add(id, record_item);
            }
        }

        private void raised_event_status(status_event_info_timerbar info)
        {
            if (info.inizio.HasValue)
                lbl_data.Text = info.inizio.Value.ToString("yyyy\nMM/dd");

            var ts_tot = TimeSpan.FromTicks(info.time_total);
            var v1 = ts_tot.Hours.ToString("00") + ':' + ts_tot.Minutes.ToString("00");
            var v2 = ts_tot.TotalHours.ToString("0.###");
            lbl_tot.Text = $"TOT\n{v1}{Environment.NewLine}{v2}";
        }

        private void raised_event_playstop(bool playing)
        {
            if (playing)
            {
                // TODO queste operazioni devono stare in un posto univoco

                ForeColor = System.Drawing.Color.Red;

                cb_playstop.BackColor = PLAY_BTN_COLOR;
                cb_playstop.Font = font_play;
                cb_playstop.Text = PLAY_LABEL;
                tmr_play.Enabled = true;
                cb_playstop.Checked = true;
            }
            else
            {
                tmr_play.Enabled = false;

                ForeColor = System.Drawing.Color.Black;

                cb_playstop.BackColor = STOP_BTN_COLOR;
                cb_playstop.Font = font_stop;
                cb_playstop.Text = STOP_LABEL;
                cb_playstop.ForeColor = System.Drawing.Color.Black;
                tmr_play.Enabled = false;
                cb_playstop.Checked = false;
            }
        }

        private void raised_event_selezione(bool is_selected)
        {
            //if (!parent.suspend_selection_log) {
            //    if (is_selected) {
            //        // TODO Program.logger.insert($"timerbar {id} selezionata");
            //    }
            //    else {
            //        // TODO Program.logger.insert($"timerbar {id} deselezionata");
            //    }
            //}

            if (is_selected)
            {
                BackColor = System.Drawing.SystemColors.ControlLight;
            }
            else
            {
                BackColor = System.Drawing.SystemColors.Control;
            }
        }

        private void raised_event_cancellazione_record(intervallo_model r)
        {
            // TODO Program.logger.insert($"eliminato intervallo da {r.inizio} a {r.fine}", System.Drawing.Color.Chartreuse);
        }

        private void TimerBar_MouseClick(object sender, MouseEventArgs e) {
            switch (e.Button) {
                case MouseButtons.Right: {
                    cms_timerbar.Show(this, e.Location);
                    break;
                }
                case MouseButtons.Left: {
                    cmd_timerbar_click();
                    break;
                }
            }
        }

        private async void lst_intervalli_MouseDoubleClickAsync(object sender, MouseEventArgs e){
            int id_selected = lst_intervalli.SelectedIndex;
            if (id_selected < 0)
                return;

            await cmd_intervalli_doubleclick_async(id_selected);
        }

        private async void lst_gerarchia_MouseDoubleClickAsync(object sender, MouseEventArgs e){
            await cmd_gerarchia_doubleclick_async();
        }

        private void eliminaToolStripMenuItem_Click(object sender, EventArgs e) {
            cmd_elimina_click();
        }

        private async void lst_intervalli_SelectedIndexChangedAsync(object sender, EventArgs e){
            if (lst_intervalli.SelectedIndex < 0) return;
            await cmd_intervalli_selection_changed_async(lst_intervalli.SelectedIndex);
        }

        private async void btn_salva_ClickAsync(object sender, EventArgs e){
            if (lst_intervalli.SelectedIndex < 0) {
                // TODO Program.logger.insert("non è stato selezionato un intervallo per salvare il commento", Color.Orange);
                return;
            }

            await cmd_salva_click_async(lst_intervalli.SelectedIndex, txt_note.Text);
        }

        private async void lst_intervalli_KeyUpAsync(object sender, KeyEventArgs e){
            if (e.KeyData != Keys.Delete) return;
            if (lst_intervalli.SelectedIndex < 0){
                //iLog("nessun intervallo selezionato", Color.DarkRed);
                return;
            }

            if (MessageBox.Show("vuoi davvero eliminare quest'intervallo?", "cioè tipo", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            //if(MessageBox.Show("assieme all'ultimo intervallo vuoi anche rimuovere la timerbar?", "cioè tipo", MessageBoxButtons.YesNo) == DialogResult.Yes) {
            //    cancella_timerbar = true;
            //}

            await cmd_intervalli_keyup_async(intervalli[lst_intervalli.SelectedIndex]);
        }

        private async void cb_playstop_ClickAsync(object sender, EventArgs e) {
            await cmd_playstop_click_async(Control.ModifierKeys == Keys.Shift);
        }

        private void lbldata_click(object sender, MouseEventArgs e) {
            TimerBar_MouseClick(null, e);
        }

        private void lbltot_click(object sender, MouseEventArgs e) {
            TimerBar_MouseClick(null, e);
        }

        public bool can_delete() {
            // UNDONE rilevare se il commento è stato modificato e non salvato
            // potrei anche mettere un'opzione per salvarlo e poi cancellare lo stesso la timerbar
            return true;
        }

        private void txt_note_KeyPress(object sender, KeyPressEventArgs e) => e.Handled = keydown_suppress;

        private void txt_note_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode != Keys.Back || e.Modifiers != Keys.Control) {
                keydown_suppress = false;
                return;
            }

            e.Handled = true;
            keydown_suppress = true;

            if (txt_note.Text.Length == 0) return;

            int last_space = 0;

            for (var char_index = txt_note.SelectionStart - 1; char_index >= 0; --char_index)
                if (!Char.IsLetterOrDigit(txt_note.Text[char_index]))
                    last_space = char_index;

            txt_note.Text
                = txt_note.Text.Substring(0, last_space)
                + txt_note.Text.Substring(txt_note.SelectionStart - 1, txt_note.Text.Length - txt_note.SelectionStart)
            ;
            txt_note.SelectionStart = last_space;
        }
    }
}
