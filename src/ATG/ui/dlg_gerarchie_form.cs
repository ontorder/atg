﻿namespace AtgCore
{
    public partial class dlg_gerarchie_form : Form {
        private atg_context     _db;
        private termine_ctx[]   elenco_termini;
        private string[]        termini;

        private string[]        istanze;
        private string          istanza_filter = String.Empty;

        public  string[]        dlg_result;

        const int TabPage_CreaGerarchia = 0;
        const int TabPage_Istanze = 1;
        const int TabPage_GVL = 2;

        // ------------------------- //

        public dlg_gerarchie_form(atg_context db) {
            InitializeComponent();

            _db            = db;
            dlg_result     = null;
        }

        public async Task carica_async(string[]? str_precarica)
        {
            elenco_termini = (await _db.termini.get_async()).ToArray();

            if (elenco_termini != null) {
                var temp_termini = new List<string>(elenco_termini.Length);

                foreach (var termine in elenco_termini) {
                    temp_termini.Add(termine.nome);
                    tv_termini.Nodes.Add(termine.nome, termine.nome);
                    lb_aggtermini.Items.Add(termine.nome);
                }
                termini = temp_termini.ToArray();
            }
            else {
                termini = new string[0];
            }

            var elenco_gerarchie = await _db.gerarchie.get_async();

            if (elenco_gerarchie.Any()){
                istanze = elenco_gerarchie
                    .Select(gerarchia => gerarchia.gerarchia_id + ": " + gerarchia.aggrega())
                    .ToArray()
                ;

                lb_istanze.Items.AddRange(istanze);
            }

            tc_gerarchie.SelectedTab = tp_gerachlist;

            if (str_precarica != null)
            {
                var nodi_termini = tv_termini.Nodes;
                string[] termini_nomi = (from TreeNode nodo_termine in nodi_termini select nodo_termine.Text).ToArray();
                TreeNode termine_scelto = null;

                foreach (string precarica in str_precarica)
                {
                    foreach (TreeNode nodo_termine in nodi_termini)
                    {
                        if (nodo_termine.Text == precarica)
                        {
                            termine_scelto = nodo_termine;
                            break;
                        }
                    }
                    termine_scelto.Nodes.AddRange(
                        termini_nomi.Select(T => new TreeNode(T)).ToArray()
                    );
                    nodi_termini = termine_scelto.Nodes;

                    lb_gerarchres.Items.Add(precarica);
                }

                if (termine_scelto != null)
                    tv_termini.SelectedNode = termine_scelto;
            }
        }

        private void tv_gerarchie_MouseClick(object sender, MouseEventArgs e){
            if (tv_termini.SelectedNode == null) return;
            if (tv_termini.SelectedNode.Nodes.Count > 0) return;
            if (elenco_termini == null) return;

            foreach (var termine in elenco_termini)
                tv_termini.SelectedNode.Nodes.Add(termine.nome);
        }

        private async void btn_conferma_ClickAsync(object sender, EventArgs e){
            List<string> res_list;
            string       res_str;

            res_list = null;
            if (tc_gerarchie.SelectedTab==tp_gerarchie){
                if (tv_termini.SelectedNode==null) return;

                Func<TreeNode, List<string>> GenitoriRicors = null;

                GenitoriRicors = new Func<TreeNode, List<string>>(
                    o=>(
                        o.Parent==null?(
                            new List<string>(
                                new string[]{o.Text}
                            )
                        ):(
                            GenitoriRicors(o.Parent)
                            .Union(
                                new string[]{o.Text}
                            )
                            .ToList()
                        )
                    )
                );
                res_list = GenitoriRicors(tv_termini.SelectedNode);
                res_str  = tv_termini.SelectedNode.FullPath;
            }
            else { // tp_istanze
                if (lb_istanze.SelectedIndex<0)return;

                res_str = ((string)lb_istanze.SelectedItem).Split(':')[0];
                int sid = Int32.Parse(res_str);

                res_list =
                    (await _db.gerarchie.get_async(sid))
                    .gerarchia_termini.Select(t => t.termine.nome)
                    .ToList()
                ;
            }

            dlg_result = res_list.ToArray();
            Hide();
        }

        private void tv_gerarchie_KeyUp(object sender, KeyEventArgs e){
            if(e.KeyData == Keys.Right)         return;
            if(e.KeyData!=Keys.Delete)          return;
            if(tv_termini.SelectedNode==null) return;

            tv_termini.SelectedNode.Nodes.Clear();
        }

        private void tv_gerarchie_KeyPress(object sender, KeyPressEventArgs e){
            // cancellazione con backspace
            if (e.KeyChar == '\b'){
                if (tv_termini.SelectedNode == null) return;
                tv_termini.SelectedNode.Remove();
            }
            else
            // inserimento con invio
            if (e.KeyChar == '\r' || e.KeyChar == '\n'){
                TextBox  tb_in    = new TextBox();
                TreeNode new_idx;

                // metto uno spazio vuoto così è chiaro che sto inserendo
                if (tv_termini.SelectedNode != null){
                    if (tv_termini.SelectedNode.Parent == null)
                        new_idx = tv_termini.Nodes.Add("");
                    else
                        new_idx = tv_termini.SelectedNode.Parent.Nodes.Add("");
                }
                else {
                    new_idx = tv_termini.Nodes.Add("");
                }
                tb_in.BackColor  = Color.Black;
                tb_in.ForeColor  = Color.Green;
                var eh_lostfocus = new EventHandler(
                    (o, ev)=>{
                        tb_in.Hide();
                        new_idx.Remove();
                        Controls.Remove(tb_in);
                        tv_termini.Focus();
                        tb_in = null;
                    }
                );
                tb_in.KeyPress  += new KeyPressEventHandler(
                    (o, ev) => {
                        if (ev.KeyChar=='\r') {
                            if (tb_in.Text.Length == 0) return;
                            tb_in.LostFocus          -= eh_lostfocus;
                            new_idx.Text              = tb_in.Text;
                            tb_in.Hide();
                            Controls.Remove(tb_in);
                            tv_termini.SelectedNode = new_idx;
                            tv_termini.Focus();
                            tb_in                     = null;
                        }
                        else
                        if(ev.KeyChar==''){ // È il carattere ESC (char 27), non è vuoto (se no darebbe errore)
                            tb_in.Hide();
                            Controls.Remove(tb_in);
                            tb_in = null;
                        }
                    }
                );
                tb_in.LostFocus += eh_lostfocus;
                tv_termini.Controls.Add(tb_in);
                tb_in.Visible    = true;
                tb_in.BringToFront();
                tb_in.Left       = new_idx.Bounds.Left;
                tb_in.Top        = new_idx.Bounds.Top;
                tb_in.Focus();
            }
            else
            // modifica con lettere
            if(
                (e.KeyChar >= 'a' && e.KeyChar <= 'z')
                ||
                (e.KeyChar >= 'A' && e.KeyChar <= 'Z')
                ||
                (e.KeyChar >= '0' && e.KeyChar <= '9')
            ){
                if (tv_termini.SelectedNode==null) return;
                var snode  = tv_termini.SelectedNode;
                if (termini != null && termini.Contains(snode.Text)) return;
                var tb_in  = new TextBox();
                tb_in.BackColor  = Color.Black;
                tb_in.ForeColor  = Color.Green;
                tb_in.Text       = e.KeyChar.ToString()+snode.Text.Substring(1);
                var eh_lostfocus = new EventHandler(
                    (o, ev) => {
                        tb_in.Hide();
                        Controls.Remove(tb_in);
                        tv_termini.Focus();
                        tb_in = null;
                    }
                );
                tb_in.KeyPress  += new KeyPressEventHandler(
                    (o,ev) => {
                        if (ev.KeyChar == '\r'){
                            if (tb_in.Text.Length == 0) return;
                            tb_in.LostFocus -= eh_lostfocus;
                            snode.Text       = tb_in.Text;
                            tb_in.Hide();
                            Controls.Remove(tb_in);
                            tv_termini.Focus();
                            tb_in            = null;
                        }
                        else
                        if (ev.KeyChar == ''){ // È il carattere ESC (char 27), non è vuoto (se no darebbe errore)
                            tb_in.Hide();
                            Controls.Remove(tb_in);
                            tb_in = null;
                        }
                    }
                );
                tb_in.LostFocus += eh_lostfocus;
                tv_termini.Controls.Add(tb_in);
                tb_in.Visible    = true;
                tb_in.BringToFront();
                tb_in.Focus();
                tb_in.Left       = tv_termini.SelectedNode.Bounds.Left;
                tb_in.Top        = tv_termini.SelectedNode.Bounds.Top;
                tb_in.Select(1, tb_in.Text.Length-1);
            }
        }

        private void lb_aggtermini_MouseDoubleClick(object sender, MouseEventArgs e){
            if (lb_aggtermini.SelectedIndex < 0) return;
            if (lb_gerarchres.SelectedIndex < 0) {
                lb_gerarchres.Items.Add(lb_aggtermini.SelectedItem);
            }
            else {
                lb_gerarchres.Items.Insert(lb_gerarchres.SelectedIndex, lb_aggtermini.SelectedItem);
            }
        }

        private void lb_gerarchres_KeyDown(object sender, KeyEventArgs e){
            if (lb_gerarchres.SelectedIndex < 0) return;

            if (e.KeyData == (Keys.Up | Keys.Shift)){
                if(lb_gerarchres.SelectedIndex==0) return;
                var otemp = lb_gerarchres.SelectedItem;
                int itemp = lb_gerarchres.SelectedIndex;
                lb_gerarchres.Items[itemp]   = lb_gerarchres.Items[itemp-1];
                lb_gerarchres.Items[itemp-1] = otemp;
                lb_gerarchres.SelectedIndex  = itemp;
            }
            else
            if (e.KeyData == (Keys.Down | Keys.Shift)) {
                if (lb_gerarchres.SelectedIndex==(lb_gerarchres.Items.Count-1)) return;
                var otemp = lb_gerarchres.SelectedItem;
                int itemp = lb_gerarchres.SelectedIndex;
                lb_gerarchres.Items[itemp]   = lb_gerarchres.Items[itemp+1];
                lb_gerarchres.Items[itemp+1] = otemp;
                lb_gerarchres.SelectedIndex  = itemp;
            }
            else
            if (e.KeyData == Keys.Delete) {
                lb_gerarchres.Items.RemoveAt(lb_gerarchres.SelectedIndex);
            }
        }

        private void ordinaPerNomeToolStripMenuItem_Click(object sender, EventArgs e){
            if (lb_istanze.Items.Count == 0) return;
            char[] sepa = new char[]{':'};
            var    sli  = lb_istanze.Items.Cast<string>().ToList();
            sli.Sort(
                (s1,s2) => String.Compare(s1.Split(sepa, 2)[1], s2.Split(sepa, 2)[1])
            );
            lb_istanze.Items.Clear();
            lb_istanze.Items.AddRange(sli.ToArray());
        }

        private void ordinaPerIdToolStripMenuItem_Click(object sender, EventArgs e) {
            if (lb_istanze.Items.Count==0) return;
            var sli = lb_istanze.Items.Cast<string>().ToList();
            sli.Sort();
            lb_istanze.Items.Clear();
            lb_istanze.Items.AddRange(sli.ToArray());
        }

        private void lb_istanze_MouseUp(object sender, MouseEventArgs e){
            if (e.Button == System.Windows.Forms.MouseButtons.Right) {
                cms_istanze.Show(lb_istanze.PointToScreen(e.Location));
            }
        }

        private void lb_istanze_KeyPress(object sender, KeyPressEventArgs e) {
            var k = e.KeyChar;

            if (Char.IsLetterOrDigit(k) && !txtIstanzaSearch.Visible) {
                txtIstanzaSearch.Text = k.ToString();
                txtIstanzaSearch.Visible = true;
                txtIstanzaSearch.Focus();
                txtIstanzaSearch.Select(1, 0);
            }
        }

        void AggiornaFiltroIstanza() {
            lb_istanze.Items.Clear();
            lb_istanze.Items.AddRange(
                istanze
                .Where(istanza => istanza.Contains(istanza_filter))
                .ToArray()
            );
        }

        private void txtIstanzaSearch_KeyPress(object sender, KeyPressEventArgs e) {
            if (e.KeyChar == 27) {
                txtIstanzaSearch.Text = "";
                txtIstanzaSearch.Visible = false;

                istanza_filter = string.Empty;
                AggiornaFiltroIstanza();
                return;
            }
        }

        private void tc_gerarchie_KeyPress(object sender, KeyPressEventArgs e) {
            if (!tc_gerarchie.Focused)
                return;

            switch (tc_gerarchie.SelectedIndex){
                case TabPage_CreaGerarchia:{
                    tv_gerarchie_KeyPress(sender, e);
                    break;
                }
                case TabPage_Istanze:{
                    lb_istanze_KeyPress(sender, e);
                    break;
                }
                case TabPage_GVL:{
                    break;
                }
            }
        }

        private void txtIstanzaSearch_TextChanged(object sender, EventArgs e) {
            istanza_filter = txtIstanzaSearch.Text;
            AggiornaFiltroIstanza();
        }
    }
}
