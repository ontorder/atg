﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
//using System.Windows.Forms.DataVisualization;
using System.Windows.Forms.DataVisualization.Charting;
using System.Xml;

namespace AtgCore
{
    public partial class frm_report_form : Form {
        private   Dictionary<string, List<long>> v_inizio;
        private   Dictionary<string, List<long>> v_fine;

        // ---------------------- //

        public frm_report_form(){
            XmlDocument xml_cfg;
            TimeSpan    tstot2;

            InitializeComponent();

            v_inizio = new Dictionary<string,List<long>>();
            v_fine   = new Dictionary<string,List<long>>();
            xml_cfg  = null; // UNDONE atg_ds.getOH();

            // i dati dovrebbero essere in ordine
            foreach(XmlNode xn in xml_cfg.SelectNodes("root/istanze//gerarchia")){
                string   idg     = xn.Attributes["id"].Value;
                string   db_path = null; // UNDONE atg_ds.getPath() + atg_ds.getAuthor() + '/' + idg;
                long     ts_tot  = 0;

                if(System.IO.Directory.Exists(db_path)){
                    List<long>   i_ini    = new List<long>()
                        ,        i_fin    = new List<long>()
                    ;
                    long         t_ini, t_fin;
                    List<string> db_files = new List<string>(System.IO.Directory.EnumerateFiles(db_path, "*.xml", System.IO.SearchOption.TopDirectoryOnly));
                    db_files.Sort();

                    foreach(string sfile in db_files){
                        XmlDocument xd = new XmlDocument();
                        long        parz;
                        xd.Load(sfile);

                        foreach(XmlNode xx in xd.SelectNodes("root//record")){
                            if(
                                xx.NodeType == XmlNodeType.Element
                                &&
                                xx.Attributes["fine"].Value != ""
                            ){
                                t_fin   = DateTime.Parse(xx.Attributes["fine"].Value).ToBinary();
                                t_ini   = DateTime.Parse(xx.Attributes["inizio"].Value).ToBinary();
                                parz    = t_fin-t_ini;
                                i_fin.Add(t_fin);
                                i_ini.Add(t_ini);

                                ts_tot += parz;
                            }
                        }
                    }

                    v_inizio.Add(idg, i_ini);
                    v_fine.Add(idg, i_fin);
                }

                tstot2 = new TimeSpan(ts_tot);
                lv_ore.Items.Add(
                    new ListViewItem(
                        new string[]{
                            idg,
                            tstot2.TotalHours.ToString("0.##")
                        }
                    )
                );
            }
        }

        private void lv_ore_SelectedIndexChanged(object sender, EventArgs e){
            grf_ore.Series.Clear();
            if(lv_ore.SelectedIndices.Count==0) return;

            string     idg      = lv_ore.SelectedItems[0].SubItems[0].Text;
            if(string.IsNullOrEmpty(idg)) return;

            Series     ds1      = new Series("id "+idg);
            List<long> t_inizio = v_inizio[idg]
                ,      t_fine   = v_fine[idg];
            double     t_t;

            for(int cx = 0; cx<t_inizio.Count; ++cx)
                if(t_fine[cx]>0){
                    t_t = t_fine[cx]-t_inizio[cx];
                    t_t /= 36000000000.0; // 3600 * 10 mil
                    ds1.Points.Add(t_t);
                }

            grf_ore.Series.Add(ds1);
        }
    }
}
