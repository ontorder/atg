﻿namespace AtgCore
{
    partial class dlg_edita_ore_form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_annulla = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.mtb_ora_inizio = new System.Windows.Forms.MaskedTextBox();
            this.mtb_ora_fine = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // btn_ok
            // 
            this.btn_ok.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_ok.Location = new System.Drawing.Point(129, 3);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(38, 46);
            this.btn_ok.TabIndex = 0;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_annulla
            // 
            this.btn_annulla.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_annulla.Location = new System.Drawing.Point(6, 27);
            this.btn_annulla.Name = "btn_annulla";
            this.btn_annulla.Size = new System.Drawing.Size(118, 22);
            this.btn_annulla.TabIndex = 1;
            this.btn_annulla.Text = "Annulla";
            this.btn_annulla.UseVisualStyleBackColor = true;
            this.btn_annulla.Click += new System.EventHandler(this.btn_annulla_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 14);
            this.label1.TabIndex = 2;
            this.label1.Text = "►";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "■";
            // 
            // mtb_ora_inizio
            // 
            this.mtb_ora_inizio.Location = new System.Drawing.Point(25, 4);
            this.mtb_ora_inizio.Mask = "##:##";
            this.mtb_ora_inizio.Name = "mtb_ora_inizio";
            this.mtb_ora_inizio.Size = new System.Drawing.Size(35, 20);
            this.mtb_ora_inizio.TabIndex = 5;
            this.mtb_ora_inizio.TextChanged += new System.EventHandler(this.mtb_ora_inizio_TextChanged);
            // 
            // mtb_ora_fine
            // 
            this.mtb_ora_fine.Location = new System.Drawing.Point(88, 4);
            this.mtb_ora_fine.Mask = "##:##";
            this.mtb_ora_fine.Name = "mtb_ora_fine";
            this.mtb_ora_fine.Size = new System.Drawing.Size(35, 20);
            this.mtb_ora_fine.TabIndex = 6;
            this.mtb_ora_fine.TextChanged += new System.EventHandler(this.mtb_ora_fine_TextChanged);
            // 
            // dlg_edita_ore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_annulla;
            this.ClientSize = new System.Drawing.Size(169, 53);
            this.Controls.Add(this.mtb_ora_fine);
            this.Controls.Add(this.mtb_ora_inizio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_annulla);
            this.Controls.Add(this.btn_ok);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "dlg_edita_ore";
            this.ShowInTaskbar = false;
            this.Text = "Sovrascrivi Intervallo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button btn_annulla;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox mtb_ora_inizio;
        private System.Windows.Forms.MaskedTextBox mtb_ora_fine;
    }
}