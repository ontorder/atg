﻿namespace AtgCore
{
    partial class dlg_gerarchie_form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.tv_termini = new System.Windows.Forms.TreeView();
            this.tc_gerarchie = new System.Windows.Forms.TabControl();
            this.tp_gerarchie = new System.Windows.Forms.TabPage();
            this.tp_istanze = new System.Windows.Forms.TabPage();
            this.txtIstanzaSearch = new System.Windows.Forms.TextBox();
            this.lb_istanze = new System.Windows.Forms.ListBox();
            this.tp_gerachlist = new System.Windows.Forms.TabPage();
            this.tlp_gvl = new System.Windows.Forms.TableLayoutPanel();
            this.lb_gerarchres = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lb_aggtermini = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_conferma = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cms_istanze = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ordinaPerNomeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordinaPerIdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tc_gerarchie.SuspendLayout();
            this.tp_gerarchie.SuspendLayout();
            this.tp_istanze.SuspendLayout();
            this.tp_gerachlist.SuspendLayout();
            this.tlp_gvl.SuspendLayout();
            this.cms_istanze.SuspendLayout();
            this.SuspendLayout();
            // 
            // tv_gerarchie
            // 
            this.tv_termini.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tv_termini.HideSelection = false;
            this.tv_termini.Location = new System.Drawing.Point(3, 3);
            this.tv_termini.Name = "tv_gerarchie";
            this.tv_termini.PathSeparator = "→";
            this.tv_termini.Size = new System.Drawing.Size(384, 338);
            this.tv_termini.TabIndex = 0;
            this.tv_termini.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tv_gerarchie_KeyPress);
            this.tv_termini.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tv_gerarchie_KeyUp);
            this.tv_termini.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tv_gerarchie_MouseClick);
            // 
            // tc_gerarchie
            // 
            this.tc_gerarchie.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tc_gerarchie.Controls.Add(this.tp_gerarchie);
            this.tc_gerarchie.Controls.Add(this.tp_istanze);
            this.tc_gerarchie.Controls.Add(this.tp_gerachlist);
            this.tc_gerarchie.Location = new System.Drawing.Point(1, 1);
            this.tc_gerarchie.Name = "tc_gerarchie";
            this.tc_gerarchie.SelectedIndex = 0;
            this.tc_gerarchie.Size = new System.Drawing.Size(398, 370);
            this.tc_gerarchie.TabIndex = 1;
            this.tc_gerarchie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tc_gerarchie_KeyPress);
            // 
            // tp_gerarchie
            // 
            this.tp_gerarchie.Controls.Add(this.tv_termini);
            this.tp_gerarchie.Location = new System.Drawing.Point(4, 22);
            this.tp_gerarchie.Name = "tp_gerarchie";
            this.tp_gerarchie.Padding = new System.Windows.Forms.Padding(3);
            this.tp_gerarchie.Size = new System.Drawing.Size(390, 344);
            this.tp_gerarchie.TabIndex = 0;
            this.tp_gerarchie.Text = "Crea Gerarchia";
            this.tp_gerarchie.UseVisualStyleBackColor = true;
            // 
            // tp_istanze
            // 
            this.tp_istanze.Controls.Add(this.lb_istanze);
            this.tp_istanze.Location = new System.Drawing.Point(4, 22);
            this.tp_istanze.Name = "tp_istanze";
            this.tp_istanze.Padding = new System.Windows.Forms.Padding(3);
            this.tp_istanze.Size = new System.Drawing.Size(390, 344);
            this.tp_istanze.TabIndex = 1;
            this.tp_istanze.Text = "Istanze";
            this.tp_istanze.UseVisualStyleBackColor = true;
            // 
            // txtIstanzaSearch
            // 
            this.txtIstanzaSearch.BackColor = System.Drawing.Color.Black;
            this.txtIstanzaSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIstanzaSearch.ForeColor = System.Drawing.Color.White;
            this.txtIstanzaSearch.Location = new System.Drawing.Point(179, 371);
            this.txtIstanzaSearch.Name = "txtIstanzaSearch";
            this.txtIstanzaSearch.Size = new System.Drawing.Size(220, 20);
            this.txtIstanzaSearch.TabIndex = 4;
            this.txtIstanzaSearch.Visible = false;
            this.txtIstanzaSearch.TextChanged += new System.EventHandler(this.txtIstanzaSearch_TextChanged);
            this.txtIstanzaSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIstanzaSearch_KeyPress);
            // 
            // lb_istanze
            // 
            this.lb_istanze.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_istanze.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_istanze.FormattingEnabled = true;
            this.lb_istanze.ItemHeight = 14;
            this.lb_istanze.Location = new System.Drawing.Point(3, 3);
            this.lb_istanze.Name = "lb_istanze";
            this.lb_istanze.Size = new System.Drawing.Size(384, 338);
            this.lb_istanze.TabIndex = 0;
            this.lb_istanze.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lb_istanze_KeyPress);
            this.lb_istanze.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lb_istanze_MouseUp);
            // 
            // tp_gerachlist
            // 
            this.tp_gerachlist.Controls.Add(this.tlp_gvl);
            this.tp_gerachlist.Location = new System.Drawing.Point(4, 22);
            this.tp_gerachlist.Name = "tp_gerachlist";
            this.tp_gerachlist.Size = new System.Drawing.Size(390, 344);
            this.tp_gerachlist.TabIndex = 2;
            this.tp_gerachlist.Text = "Gerarchie via Lista";
            this.tp_gerachlist.UseVisualStyleBackColor = true;
            // 
            // tlp_gvl
            // 
            this.tlp_gvl.ColumnCount = 2;
            this.tlp_gvl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_gvl.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_gvl.Controls.Add(this.lb_gerarchres, 0, 1);
            this.tlp_gvl.Controls.Add(this.label3, 0, 0);
            this.tlp_gvl.Controls.Add(this.lb_aggtermini, 1, 1);
            this.tlp_gvl.Controls.Add(this.label2, 1, 0);
            this.tlp_gvl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_gvl.Location = new System.Drawing.Point(0, 0);
            this.tlp_gvl.Name = "tlp_gvl";
            this.tlp_gvl.RowCount = 2;
            this.tlp_gvl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp_gvl.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_gvl.Size = new System.Drawing.Size(390, 344);
            this.tlp_gvl.TabIndex = 4;
            // 
            // lb_gerarchres
            // 
            this.lb_gerarchres.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_gerarchres.FormattingEnabled = true;
            this.lb_gerarchres.Location = new System.Drawing.Point(3, 23);
            this.lb_gerarchres.Name = "lb_gerarchres";
            this.lb_gerarchres.Size = new System.Drawing.Size(189, 318);
            this.lb_gerarchres.TabIndex = 1;
            this.lb_gerarchres.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lb_gerarchres_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Gerarchia:";
            // 
            // lb_aggtermini
            // 
            this.lb_aggtermini.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_aggtermini.FormattingEnabled = true;
            this.lb_aggtermini.Location = new System.Drawing.Point(198, 23);
            this.lb_aggtermini.Name = "lb_aggtermini";
            this.lb_aggtermini.Size = new System.Drawing.Size(189, 318);
            this.lb_aggtermini.TabIndex = 0;
            this.lb_aggtermini.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lb_aggtermini_MouseDoubleClick);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(295, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Termini disponibili:";
            // 
            // btn_conferma
            // 
            this.btn_conferma.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_conferma.Location = new System.Drawing.Point(1, 370);
            this.btn_conferma.Name = "btn_conferma";
            this.btn_conferma.Size = new System.Drawing.Size(75, 21);
            this.btn_conferma.TabIndex = 2;
            this.btn_conferma.Text = "Conferma";
            this.btn_conferma.UseVisualStyleBackColor = true;
            this.btn_conferma.Click += new System.EventHandler(this.btn_conferma_ClickAsync);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 374);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "ATTENZIONE: evitare pastrocchi :)";
            // 
            // cms_istanze
            // 
            this.cms_istanze.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ordinaPerNomeToolStripMenuItem,
            this.ordinaPerIdToolStripMenuItem});
            this.cms_istanze.Name = "cms_istanze";
            this.cms_istanze.Size = new System.Drawing.Size(165, 48);
            // 
            // ordinaPerNomeToolStripMenuItem
            // 
            this.ordinaPerNomeToolStripMenuItem.Name = "ordinaPerNomeToolStripMenuItem";
            this.ordinaPerNomeToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.ordinaPerNomeToolStripMenuItem.Text = "Ordina per nome";
            this.ordinaPerNomeToolStripMenuItem.Click += new System.EventHandler(this.ordinaPerNomeToolStripMenuItem_Click);
            // 
            // ordinaPerIdToolStripMenuItem
            // 
            this.ordinaPerIdToolStripMenuItem.Name = "ordinaPerIdToolStripMenuItem";
            this.ordinaPerIdToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.ordinaPerIdToolStripMenuItem.Text = "Ordina per id";
            this.ordinaPerIdToolStripMenuItem.Click += new System.EventHandler(this.ordinaPerIdToolStripMenuItem_Click);
            // 
            // dlg_gerarchie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 391);
            this.Controls.Add(this.txtIstanzaSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_conferma);
            this.Controls.Add(this.tc_gerarchie);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "dlg_gerarchie";
            this.Text = "Gerarchie";
            this.tc_gerarchie.ResumeLayout(false);
            this.tp_gerarchie.ResumeLayout(false);
            this.tp_istanze.ResumeLayout(false);
            this.tp_gerachlist.ResumeLayout(false);
            this.tlp_gvl.ResumeLayout(false);
            this.tlp_gvl.PerformLayout();
            this.cms_istanze.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView tv_termini;
        private System.Windows.Forms.TabControl tc_gerarchie;
        private System.Windows.Forms.TabPage tp_gerarchie;
        private System.Windows.Forms.TabPage tp_istanze;
        private System.Windows.Forms.ListBox lb_istanze;
        private System.Windows.Forms.Button btn_conferma;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tp_gerachlist;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lb_gerarchres;
        private System.Windows.Forms.ListBox lb_aggtermini;
        private System.Windows.Forms.TableLayoutPanel tlp_gvl;
        private System.Windows.Forms.ContextMenuStrip cms_istanze;
        private System.Windows.Forms.ToolStripMenuItem ordinaPerNomeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordinaPerIdToolStripMenuItem;
        private System.Windows.Forms.TextBox txtIstanzaSearch;
    }
}