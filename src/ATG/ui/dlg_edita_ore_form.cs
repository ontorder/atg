﻿namespace AtgCore;

public partial class dlg_edita_ore_form : Form {
    private DateTime  dt_inizio;
    private DateTime  dt_fine;

    public  DateTime? dt_inizio_mod{ get; set; }
    public  DateTime? dt_fine_mod  { get; set; }

    // ----------------------- //

    public dlg_edita_ore_form(DateTime p_dt_inizio, DateTime p_dt_fine){
        InitializeComponent();

        mtb_ora_fine.Text   = p_dt_fine.ToShortTimeString();
        mtb_ora_inizio.Text = p_dt_inizio.ToShortTimeString();
        dt_inizio           = p_dt_inizio.Date;
        dt_fine             = p_dt_fine.Date;

        dt_inizio_mod       = null;
        dt_fine_mod         = null;
    }

    // -----------------

    private void btn_annulla_Click(object sender, EventArgs e) =>
        Hide();

    private void btn_ok_Click(object sender, EventArgs e) {
        DateTime dt_fine_temp
            ,    dt_inizio_temp
        ;

        if(
            !DateTime.TryParse(mtb_ora_fine.Text, out dt_fine_temp)
            ||
            !DateTime.TryParse(mtb_ora_inizio.Text, out dt_inizio_temp)
        ){
            MessageBox.Show("Impossibile parsare le ore");
            return;
        }

        // comunque bisognerebbe passare da un controller
        if(dt_inizio_temp > dt_fine_temp){
            MessageBox.Show("ora di inizio dopo ora di fine", "ERRORE");
            return;
        }

        dt_fine_mod   = new DateTime(
            dt_fine.Year,
            dt_fine.Month,
            dt_fine.Day,
            dt_fine_temp.Hour,
            dt_fine_temp.Minute,
            dt_fine_temp.Second
        );
        dt_inizio_mod = new DateTime(
            dt_inizio.Year,
            dt_inizio.Month,
            dt_inizio.Day,
            dt_inizio_temp.Hour,
            dt_inizio_temp.Minute,
            dt_inizio_temp.Second
        );

        Hide();
    }

    private void mtb_ora_inizio_TextChanged(object sender, EventArgs e) {
        DateTime dt_inizio_temp;

        if(DateTime.TryParse(mtb_ora_inizio.Text, out dt_inizio_temp))
            mtb_ora_inizio.ForeColor = Color.Black;
        else
            mtb_ora_inizio.ForeColor = Color.Red;
    }

    private void mtb_ora_fine_TextChanged(object sender,EventArgs e) {
        DateTime dt_fine_temp;

        if(DateTime.TryParse(mtb_ora_fine.Text, out dt_fine_temp))
            mtb_ora_fine.ForeColor = Color.Black;
        else
            mtb_ora_fine.ForeColor = Color.Red;
    }
}
