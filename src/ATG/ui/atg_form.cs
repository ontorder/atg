﻿namespace AtgCore
{
    public partial class atg_form : Form {
        public Action<int> cmd_menu_istanza_click;
        public Action cmd_ricarica_click;
        public Action cmd_elimina_click;
        public Action cmd_aggiungi_click;
        public Action cmd_gerarchia_vuota_click;
        public Action<string> cmd_autore_validated;
        public Func<Task> cmd_form_deactivated_async;
        public Func<Task> cmd_form_closing_async;
        public Func<DateTime, DateTime, Task> cmd_filtra_async;
        public Action annulla_selezione_click;
        private bool form_closing;
        private DateTime form_started;
        private Action? formLoadedEvent;
        private ToolTip status_tt;
        public bool suspend_selection_log;
        private TimerBar_UserControl? tb_moving = null;
        public List<TimerBar_UserControl> timerbars;

        public atg_form(Action? pFormLoadedEvent = null)
        {
            formLoadedEvent = pFormLoadedEvent;

            form_started = DateTime.Now;
            suspend_selection_log = false;
            timerbars = new List<TimerBar_UserControl>();

            // SetEvents();

            Visible = false;
            InitializeComponent();
            status_tt = new ToolTip();
            // TODO logger.init_ui(this, tss_status);
            // TODO logger.eventhandler_log.Add(raised_event_log);

            // TODO logger.insert("componenti grafici creati");

            Application.DoEvents();
        }

        private void event_filter_change(DateTime filter_start, DateTime filter_end)
        {
            dtp_inizio.Value = filter_start;
            dtp_fine.Value = filter_end;
            mt_ora_inizio.Text = dtp_inizio.Value.ToShortTimeString();
            mt_ora_fine.Text = dtp_fine.Value.ToShortTimeString();
        }

        private void event_user_data(string username)
        {
            try
            {
                // TODO logger.insert($"utente: '{username}'");
                TbAutoreToolStripMenuItem.Text = username;

                // TODO logger.insert("inizializzazione completata");
            }
            catch (Exception user_err)
            {
                MessageBox.Show(exception_logger.recursive(user_err), "[frm_atg.event_user_data] ECCEZIONE");
                Environment.Exit(-2);
            }
        }

        private void raised_event_log(string last_text, Color last_text_color, string tooltip_text)
        {
            status_tt.SetToolTip(ss_main, tooltip_text);

            tss_status.Text = last_text;
            tss_status.ForeColor = last_text_color;
        }

        private void menu_istanza_click(object o, EventArgs eh){
            string label               = ((ToolStripMenuItem)o).Text;
            string id_gerarchia_string = label.Split(':')[0];
            int    id_gerarchia        = Int32.Parse(id_gerarchia_string);

            cmd_menu_istanza_click(id_gerarchia);
        }

        private void aggiungiToolStripMenuItem_Click(object sender, EventArgs e){
            //TimerBar ntb = new TimerBar(stato_atg);
            //flp_main.Controls.Add(ntb);
            //iLog("timerbar aggiunta");
        }

        private void ricaricaToolStripMenuItem_Click(object sender, EventArgs e){
            cmd_ricarica_click();
        }

        private void eliminaToolStripMenuItem_Click(object sender, EventArgs e){
            cmd_elimina_click();
        }

        private void flp_main_MouseClick(object sender, MouseEventArgs e){
            if (e.Button == MouseButtons.Right)
                cms_flp.Show(flp_main, e.Location);
        }

        private void aggiungiToolStripMenuItem1_Click(object sender, EventArgs e){
            cmd_aggiungi_click();
        }

        private void gerarchiaVuotaToolStripMenuItem_Click(object sender,EventArgs e) {
            cmd_gerarchia_vuota_click();
        }

        private void timerDiDefaultToolStripMenuItem_Click(object sender, EventArgs e){
            // root/default
        }

        private bool MostraTimerBar_Filtro(bool silenz=false){
            // TODO mostrare gerarchie configurate in root/default...
            // ATTENZIONE nel caso in cui il filtro nasconda timerbar attivate!!!
            //     OPPURE faccio che non toccarli e bon

            //iLog($"filtro applicato: {tbl.Length} tb");

            return true;
        }

        private void TbAutoreToolStripMenuItem_Validated(object sender, EventArgs e){
            cmd_autore_validated(TbAutoreToolStripMenuItem.Text);
        }

        private void sommeToolStripMenuItem_Click(object sender, EventArgs e){
            //frm_report temp_frm = new frm_report(stato_atg);
            //temp_frm.ShowDialog(this);
            //temp_frm.Dispose();
            //temp_frm            = null;
        }

        private async void frm_atg_Deactivate_Async(object sender, EventArgs e){
            // UNDONE salva solo se sporco (ma questo significa una flag anche dalle timerbar)
            if (form_closing) return;

            // UNDONE await cmd_form_deactivated_async();
        }

        private async void frm_atg_FormClosingAsync(object sender, FormClosingEventArgs e){
            form_closing = true;
            // UNDONE SE CI SONO TIMER ATTIVI DEVO INTERROMPERLI, magari metto anche un messaggio che avvisa di ciò
            if (MessageBox.Show("Chiudere ATG???", ":(", MessageBoxButtons.YesNo) == DialogResult.No){
                e.Cancel     = true;
                form_closing = false;

                return;
            }

            await cmd_form_closing_async();
        }

        private void _MouseDown(object s, MouseEventArgs m){ if(layoutManualeToolStripMenuItem.Checked) tb_moving = (TimerBar_UserControl)s; }

        private void _MouseUp(object s, MouseEventArgs m){ if(layoutManualeToolStripMenuItem.Checked) tb_moving = null; }

        private void _MouseMove(object s, MouseEventArgs m){
            if (layoutManualeToolStripMenuItem.Checked && tb_moving != null){
                flp_main.Capture = true;
                System.Threading.Thread.Sleep(99);
            }
        }

        private void layoutManualeToolStripMenuItem_Click(object sender, EventArgs e){
            layoutManualeToolStripMenuItem.Checked = !layoutManualeToolStripMenuItem.Checked;

            if (layoutManualeToolStripMenuItem.Checked){
                flp_main.SuspendLayout();
                MaximizeBox = true;
                foreach (Control _tb in flp_main.Controls) if (_tb is TimerBar_UserControl) {
                    _tb.MouseDown += _MouseDown;
                    _tb.MouseMove += _MouseMove;
                    _tb.MouseUp   += _MouseUp;
                }
            }
            else {
                foreach (Control _tb in flp_main.Controls) if (_tb is TimerBar_UserControl) {
                    _tb.MouseDown -= _MouseDown;
                    _tb.MouseMove -= _MouseMove;
                    _tb.MouseUp   -= _MouseUp;
                }
                flp_main.ResumeLayout();
                MaximizeBox = false;
            }
        }

        private void flp_main_MouseMove(object sender, MouseEventArgs e){
            if (layoutManualeToolStripMenuItem.Checked && tb_moving != null){
                tb_moving.Left = e.X;
                tb_moving.Top  = e.Y;
            }
        }

        private void flp_main_MouseUp(object sender, MouseEventArgs e){
            if (layoutManualeToolStripMenuItem.Checked)
            //if(tb_moving!=null)
                tb_moving = null;
        }

        private void commentiToolStripMenuItem_Click(object sender, EventArgs e) {
        }

        private void progettoSingoloToolStripMenuItem_Click(object sender,EventArgs e) {
        }

        private async void btnFiltra_ClickAsync(object sender, EventArgs e) {
            TimeSpan tsTimeStart, tsTimeEnd;
            DateTime fltStart, fltEnd;

            // TODO controlla poi assieme ad ora, non solo data?
            if (!TimeSpan.TryParse(mt_ora_inizio.Text, out tsTimeStart) || !TimeSpan.TryParse(mt_ora_fine.Text, out tsTimeEnd)) {
                // TODO Program.logger.insert("inserire degli orari validi...", Color.Red);
                return;
            }

            fltStart = dtp_inizio.Value.Add(tsTimeStart);
            fltEnd = dtp_fine.Value + tsTimeEnd;

            await cmd_filtra_async(fltStart, fltEnd);
        }

        private void annullaSelezioneToolStripMenuItem_Click(object sender, EventArgs e) {
            suspend_selection_log = true;
            annulla_selezione_click();
        }

        private void raised_event_added_timerbar(uint id_timerbar, int? id_gerarchia)
        {
            var tbar = new TimerBar_UserControl(); // this, id_timerbar);

            timerbars.Add(tbar);
            flp_main.Controls.Add(tbar);

            // TODO Program.logger.insert($"timerbar aggiunta (id: {id_timerbar}); gerarchia: {id_gerarchia}");
        }

        private void raised_event_termini(termine_model[] termini)
        {
        }

        private void raised_event_gerarchie(gerarchia_model[] gerarchie) =>
            ricarica_istanze();

        private void ricarica_istanze()
        {
            //var termini = atg.db.elenco_termini();
            //
            //istanzeToolStripMenuItem.DropDownItems.Clear();
            //istanzeToolStripMenuItem.DropDownItems.AddRange(termini.Select(termine => new ToolStripMenuItem(termine.nome)).ToArray());
            //
            //var termini_per_gerarchia = atg.db.termini_per_gerarchia();
            //
            //foreach(ToolStripMenuItem tsi in istanzeToolStripMenuItem.DropDownItems){
            //    var gerarchStartsWith = termini_per_gerarchia.Where(kvp=>kvp.Value[0] == tsi.Text);
            //
            //    if (gerarchStartsWith.Count()>0){
            //        tsi.DropDownItems.AddRange(
            //            gerarchStartsWith.Select(
            //                kvp => new ToolStripMenuItem(
            //                    kvp.Key + ": " + String.Join(config.atg.SEPARATORE_TERMINI.ToString(), kvp.Value)
            //                    ,null
            //                    ,menu_istanza_click
            //                )
            //            ).ToArray()
            //        );
            //    }
            //}
        }

        private void raised_event_timerpage_status(TimeSpan ltot)
        {
            var time_ses = $"{ltot.TotalHours:00}:{ltot.Minutes:00}";
            var time_dec = ltot.TotalHours.ToString("0.###");

            tss_filtertot.Text = $"{time_ses}/{time_dec}";
        }

        private void raised_event_gerarchie_preferite(string[] preferite) =>
            ricarica_gerarchie_preferite(preferite);

        // ma questo non è gerarchie preferite?!
        private void ricarica_gerarchie_preferite(string[] preferite)
        {
            for (int x = 2; x < aggiungiToolStripMenuItem.DropDownItems.Count; ++x)
                aggiungiToolStripMenuItem.DropDownItems.RemoveAt(x);

            aggiungiToolStripMenuItem.DropDownItems.AddRange(
                preferite
                .Select(pref => new ToolStripMenuItem(pref))
                .ToArray()
            );
        }

        private void raised_event_clean_selection()
        {
            suspend_selection_log = false;

            // TODO Program.logger.insert("ripulita selezione timerbar");
        }

        private bool raised_event_delete_timerbar(uint timerbar_id)
        {
            //var tbar    = timerbars.Single(timerbar => timerbar.id == timerbar_id);
            //var confirm = tbar.can_delete();
            //
            //if (confirm) {
            //    flp_main.Controls.Remove(tbar);
            //    timerbars.Remove(tbar);
            //    tbar.Dispose();
            //}
            //
            //return confirm;
            return false;
        }

        private void raised_event_default_gerarchie_loaded()
        {
            // TODO Program.logger.insert("caricate gerarchie di default");
        }

        private void FormEvent_Load(object sender, EventArgs e)
        {
            // TODO logger.insert($"form caricato");
            formLoadedEvent?.Invoke();
        }
    }
}
