﻿namespace AtgCore
{
    partial class frm_report_form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.sc_main = new System.Windows.Forms.SplitContainer();
            this.lv_ore = new System.Windows.Forms.ListView();
            this.ch_gerarch = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch_ore = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch_ore_dec = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ch_comm = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tlp_graf = new System.Windows.Forms.TableLayoutPanel();
            this.gb_graf_cmd = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.grf_ore = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.sc_main)).BeginInit();
            this.sc_main.Panel1.SuspendLayout();
            this.sc_main.Panel2.SuspendLayout();
            this.sc_main.SuspendLayout();
            this.tlp_graf.SuspendLayout();
            this.gb_graf_cmd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grf_ore)).BeginInit();
            this.SuspendLayout();
            // 
            // sc_main
            // 
            this.sc_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sc_main.Location = new System.Drawing.Point(0, 0);
            this.sc_main.Name = "sc_main";
            this.sc_main.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // sc_main.Panel1
            // 
            this.sc_main.Panel1.Controls.Add(this.lv_ore);
            // 
            // sc_main.Panel2
            // 
            this.sc_main.Panel2.Controls.Add(this.tlp_graf);
            this.sc_main.Size = new System.Drawing.Size(779, 534);
            this.sc_main.SplitterDistance = 174;
            this.sc_main.SplitterWidth = 3;
            this.sc_main.TabIndex = 4;
            // 
            // lv_ore
            // 
            this.lv_ore.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ch_gerarch,
            this.ch_ore,
            this.ch_ore_dec,
            this.ch_comm});
            this.lv_ore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv_ore.FullRowSelect = true;
            this.lv_ore.GridLines = true;
            this.lv_ore.Location = new System.Drawing.Point(0, 0);
            this.lv_ore.MultiSelect = false;
            this.lv_ore.Name = "lv_ore";
            this.lv_ore.ShowGroups = false;
            this.lv_ore.Size = new System.Drawing.Size(779, 174);
            this.lv_ore.TabIndex = 1;
            this.lv_ore.UseCompatibleStateImageBehavior = false;
            this.lv_ore.View = System.Windows.Forms.View.Details;
            this.lv_ore.SelectedIndexChanged += new System.EventHandler(this.lv_ore_SelectedIndexChanged);
            // 
            // ch_gerarch
            // 
            this.ch_gerarch.Text = "Gerarchia";
            this.ch_gerarch.Width = 296;
            // 
            // ch_ore
            // 
            this.ch_ore.Text = "Ore";
            // 
            // ch_ore_dec
            // 
            this.ch_ore_dec.Text = "Ore (decimale)";
            this.ch_ore_dec.Width = 82;
            // 
            // ch_comm
            // 
            this.ch_comm.Text = "Commento";
            this.ch_comm.Width = 522;
            // 
            // tlp_graf
            // 
            this.tlp_graf.ColumnCount = 2;
            this.tlp_graf.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tlp_graf.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlp_graf.Controls.Add(this.grf_ore, 0, 0);
            this.tlp_graf.Controls.Add(this.gb_graf_cmd, 0, 0);
            this.tlp_graf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_graf.Location = new System.Drawing.Point(0, 0);
            this.tlp_graf.Name = "tlp_graf";
            this.tlp_graf.RowCount = 1;
            this.tlp_graf.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_graf.Size = new System.Drawing.Size(779, 357);
            this.tlp_graf.TabIndex = 4;
            // 
            // gb_graf_cmd
            // 
            this.gb_graf_cmd.Controls.Add(this.button4);
            this.gb_graf_cmd.Controls.Add(this.button3);
            this.gb_graf_cmd.Controls.Add(this.button2);
            this.gb_graf_cmd.Controls.Add(this.button1);
            this.gb_graf_cmd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_graf_cmd.Location = new System.Drawing.Point(3, 3);
            this.gb_graf_cmd.Name = "gb_graf_cmd";
            this.gb_graf_cmd.Size = new System.Drawing.Size(94, 351);
            this.gb_graf_cmd.TabIndex = 0;
            this.gb_graf_cmd.TabStop = false;
            this.gb_graf_cmd.Text = "Tipo";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(9, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "Normale";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // grf_ore
            // 
            chartArea2.Name = "ChartArea1";
            this.grf_ore.ChartAreas.Add(chartArea2);
            this.grf_ore.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.grf_ore.Legends.Add(legend2);
            this.grf_ore.Location = new System.Drawing.Point(103, 3);
            this.grf_ore.Name = "grf_ore";
            this.grf_ore.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SemiTransparent;
            this.grf_ore.Size = new System.Drawing.Size(673, 351);
            this.grf_ore.TabIndex = 7;
            this.grf_ore.Text = "Guardami";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(9, 55);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(79, 30);
            this.button2.TabIndex = 1;
            this.button2.Text = "Ordinato";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(9, 91);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(79, 30);
            this.button3.TabIndex = 2;
            this.button3.Text = "Media";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(9, 127);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(79, 30);
            this.button4.TabIndex = 3;
            this.button4.Text = "Cumulativo";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // frm_report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 534);
            this.Controls.Add(this.sc_main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_report";
            this.Text = "Dati!";
            this.sc_main.Panel1.ResumeLayout(false);
            this.sc_main.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sc_main)).EndInit();
            this.sc_main.ResumeLayout(false);
            this.tlp_graf.ResumeLayout(false);
            this.gb_graf_cmd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grf_ore)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer sc_main;
        private System.Windows.Forms.ListView lv_ore;
        private System.Windows.Forms.ColumnHeader ch_gerarch;
        private System.Windows.Forms.ColumnHeader ch_ore;
        private System.Windows.Forms.ColumnHeader ch_ore_dec;
        private System.Windows.Forms.ColumnHeader ch_comm;
        private System.Windows.Forms.TableLayoutPanel tlp_graf;
        private System.Windows.Forms.DataVisualization.Charting.Chart grf_ore;
        private System.Windows.Forms.GroupBox gb_graf_cmd;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}