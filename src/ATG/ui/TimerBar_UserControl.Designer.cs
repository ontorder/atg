﻿namespace AtgCore
{
    partial class TimerBar_UserControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.lst_gerarchia = new System.Windows.Forms.ListBox();
            this.lst_intervalli = new System.Windows.Forms.ListBox();
            this.cms_timerbar = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.eliminaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbl_tot = new System.Windows.Forms.Label();
            this.txt_note = new System.Windows.Forms.TextBox();
            this.lbl_data = new System.Windows.Forms.Label();
            this.btn_salva = new System.Windows.Forms.Button();
            this.cb_playstop = new System.Windows.Forms.CheckBox();
            this.tmr_play = new System.Windows.Forms.Timer(this.components);
            this.cms_timerbar.SuspendLayout();
            this.SuspendLayout();
            // 
            // lst_gerarchia
            // 
            this.lst_gerarchia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            this.lst_gerarchia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lst_gerarchia.FormattingEnabled = true;
            this.lst_gerarchia.Location = new System.Drawing.Point(3, 3);
            this.lst_gerarchia.Name = "lst_gerarchia";
            this.lst_gerarchia.Size = new System.Drawing.Size(172, 41);
            this.lst_gerarchia.TabIndex = 6;
            this.lst_gerarchia.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lst_gerarchia_MouseDoubleClickAsync);
            // 
            // lst_intervalli
            // 
            this.lst_intervalli.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(216)))), ((int)(((byte)(150)))));
            this.lst_intervalli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lst_intervalli.FormattingEnabled = true;
            this.lst_intervalli.Location = new System.Drawing.Point(181, 3);
            this.lst_intervalli.Name = "lst_intervalli";
            this.lst_intervalli.ScrollAlwaysVisible = true;
            this.lst_intervalli.Size = new System.Drawing.Size(88, 41);
            this.lst_intervalli.TabIndex = 7;
            this.lst_intervalli.SelectedIndexChanged += new System.EventHandler(this.lst_intervalli_SelectedIndexChangedAsync);
            this.lst_intervalli.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lst_intervalli_KeyUpAsync);
            this.lst_intervalli.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lst_intervalli_MouseDoubleClickAsync);
            // 
            // cms_timerbar
            // 
            this.cms_timerbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eliminaToolStripMenuItem});
            this.cms_timerbar.Name = "cms_timerbar";
            this.cms_timerbar.Size = new System.Drawing.Size(114, 26);
            // 
            // eliminaToolStripMenuItem
            // 
            this.eliminaToolStripMenuItem.Name = "eliminaToolStripMenuItem";
            this.eliminaToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.eliminaToolStripMenuItem.Text = "Elimina";
            this.eliminaToolStripMenuItem.Click += new System.EventHandler(this.eliminaToolStripMenuItem_Click);
            // 
            // lbl_tot
            // 
            this.lbl_tot.AutoSize = true;
            this.lbl_tot.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lbl_tot.Location = new System.Drawing.Point(299, 3);
            this.lbl_tot.Name = "lbl_tot";
            this.lbl_tot.Size = new System.Drawing.Size(34, 39);
            this.lbl_tot.TabIndex = 11;
            this.lbl_tot.Text = "TOT\r\n4:26\r\n4.483";
            this.lbl_tot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_tot.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbltot_click);
            // 
            // txt_note
            // 
            this.txt_note.BackColor = System.Drawing.Color.White;
            this.txt_note.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_note.Location = new System.Drawing.Point(3, 47);
            this.txt_note.Multiline = true;
            this.txt_note.Name = "txt_note";
            this.txt_note.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_note.Size = new System.Drawing.Size(266, 34);
            this.txt_note.TabIndex = 12;
            this.txt_note.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_note_KeyDown);
            this.txt_note.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_note_KeyPress);
            // 
            // lbl_data
            // 
            this.lbl_data.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_data.Location = new System.Drawing.Point(298, 50);
            this.lbl_data.Name = "lbl_data";
            this.lbl_data.Size = new System.Drawing.Size(38, 28);
            this.lbl_data.TabIndex = 14;
            this.lbl_data.Text = "\r\n";
            this.lbl_data.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_data.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbldata_click);
            // 
            // btn_salva
            // 
            this.btn_salva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(166)))));
            this.btn_salva.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_salva.Location = new System.Drawing.Point(270, 47);
            this.btn_salva.Name = "btn_salva";
            this.btn_salva.Size = new System.Drawing.Size(26, 33);
            this.btn_salva.TabIndex = 15;
            this.btn_salva.Text = "¶";
            this.btn_salva.UseVisualStyleBackColor = false;
            this.btn_salva.Click += new System.EventHandler(this.btn_salva_ClickAsync);
            // 
            // cb_playstop
            // 
            this.cb_playstop.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_playstop.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cb_playstop.Location = new System.Drawing.Point(270, 3);
            this.cb_playstop.Name = "cb_playstop";
            this.cb_playstop.Size = new System.Drawing.Size(26, 41);
            this.cb_playstop.TabIndex = 16;
            this.cb_playstop.Text = "-";
            this.cb_playstop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb_playstop.UseVisualStyleBackColor = true;
            this.cb_playstop.Click += new System.EventHandler(this.cb_playstop_ClickAsync);
            // 
            // tmr_play
            // 
            this.tmr_play.Tick += new System.EventHandler(this.tmr_play_Tick);
            // 
            // TimerBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.cb_playstop);
            this.Controls.Add(this.btn_salva);
            this.Controls.Add(this.lbl_data);
            this.Controls.Add(this.txt_note);
            this.Controls.Add(this.lbl_tot);
            this.Controls.Add(this.lst_intervalli);
            this.Controls.Add(this.lst_gerarchia);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "TimerBar";
            this.Size = new System.Drawing.Size(340, 85);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TimerBar_MouseClick);
            this.cms_timerbar.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lst_gerarchia;
        private System.Windows.Forms.ListBox lst_intervalli;
        private System.Windows.Forms.ContextMenuStrip cms_timerbar;
        private System.Windows.Forms.ToolStripMenuItem eliminaToolStripMenuItem;
        private System.Windows.Forms.Label lbl_tot;
        private System.Windows.Forms.TextBox txt_note;
        private System.Windows.Forms.Label lbl_data;
        private System.Windows.Forms.Button btn_salva;
        private System.Windows.Forms.CheckBox cb_playstop;
        private System.Windows.Forms.Timer tmr_play;
    }
}
