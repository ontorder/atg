﻿using System.Xml;

namespace AtgCore;

public sealed class repo
{
    private void Salva(XmlNode dxn)
    {
        string buri = dxn.BaseURI;

        if (buri.StartsWith("file:///")) buri = buri.Substring(8);
        dxn.OwnerDocument.Save(buri);
    }
}
