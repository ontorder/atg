﻿namespace AtgCore.framework.dependency_injection;

public class provider
{
    private List<object> _registered;

    public provider(List<object> registered)
    {
        _registered = registered;
    }

    public object activate()
    {
        return new();
    }
}