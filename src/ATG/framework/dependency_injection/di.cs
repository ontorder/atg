﻿namespace AtgCore.framework.dependency_injection;

public class di
{
    public List<Type> _registered = new();

    public void register<t_abstraction, t_implementation>()
    {
        _registered.Add(typeof(t_implementation));
    }

    public provider get_provider()
    {
        return new(new(_registered));
    }
}
