﻿namespace AtgCore
{
    public class logger : ilogger {
        public const string ATG_TEXT_MEASURE_STD = "AAAAAAAAAAAAAAAAAAABAAAAAAAAACAAAAAAAAADAAAAAAAAAE";

        private Queue<log_data> mylog;
        private int           MaxRenderTextChars;

        private Form          form_reference;
        private ToolStripItem toolstrip_reference;

        private int cfg_myloglen;
        private int cfg_mylogview;

        private bool grafica_inizializzata;

        public List<Action<string, Color, string>> eventhandler_log;

        private delegate void switch_insert(DateTime timestamp, string log_text, Color log_color);
        private switch_insert insert_internal;

        // -------------- //

        public logger(int loglen, int logview) {
            cfg_myloglen          = loglen;
            cfg_mylogview         = logview;
            mylog                 = new Queue<log_data>();
            grafica_inizializzata = false;
            eventhandler_log      = new List<Action<string, Color, string>>();
            insert_internal       = insert_raw;
        }

        // ---------------

        public void init_ui(Form p_form_reference, ToolStripItem p_toolstrip_reference){
            form_reference      = p_form_reference;
            toolstrip_reference = p_toolstrip_reference;

            float charsize = TextRenderer.MeasureText(ATG_TEXT_MEASURE_STD, toolstrip_reference.Font).Width/(float)50.0;
            int   maxsize  = Screen.GetWorkingArea(form_reference).Width;
            int   nchars   = (int)(maxsize/charsize);

            MaxRenderTextChars    = nchars;
            grafica_inizializzata = true;
            insert_internal       = insert_ui;
        }

        public void insert(string t) =>
            insert(t, Color.Black);

        public void insert(string log_text, Color log_color) =>
            insert_internal(DateTime.Now, log_text, log_color);

        public void insert(DateTime timestamp, string log_text, Color log_color) =>
            insert_internal(timestamp, log_text, log_color);

        public void insert(DateTime timestamp, string log_text) =>
            insert_internal(timestamp, log_text, Color.Black);

        private void insert_ui(DateTime timestamp, string log_text, Color log_color){
            var data = new log_data(timestamp, log_text, log_color);
            log_text = $"[{data.timestamp:HH:mm:ss.fff}] {log_text}";
            mylog.Enqueue(data);
            while (mylog.Count > cfg_myloglen) mylog.Dequeue();

            int log_start = mylog.Count - cfg_mylogview;
            log_start = log_start < 0 ? 0 : log_start;

            var log_count = cfg_mylogview>mylog.Count ? mylog.Count : cfg_mylogview;
            var last_logs = mylog.ToList()
                .GetRange(log_start, log_count)
                .Select(item => {
                    string msg = item.message.Length > MaxRenderTextChars
                        ? item.message.Substring(0, MaxRenderTextChars)
                        : item.message
                    ;
                    return $"[{item.timestamp:HH:mm:ss.fff}] {msg}";
                })
            ;

            raise_log_event(log_text, log_color, String.Join(Environment.NewLine, last_logs));
        }

        private void raise_log_event(string last_text, Color last_text_color, string tooltip_text) {
            foreach (var @event in eventhandler_log) {
                @event(last_text, last_text_color, tooltip_text);
            }
        }

        private void insert_raw(DateTime timestamp, string log_text, Color log_color) =>
            mylog.Enqueue(new log_data(timestamp, log_text, log_color));

        public log_data[] get_all() => mylog.ToArray();
    }
}
