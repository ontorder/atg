﻿namespace AtgCore;

public interface database_row
{
    IReadOnlyDictionary<string, database_field_type_dispatcher> fields { get; }
}
