﻿namespace AtgCore;

public sealed class xml_file_db_settings
{
    public string archive_folder;
    public i_archive_mode_xml_db_driver archive_mode;
    public string table;

    public xml_file_db_settings(string p_table_name, string p_archive_folder, i_archive_mode_xml_db_driver p_archive_mode)
    {
        archive_folder = p_archive_folder;
        archive_mode = p_archive_mode;
        table = p_table_name;
    }
}
