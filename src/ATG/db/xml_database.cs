﻿namespace AtgCore;

using database_fields = IReadOnlyDictionary<string, database_field_type_dispatcher>;

public sealed class xml_database : exec_db_query {
    private readonly metadata_database _db_metadata;
    private readonly Dictionary<string, table_database> _tables = [];
    private readonly Dictionary<string, table_metadata_database> _tables_metadata = [];
    private readonly SortedList<string, xml_file_db_settings> _tables_settings = [];

    public xml_database(metadata_database metadata, xml_file_db_settings[] tables_settings) {
        _db_metadata = metadata;
        foreach (var tm in metadata.tables) _tables_metadata.Add(tm.table_name, tm);
        foreach (var ts in tables_settings) _tables_settings.Add(ts.table, ts);
        foreach (var ts in tables_settings) _tables.Add(ts.table, new xml_file_table(ts, _tables_metadata[ts.table]));
    }

    public IEnumerable<string> enum_tables() => _db_metadata.tables.Select(t => t.table_name);

    // quasi quasi lo strutturerei più come uno script o workflow che come oggetti di macro-logiche
    public async Task<database_query_response> execute_query_async(database_query query) {
        switch (query) {
            case test_get_all_table_query get: {
                var metadata = _tables_metadata[get.table];
                var table = _tables[get.table];
                var rows = await table.get_all_async(default);
                var mapped = rows.Select(row => new xml_database_row(row));
                return new xml_database_query_response(
                    query,
                    Array.Empty<string>(),
                    new[] { new xml_database_dataset(mapped, metadata) }
                );
            }

            case test_level1_join_query get_join: {
                // preparazione dati relazioni
                var metadata = _tables_metadata[get_join.table];
                var my_joins_setup = get_join.joins[0];
                var my_joins = my_joins_setup.joins.Select(ufk => _db_metadata.foreign_keys.First(mfk => mfk.name == ufk.relation_name));

                var file_db = _tables[get_join.table];
                var joins_rows = new Dictionary<string, IEnumerable<database_fields>>();

                // recupero dati da tabelle esterne
                var main_rows = file_db.get_all_asyncenum(default);
                foreach (var table in my_joins) {
                    var join_db = _tables[table.right_table_name];
                    var join_rows = await join_db.get_all_async(default);
                    joins_rows.Add(table.right_table_name, join_rows);
                }

                // eseguo join
                var joined_rows = new List<xml_database_row>();
                await foreach (var main_row in main_rows) {
                    var joins_records = new SortedList<string, IEnumerable<database_fields>>();
                    foreach (var relation in my_joins) {
                        // prendi tabella per questa relazione
                        var tabella_interna_id = relation.left_field_name;
                        var tabella_esterna = joins_rows[relation.right_table_name];

                        // trova campo fk per questa relazione
                        var fk_value = metadata.fields.Single(_ => _.field_name == tabella_interna_id).field_type switch {
                            field_type_database.int32_type => (ulong)main_row[tabella_interna_id].get_int32()!.Value,
                            field_type_database.int64_type => (ulong)main_row[tabella_interna_id].get_int64()!.Value,
                            field_type_database.uint32_type => main_row[tabella_interna_id].get_uint32()!.Value,
                            field_type_database.uint64_type => main_row[tabella_interna_id].get_uint64()!.Value,
                            _ => throw new NotSupportedException()
                        };
                        var join_right_field = _tables_metadata[relation.right_table_name].fields.Single(_ => _.field_name == relation.right_field_name);

                        // estrai record relazione
                        var record_esterni = tabella_esterna.Where(te =>
                            join_right_field.field_type switch {
                                field_type_database.int32_type => (ulong)te[tabella_interna_id].get_int32()!.Value,
                                field_type_database.int64_type => (ulong)te[tabella_interna_id].get_int64()!.Value,
                                field_type_database.uint32_type => te[tabella_interna_id].get_uint32()!.Value,
                                field_type_database.uint64_type => te[tabella_interna_id].get_uint64()!.Value,
                                _ => throw new NotSupportedException()
                            }
                            == fk_value
                        );
                        joins_records.Add(relation.right_table_name, record_esterni);
                    }

                    var joined_field = new SortedList<string, database_field_type_dispatcher>();
                    foreach (var from_field in main_row) {
                        var join_map = get_join.mappings.Where(_ => _.table == get_join.table && _.table_field == from_field.Key);
                        // UNDONE usa nome relazione invece di tabella.campo
                        var field_rename = join_map.Any() ? join_map.Single().as_field : $"{get_join.table}.{from_field.Key}";
                        joined_field.Add(field_rename, from_field.Value);
                    }

                    foreach (var join_records in joins_records) {
                        foreach (var join_record in join_records.Value) {
                            foreach (var join_field in join_record) {
                                var remappings = get_join.mappings.Where(_ => _.table == join_records.Key && _.table_field == join_field.Key);
                                var field_rename = remappings.Any() ? remappings.Single().as_field : $"{join_records.Key}.{join_field.Key}";
                                joined_field.Add(field_rename, join_field.Value);
                            }
                        }
                    }
                    joined_rows.Add(new xml_database_row(joined_field));
                }
                var joined_metadata = new table_metadata_database("result", [], metadata.id_definition);

                return new xml_database_query_response(
                    query,
                    Array.Empty<string>(),
                    new[] { new xml_database_dataset(joined_rows, joined_metadata) }
                );
            }

            default:
                throw new NotImplementedException();
        }
    }

    private void create_memory_table(table_metadata_database table) {
        if (_tables_metadata.ContainsKey(table.table_name))
            throw new Exception($"a table with name '{table.table_name}' already exists");
        _tables_metadata.Add(table.table_name, table);
        _tables.Add(table.table_name, new memory_table(table));
    }

    private void delete_memory_table(string table) {
        if (!_tables.ContainsKey(table)) throw new Exception($"table {table} doesn't exist");
        if (_tables[table] is memory_table) {
            // TODO check vari nel caso in cui tabella abbia relazioni...
            _tables_metadata.Remove(table);
            _tables.Remove(table);
        }
        else
            throw new InvalidOperationException($"table '{table}' is no deletable");
    }
}
