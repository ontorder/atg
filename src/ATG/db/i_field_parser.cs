﻿namespace AtgCore
{
    public interface i_field_parser
    {
        int parse_int_field(string fieldName);
        string parse_string_field(string fieldName);
        double parse_double_field(string fieldName);
        DateTime parse_datetime_field(string fieldName);
        bool has_field(string fieldName);
    }
}
