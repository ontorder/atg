﻿namespace AtgCore;

public sealed class memory_table : table_database
{
    public table_metadata_database table_metadata { get; }

    public memory_table(table_metadata_database p_table_metadata) => table_metadata = p_table_metadata;

    public Task<ICollection<IReadOnlyDictionary<string, database_field_type_dispatcher>>> get_all_async(CancellationToken cancellation)
    {
        throw new NotImplementedException();
    }

    public IAsyncEnumerable<IReadOnlyDictionary<string, database_field_type_dispatcher>> get_all_asyncenum(CancellationToken cancellation)
    {
        throw new NotImplementedException();
    }
}
