﻿namespace AtgCore;

public sealed class query_context<TData> where TData : class
{
    private readonly exec_db_query executor;
    private readonly metadata_database metadata;
    private readonly string metadataId;
    private readonly data_parsers parsers;
    private readonly table_metadata_database table;

    public query_context(metadata_database p_metadata, string p_metadataId, data_parsers p_parsers, exec_db_query p_executor)
    {
        metadata = p_metadata;
        metadataId = p_metadataId;
        parsers = p_parsers;
        executor = p_executor;
        table = metadata.tables.Single(_ => _.table_name == metadataId);
    }

    public async Task add_async(TData record)
    {

    }

    public async Task<IEnumerable<TData>> get_async()
    {
        var ds = await executor.execute_query_async(new test_get_all_table_query(table.table_name));
        var parser = ((context_parser<TData>)parsers.parsers[metadataId]).parse;
        return ds.datasets.First().Value.rows.Select(_ => parser(_, null));
    }

    public async Task<IEnumerable<TData>> get_test_async(ICollection<context_data_graph_setup>? graph_setup)
    {
        var mappings = new List<field_remap>();
        foreach (var field in table.fields)
            mappings.Add(new(table.table_name, field.field_name, p_as_field: field.field_name));

        // UNDONE questo sarebbe solo un livello di fk; dovrei passare tipo tutto
        // NO in realtà tutto no perché ci son relazioni inverse che creano circolarità
        // o meglio, andrei a prendere tutto il db
        // bisogna per forza specificare le relazioni da fuori
        if (graph_setup?.Count > 0)
        {
            var relations = graph_setup.Where(setup => setup.is_empty() == false).SelectMany(_ => _.list_includes()!);
            var fks = metadata.foreign_keys.Where(fk => relations.Contains(fk.name));
            if (fks.Any())
            {
                foreach (var fk in fks)
                    foreach (var field in metadata.tables.Single(_ => _.table_name == fk.right_table_name).fields)
                        mappings.Add(new(fk.right_table_name, field.field_name, $"{fk.right_table_name}.{field.field_name}"));
            }
        }
        //var ds = await executor.execute_query_async(new test_join_query(table.table_name, fks.Select(_ => _.name).ToArray(), mappings));
        //var ds_default = ds.datasets.First().Value;
        //var rows = ds_default.rows;

        var parseds = new List<TData>();
        //var fksName = fks.Select(_ => _.name).ToArray();
        //foreach (var row in rows)
        {
            var parser = (context_parser<TData>)parsers.parsers[metadataId];

            //foreach (var fk in fks)
            {
                // qui dovrei parsare gli altri campi per il relativo modello
                // però dovrei tenere uno stato con la (le?) chiave usata nella join e l'oggetto left-value/esterno
                // in modo da poi accedere al suo campo e assegnargli l'oggetto parsato
                // ma dovrei fare un parser per la fk?
            }
            //parseds.Add(parser.parse(row, (fksName, mappings)));
        }
        return parseds;
    }

    public async Task<TData> get_async(object id)
    {
        return default;
    }

    public async Task delete_async(object id)
    {

    }

    public async Task update_async(object stuff)
    {
    }

    // where dovrebbe essere un parametro di get? (gets?)
    public async Task<IEnumerable<TData>> where_async(object filter)
    {
        return default;
    }
}
