﻿namespace AtgCore;

public record join_data(string relation_name, int? next_join_node_id);
