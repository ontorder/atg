﻿namespace AtgCore;

public class field_remap
{
    public string as_field;
    public string table;
    public string table_field;

    public field_remap(string p_table, string p_table_field, string p_as_field)
    {
        table = p_table;
        table_field = p_table_field;
        as_field = p_as_field;
    }
}
