﻿namespace AtgCore;

public class xml_database_row : database_row
{
    // TODO SortedList
    public IReadOnlyDictionary<string, database_field_type_dispatcher> fields { get; }

    public xml_database_row(IReadOnlyDictionary<string, database_field_type_dispatcher> p_fields) => fields = p_fields;
}
