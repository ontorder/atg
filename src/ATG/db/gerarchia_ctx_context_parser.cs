﻿namespace AtgCore;

public class gerarchia_ctx_context_parser : context_parser<gerarchia_ctx>
{
    private readonly metadata_database metadata;
    private readonly string metadataId;
    private readonly atg_parsers parsers;

    public gerarchia_ctx_context_parser(metadata_database p_metadata, string p_metadataId, atg_parsers p_parsers)
    {
        metadata = p_metadata;
        metadataId = p_metadataId;
        parsers = p_parsers;
    }

    public gerarchia_ctx parse(database_row row, (string[] foreign_keys, ICollection<field_remap> mappings)? include_context = null)
    {
        string gerarchia_id_field = include_context?.mappings.SingleOrDefault(m => m.table == metadataId && m.table_field == "gerarchia_id")?.as_field ?? "gerarchia_id";
        string nome_field = include_context?.mappings.SingleOrDefault(m => m.table == metadataId && m.table_field == "nome")?.as_field ?? "nome";

        var gerarchia_ctx = new gerarchia_ctx();
        gerarchia_ctx.gerarchia_id = row.fields[gerarchia_id_field].get_int32().Value;
        gerarchia_ctx.nome = row.fields[nome_field].get_string();

        if (include_context.Value.foreign_keys.Contains("g_gd"))
            gerarchia_ctx.gerarchia_default = parsers.gerarchia_default_parser.parse(row, include_context);

        if (include_context.Value.foreign_keys.Contains("g_gp"))
            gerarchia_ctx.gerarchia_preferita = parsers.gerarchia_preferita_parser.parse(row, include_context);

        if (include_context.Value.foreign_keys.Contains("g_gt"))
            gerarchia_ctx.gerarchia_termini = new[] { parsers.gerarchia_termine_parser.parse(row, include_context) };

        if (include_context.Value.foreign_keys.Contains("g_i"))
            gerarchia_ctx.intervalli = new[] { parsers.intervallo_parser.parse(row, include_context) };
        return gerarchia_ctx;
    }
}
