﻿namespace AtgCore;

public interface i_action_database { }
public interface i_source_database { }
public interface i_table_source : i_source_database { }
public interface i_right_value { }

// TODO tracked dovrebbe essere implementato da context? o comunque da un livello superiore a questo
// TODO lo stesso vale per yieldable/IEnumerableAsync, ma quanto dovrei reimplementare?

public sealed class test_get_all_table_query : database_query
{
    public string table;

    public test_get_all_table_query(string p_table) => table = p_table;
}

// TODO come gestisco contesto, variabili, funzioni? li metto a parte? uso un array di funzioni?
// TODO order, group, +join
// espressione sarebbe una serie di operazioni senza storage, in pratica? tutte immediate?
// tabelle sarebbero da considerare le unità base del db?
// TODO expression_table sarebbe la select, quindi un 'immediato'
public class actions_query : database_query
{
    public ICollection<i_action_database> actions;
    public actions_query(ICollection<i_action_database> p_actions) => actions = p_actions;
}

public class remap_field
{
    public field_database remapped_field;
    public string table_field;
    public string table_name;

    public remap_field(field_database p_remapped_field, string p_table_field, string p_table_name) =>
        (remapped_field, table_field, table_name) = (p_remapped_field, p_table_field, p_table_name);
}
public class expression_table_action : i_action_database
{
    public object[] fields;
    public (string field, i_action_database[] actions)[] fields_actions; // remap o espressioni (tra cui assign)
    public string source_name;

    public expression_table_action(object[] p_fields, (string, i_action_database[])[] p_fields_actions, string p_source_name)
    {
        fields = p_fields;
        fields_actions = p_fields_actions;
        source_name = p_source_name;
    }
}
public class declare_dataset_result_table : i_action_database
{
    public string table_name;

    public declare_dataset_result_table(string p_table_name) => table_name = p_table_name;
}
public class table_source_action : i_action_database
{
    public string table_name;

    public table_source_action(string p_table_name) => table_name = p_table_name;
}
public class join_tables_source_action : i_action_database
{
    public (string left_field, string right_field)[] join_links;
    public string[] tables;

    public join_tables_source_action(string[] p_tables, (string left_field, string right_field)[] p_join_likns)
    {
        tables = p_tables;
        join_links = p_join_likns;
    }
}

public class loop_action : i_action_database
{
    public object condition_expression;
    // TODO
    public loop_action() => throw new NotImplementedException();
}
public class index_datasource_loop_action : i_action_database
{
    public object condition_expression;
    public int index_start;
    public int index_end;
}
public class all_elements_datasource_loop_action : i_action_database
{
}
public class call_function : i_action_database
{
    public string function_name;
    public object[]? parameters;
}
// non sarebbe una i_action_database in realtà
// perché è a questo punto è implicito che io abbia un main()
// quindi dovrei passare un array di funzioni alla query
public class define_function_action
{
    public i_action_database[] actions;
    public string function_name;
    public (string, object)[]? parameters;
    public (string, object)[]? return_values;
}
// scope...
public class actions_block
{
    public i_action_database[] actions;
}

// attenzione ora che voglio implementare loop, assign dovrebbe servire per assegnare variabili, record, ecc
// lo uso comunque per assegnare anche tabelle?
public class assign_action : i_action_database
{
    public string left_value;
    public string right_value;
    public assign_action(string p_left_value, string p_right_value) => (left_value, right_value) = (p_left_value, p_right_value);
}


public enum table_storage_class { temporary, stored }
public enum table_context_class { global, local }
public class create_table_action : i_action_database
{
    public table_storage_class storage_class = table_storage_class.temporary;
    public table_context_class context_class = table_context_class.local;
    public object[] fields;
    public string table_name;
    public create_table_action(object[] p_fields, string p_table_name) => (fields, table_name) = (p_fields, p_table_name);
}
public class delete_table_action : i_action_database
{
    public string table_name;
    public delete_table_action(string p_table_name) => table_name = p_table_name;
}
public class clear_table_action : i_action_database
{
    public string table_name;
    public clear_table_action(string p_table_name) => table_name = p_table_name;
}

public interface expression
{

}
public interface i_operation { }
public class sum_operation : i_operation { }
public class multiply_operation : i_operation { }
public class constant_operand
{
    public object type;
    public object value;

    public constant_operand(object p_type, object p_value) => (type, value) = (p_type, p_value);
}
public class variable_operand
{
    public object initial_value;
    public object type;

    public variable_operand(object p_type, object p_initial_value) => (type, initial_value) = (p_type, p_initial_value);
}

