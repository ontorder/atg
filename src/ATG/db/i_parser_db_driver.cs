﻿namespace AtgCore;

public interface i_parser_db_driver
{
    int parse_int_field(string? field_name);
    string parse_string_field(string? field_name);
    double parse_double_field(string? field_name);
    DateTime parse_datetime_field(string? field_name);
    bool has_field(string? field_name);
}
