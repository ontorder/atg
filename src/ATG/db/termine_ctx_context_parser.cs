﻿namespace AtgCore
{
    public class termine_ctx_context_parser : context_parser<termine_ctx>
    {
        private readonly metadata_database metadata;
        private readonly string metadataId;
        private readonly atg_parsers _parsers;

        public termine_ctx_context_parser(metadata_database p_metadata, string p_metadataId, atg_parsers p_parsers)
        {
            metadata = p_metadata;
            metadataId = p_metadataId;
            _parsers = p_parsers;
        }

        string get_remapped_field(ICollection<field_remap> mappings, string field_name) =>
            mappings.SingleOrDefault(m => m.table == metadataId && m.table_field == field_name)?.as_field ?? field_name;

        public termine_ctx parse(database_row row, (string[] foreign_keys, ICollection<field_remap> mappings)? include_context = null)
        {
            string termine_id_field = include_context.HasValue ? get_remapped_field(include_context.Value.mappings, "termine_id") : "termine_id";
            string nome_field = include_context.HasValue ? get_remapped_field(include_context.Value.mappings, "nome") : "nome";

            gerarchia_termine_ctx[]? gerarchie_termine = null;
            if (include_context.Value.foreign_keys.Contains("t_gt"))
            {
                var gt = _parsers.gerarchia_termine_parser.parse(row, include_context);
                gerarchie_termine = new[] { gt };
            }
            return new termine_ctx(
                row.fields[termine_id_field].get_int32().Value,
                row.fields[nome_field].get_string(),
                gerarchie_termine
            );
        }
    }
}
