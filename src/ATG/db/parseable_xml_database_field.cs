﻿using System.Xml;

namespace AtgCore;

public class parseable_xml_database_field : database_field_type_dispatcher
{
    public object field_value { get; }
    private XmlNode _node;

    public parseable_xml_database_field(XmlNode node, field_storage_database storage)
    {
        _node = node;
        switch (storage)
        {
            case field_storage_database.inner_text: field_value = node.InnerText; break;
            case field_storage_database.inner_element: field_value = node.InnerText; break;
            case field_storage_database.node_attribute: field_value = node.Value; break;
            default: throw new Exception();
        }
    }

    public DateTime? get_datetime()
    {
        throw new NotImplementedException();
    }

    public float? get_float32()
    {
        throw new NotImplementedException();
    }

    public double? get_float64()
    {
        throw new NotImplementedException();
    }

    public int? get_int32()
    {
        throw new NotImplementedException();
    }

    public long? get_int64()
    {
        throw new NotImplementedException();
    }

    public string? get_string()
    {
        throw new NotImplementedException();
    }

    public TimeSpan? get_timespan()
    {
        throw new NotImplementedException();
    }

    public uint? get_uint32()
    {
        throw new NotImplementedException();
    }

    public ulong? get_uint64()
    {
        throw new NotImplementedException();
    }

    public bool? get_bool()
    {
        throw new NotImplementedException();
    }
}
