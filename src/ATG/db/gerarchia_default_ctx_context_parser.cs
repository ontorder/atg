﻿namespace AtgCore
{
    public class gerarchia_default_ctx_context_parser : context_parser<gerarchia_default_ctx>
    {
        public gerarchia_default_ctx parse(database_row row, (string[] foreign_keys, ICollection<field_remap> mappings)? include_context = null)
        {
            var gd = new gerarchia_default_ctx();
            gd.gerarchia_default_id = row.fields["gerarchia_default_id"].get_int32().Value;
            return gd;
        }
    }
}
