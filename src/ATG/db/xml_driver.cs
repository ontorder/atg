﻿using System.Runtime.CompilerServices;
using System.Xml;

namespace ATG;

public sealed class xml_driver {
    private readonly string _arhive_path_filename;

    public xml_driver(string path_name)
        => _arhive_path_filename = path_name;

    public async Task<List<xml_element>> get_elements_async(CancellationToken cancellation) {
        using var reader = create_reader();
        while (reader.Depth == 0) if (await reader.ReadAsync() == false) break;

        var elements = new List<xml_element>();
        var canread = true;
        do {
            while (canread && reader.NodeType != XmlNodeType.Element) if (await reader.ReadAsync() == false) canread = false;
            if (canread == false) break;
            var parsed = await parse_element_async(reader);
            elements.Add(parsed);
        }
        while (canread);
        return elements;
    }

    public async IAsyncEnumerable<xml_element> get_elements_asyncenum([EnumeratorCancellation] CancellationToken cancellation) {
        using var reader = create_reader();
        while (reader.Depth == 0) if (await reader.ReadAsync() == false) yield break;

        do {
            while (reader.NodeType != XmlNodeType.Element) if (await reader.ReadAsync() == false) yield break;
            var parsed = await parse_element_async(reader);
            yield return parsed;
        }
        while (true);
    }

    private XmlReader create_reader()
        => XmlReader.Create(_arhive_path_filename, new XmlReaderSettings { Async = true });

    private async Task<xml_element> parse_element_async(XmlReader reader) {
        List<(string, string)>? attrs = null;
        List<xml_element>? elements = null;
        string? innertext = null;
        var name = reader.LocalName;
        int myDepth = reader.Depth;

        if (reader.NodeType != XmlNodeType.Element)
            throw new InvalidOperationException();

        if (reader.HasAttributes) {
            attrs ??= [];
            while (reader.MoveToNextAttribute()) attrs.Add((reader.LocalName, reader.Value));
        }

        do {
            if (false == await move_to_content_async(reader)) break;

            if (reader.NodeType == XmlNodeType.Element && reader.Depth > myDepth) {
                elements ??= [];
                elements.Add(await parse_element_async(reader));
            }

            if (reader.NodeType == XmlNodeType.Text)
                innertext = await reader.GetValueAsync();

            // PROBLEMA dovrei o mettere _reader.ReadAsync() in una macchina a stati o comunque proteggerlo da qualche if
        } while (reader.Depth != myDepth || reader.NodeType != XmlNodeType.EndElement);
        return new xml_element(innertext, attrs?.ToArray(), elements?.ToArray(), name);
    }

    async Task<bool> move_to_content_async(XmlReader reader) {
        do {
            if (await reader.ReadAsync() == false) return false;
            if (reader.NodeType == XmlNodeType.EndElement) return false;
            if (reader.NodeType == XmlNodeType.Element
                // non raggiungibile da ReadAsync a quanto pare || reader.NodeType == XmlNodeType.Attribute
                || reader.NodeType == XmlNodeType.Text)
                return true;
        } while (true);
    }
}
public sealed class xml_element {
    public (string name, string attributevalue)[]? attributes;
    public xml_element[]? children;
    public string? innertext;
    public string name;

    public xml_element(string? p_innertext, (string name, string rvalue)[]? p_attributes, xml_element[]? p_children, string name) {
        innertext = p_innertext;
        attributes = p_attributes;
        children = p_children;
        this.name = name;
    }
}
