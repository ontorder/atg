﻿namespace AtgCore;

public enum database_field_type { int32_type, float32_type, float64_type, string_type, int64_type, datetime_type, timespan_type, uint32_type, uint64_type }
