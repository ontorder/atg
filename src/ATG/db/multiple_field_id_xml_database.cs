﻿namespace AtgCore
{
    public class multiple_field_id_xml_database : i_id_definition_database
    {
        public string[] fields_name;

        public multiple_field_id_xml_database(string[] p_fields_name) => fields_name = p_fields_name;
    }
}
