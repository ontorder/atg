﻿namespace AtgCore;

public class intervallo_ctx_context_parser : context_parser<intervallo_ctx>
{
    public intervallo_ctx parse(database_row row, (string[] foreign_keys, ICollection<field_remap> mappings)? include_context = null)
    {
        return new intervallo_ctx(
            row.fields["intervallo_id"].get_int32().Value,
            row.fields["inizio"].get_datetime().Value,
            row.fields["fine"].get_datetime().Value,
            row.fields["commento"].get_string(),
            null // parser... da trovare automaticamente tipo injection?;
        );
    }
}
