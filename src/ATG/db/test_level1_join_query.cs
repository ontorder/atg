﻿namespace AtgCore;

public sealed class test_level1_join_query : database_query
{
    // usare solo fk non permette di fare join di tutto il grafo delle relazioni
    // usare solo nomi relazione non permette di fare join arbitrarie
    // TODO serve nome relazione + arbitraria
    // rinomina di campi dev'essere esterna, non va bene dentro a join_data perché relazione potrebbe essere su più campi ecc
    public join_node[] joins;
    public ICollection<field_remap> mappings;
    public string table;

    public test_level1_join_query(string p_table, join_node[] p_joins, ICollection<field_remap> p_mappings)
    {
        table = p_table;
        joins = p_joins;
        mappings = p_mappings;
    }
}
