﻿namespace AtgCore;

public interface database_query_response
{
    IReadOnlyDictionary<string, database_dataset> datasets { get; }
    database_query query { get; }
    string[]? warnings { get; }
}
