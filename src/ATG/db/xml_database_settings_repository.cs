﻿namespace AtgCore;

public class xml_database_settings_repository
{
    public static xml_file_db_settings[] get_atg_settings(string data_folder)
    {
        return new[]
        {
            new xml_file_db_settings("gerarchie", data_folder, new single_archive_xml_db_driver("gerarchie.xml")),
            new xml_file_db_settings("gerarchie_default", data_folder, new single_archive_xml_db_driver("default.xml")),
            new xml_file_db_settings("gerarchie_preferite", data_folder, new single_archive_xml_db_driver("preferite.xml")),
            new xml_file_db_settings("gerarchie_termini", data_folder, new single_archive_xml_db_driver("gerarchie_termini.xml")),
            new xml_file_db_settings("intervalli", data_folder, new sharded_archive_xml_db_driver(999)),
            new xml_file_db_settings("termini", data_folder, new single_archive_xml_db_driver("termini.xml"))
        };
    }
}
