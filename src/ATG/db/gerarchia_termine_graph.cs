﻿namespace AtgCore;

public sealed class gerarchia_termine_graph : context_data_graph_setup
{
    public void include_gerarchia() => add("gt_g");

    public void include_termine() => add("gt_t");
}