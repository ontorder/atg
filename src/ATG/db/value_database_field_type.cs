﻿namespace AtgCore;

public class value_database_field_type : database_field_type_dispatcher
{
    public object? field_value { get; }

    public value_database_field_type(object? parsed_value) => field_value = parsed_value;

    public DateTime? get_datetime() => (DateTime?)field_value;

    public float? get_float32() => (float?)field_value;

    public double? get_float64() => (double?)field_value;

    public int? get_int32() => (int?)field_value;

    public long? get_int64() => (long?)field_value;

    public string? get_string() => (string?)field_value;

    public TimeSpan? get_timespan() => (TimeSpan?)field_value;

    public uint? get_uint32() => (uint?)field_value;

    public ulong? get_uint64() => (ulong?)field_value;

    public bool? get_bool() => (bool?)field_value;
}
public class value_database_field_type2 : i2
{
    public i3 value{ get; }

    public value_database_field_type2(i3 p_value) => value = p_value;

    public bool get_bool() => ((database_field_type_generic<bool>)value).field_value;

    public DateTime get_datetime() => default;

    public float get_float32() => default;

    public double get_float64() => default;

    public int get_int32() => default;

    public long get_int64()
    {
        throw new NotImplementedException();
    }

    public string get_string()
    {
        throw new NotImplementedException();
    }

    public TimeSpan get_timespan()
    {
        throw new NotImplementedException();
    }

    public uint get_uint32()
    {
        throw new NotImplementedException();
    }

    public ulong get_uint64()
    {
        throw new NotImplementedException();
    }
}
