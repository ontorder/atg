﻿namespace AtgCore;

public class single_archive_xml_db_driver : i_archive_mode_xml_db_driver
{
    public string archive_filename;

    public single_archive_xml_db_driver(string p_archive_filename) => archive_filename = p_archive_filename;
}
