﻿namespace AtgCore;

public class xml_database_dataset : database_dataset
{
    public IEnumerable<database_row> rows { get; }
    public table_metadata_database? schema { get; }

    public xml_database_dataset(IEnumerable<database_row> p_rows, table_metadata_database? p_schema)
    {
        rows = p_rows;
        schema = p_schema;
    }
}
