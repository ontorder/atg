﻿namespace AtgCore;

public class gerarchia_preferita_ctx_context_parser : context_parser<gerarchia_preferita_ctx>
{
    public gerarchia_preferita_ctx parse(database_row row, (string[] foreign_keys, ICollection<field_remap> mappings)? include_context = null)
    {
        return new gerarchia_preferita_ctx(
            row.fields["gerarchia_preferita_id"].get_int32().Value,
            null,
            row.fields["conteggio"].get_int32().Value
        );
    }
}
