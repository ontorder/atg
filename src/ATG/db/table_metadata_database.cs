﻿namespace AtgCore;

public class table_metadata_database
{
    public field_database[] fields;
    public i_id_definition_database? id_definition;
    public string table_name;

    public table_metadata_database(string p_table_name, field_database[] p_fields, i_id_definition_database? p_id_definition)
    {
        table_name = p_table_name;
        fields = p_fields;
        id_definition = p_id_definition;
    }
}
