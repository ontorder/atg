﻿namespace AtgCore
{
    public class single_field_id_xml_database : i_id_definition_database
    {
        public string field_name;

        public single_field_id_xml_database(string p_field_name) => field_name = p_field_name;
    }
}
