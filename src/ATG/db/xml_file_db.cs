﻿using ATG;
using System.Runtime.CompilerServices;

namespace AtgCore;

public sealed class xml_file_table : table_database {
    public xml_file_db_settings settings;
    public table_metadata_database table_metadata { get; }

    public xml_file_table(xml_file_db_settings p_settings, table_metadata_database p_table_metadata) {
        settings = p_settings;
        table_metadata = p_table_metadata;
    }

    public async Task<ICollection<IReadOnlyDictionary<string, database_field_type_dispatcher>>> get_all_async(CancellationToken cancellation) {
        var archive = settings.archive_mode as single_archive_xml_db_driver;
        var temp_path = Path.Combine(settings.archive_folder, archive.archive_filename);
        var xmld = new xml_driver(temp_path);
        var records = await xmld.get_elements_async(cancellation);
        return records.Select(parse_record).ToArray();
    }

    public async IAsyncEnumerable<IReadOnlyDictionary<string, database_field_type_dispatcher>> get_all_asyncenum([EnumeratorCancellation] CancellationToken cancellation) {
        var archive = settings.archive_mode as single_archive_xml_db_driver;
        var temp_path = Path.Combine(settings.archive_folder, archive.archive_filename);
        var xmld = new xml_driver(temp_path);
        await foreach (var raw_el in xmld.get_elements_asyncenum(cancellation))
            yield return parse_record(raw_el);
    }

    protected async Task<xml_driver> load_async() {
        if (settings.archive_mode is single_archive_xml_db_driver single_file) {
            var fp = Path.Combine(settings.archive_folder, single_file.archive_filename);
            return new(fp);
        }

        throw new NotImplementedException();
    }

    private object? parse_field(xml_element node_field, field_database metadata) {
        // UNDONE nullable
        var to_parse = metadata.field_storage switch {
            field_storage_database.inner_text => node_field.innertext,
            field_storage_database.inner_element => node_field.children.First(_ => _.name == metadata.field_name).innertext,
            field_storage_database.node_attribute => node_field.attributes.First(_ => _.name == metadata.field_name).attributevalue,
            _ => throw new NotImplementedException(),
        };

        if (to_parse == null) {
            if (metadata.is_nullable)
                return null;
            throw new ArgumentNullException(metadata.field_name);
        }

        return metadata.field_type switch {
            field_type_database.datetime_type => DateTime.Parse(to_parse),
            field_type_database.datetimeoffset_type => DateTimeOffset.Parse(to_parse),
            field_type_database.float32_type => Single.Parse(to_parse),
            field_type_database.float64_type => Double.Parse(to_parse),
            field_type_database.int32_type => Int32.Parse(to_parse),
            field_type_database.int64_type => Int64.Parse(to_parse),
            field_type_database.string_type => to_parse,
            field_type_database.timespan_type => TimeSpan.Parse(to_parse),
            field_type_database.uint32_type => UInt32.Parse(to_parse),
            field_type_database.uint64_type => UInt64.Parse(to_parse),
            field_type_database.bool_type => bool.Parse(to_parse),
            _ => throw new NotImplementedException(),
        };
    }

    private IReadOnlyDictionary<string, database_field_type_dispatcher> parse_record(xml_element raw_el) {
        var parsed = new SortedList<string, database_field_type_dispatcher>();
        foreach (var campo in table_metadata.fields) {
            var campo_value = parse_field(raw_el, campo);
            parsed.Add(campo.field_name, new value_database_field_type(campo_value));
        }
        return parsed;
    }

    private void put(field_database[] record) {

    }
}
