﻿namespace AtgCore
{
    public class gerarchia_termine_ctx_context_parser : context_parser<gerarchia_termine_ctx>
    {
        private readonly metadata_database metadata;
        private readonly string metadataId;
        private readonly atg_parsers _parsers;

        public gerarchia_termine_ctx_context_parser(metadata_database p_metadata, string p_metadataId, atg_parsers p_parsers)
        {
            metadata = p_metadata;
            metadataId = p_metadataId;
            _parsers = p_parsers;
        }

        string get_remapped_field(ICollection<field_remap> mappings, string field_name) =>
            mappings.SingleOrDefault(m => m.table == metadataId && m.table_field == field_name)?.as_field ?? field_name;

        public gerarchia_termine_ctx parse(database_row row, (string[] foreign_keys, ICollection<field_remap> mappings)? include_context = null)
        {
            string peso_field = include_context.HasValue ? get_remapped_field(include_context.Value.mappings, "peso") : "peso";

            gerarchia_ctx? g_g = null;
            if (include_context.Value.foreign_keys.Contains("gt_g"))
                g_g = _parsers.gerarchia_parser.parse(row, include_context);

            termine_ctx? g_t = null;
            if (include_context.Value.foreign_keys.Contains("gt_t"))
                g_t = _parsers.termine_parser.parse(row, include_context);

            return new gerarchia_termine_ctx(g_g, row.fields[peso_field].get_float32().Value, g_t);
        }
    }
}
