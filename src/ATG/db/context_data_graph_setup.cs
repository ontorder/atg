﻿namespace AtgCore;

public abstract class context_data_graph_setup
{
    private List<string>? includes;

    public bool is_empty() => includes?.Any() == true;

    public ICollection<string>? list_includes() => includes;

    protected void add(string s)
    {
        includes ??= new();
        includes.Add(s);
    }
}
