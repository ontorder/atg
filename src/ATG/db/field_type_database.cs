﻿namespace AtgCore;

public enum field_type_database {
    int32_type, int64_type, uint32_type, uint64_type,
    timespan_type, datetime_type, datetimeoffset_type,
    float32_type, float64_type,
    string_type,
    bool_type
}
