﻿namespace AtgCore;

// TODO un field potrebbe anche essere un valore statico o un'espressione
// TODO unique/id
public class field_database
{
    public string field_name;
    public field_storage_database field_storage;
    public field_type_database field_type;
    public bool is_nullable;

    public field_database(string p_field_name, field_type_database p_field_type, field_storage_database p_field_storage, bool p_is_nullable)
    {
        field_name = p_field_name;
        field_type = p_field_type;
        field_storage = p_field_storage;
        is_nullable = p_is_nullable;
    }
}
