﻿namespace AtgCore;

public interface table_database
{
    public table_metadata_database table_metadata { get; }
    Task<ICollection<IReadOnlyDictionary<string, database_field_type_dispatcher>>> get_all_async(CancellationToken cancellation);
    IAsyncEnumerable<IReadOnlyDictionary<string, database_field_type_dispatcher>> get_all_asyncenum(CancellationToken cancellation);
}