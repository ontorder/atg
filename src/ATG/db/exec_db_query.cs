﻿namespace AtgCore;

public interface exec_db_query
{
    Task<database_query_response> execute_query_async(database_query query);
}
