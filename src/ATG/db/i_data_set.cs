﻿namespace AtgCore
{
    public interface i_data_set
    {
        IEnumerable<i_parser_db_driver> get_rows();
    }
}