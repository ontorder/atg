﻿namespace AtgCore;

public interface database_field_type_dispatcher {
    object? field_value { get; }
    int? get_int32();
    uint? get_uint32();
    float? get_float32();
    double? get_float64();
    string? get_string();
    long? get_int64();
    ulong? get_uint64();
    DateTime? get_datetime();
    TimeSpan? get_timespan();
    bool? get_bool();
}
public interface i3 { }
public interface database_field_type_generic<T> : i3 {
    T field_value { get; }
}
public interface i2 {
    i3 value { get; }
    int get_int32();
    uint get_uint32();
    float get_float32();
    double get_float64();
    string get_string();
    long get_int64();
    ulong get_uint64();
    DateTime get_datetime();
    TimeSpan get_timespan();
    bool get_bool();
}