﻿namespace AtgCore;

public record join_node(int join_node_id, join_data[] joins);
