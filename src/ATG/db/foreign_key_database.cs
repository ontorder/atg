﻿namespace AtgCore;

public class foreign_key_database
{
    public string name;
    public string left_table_name;
    public string left_field_name;
    public string right_table_name;
    public string right_field_name;
    // cascade { delete none ecc }

    public foreign_key_database(string p_name, string p_left_table_name, string p_left_field_name, string p_right_table_name, string p_right_field_name)
    {
        left_field_name = p_left_field_name;
        right_table_name = p_right_table_name;
        right_field_name = p_right_field_name;
        left_table_name = p_left_table_name;
        name = p_name;
    }
}
