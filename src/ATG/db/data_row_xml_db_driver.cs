﻿using System.Xml;

namespace AtgCore;

public sealed class data_row_xml_db_driver : i_parser_db_driver
{
    private XmlElement _node;

    public data_row_xml_db_driver(XmlElement node) => _node = node;

    public bool has_field(string? field_name)
    {
        return field_name == null | _node.Attributes[field_name] != null;
    }

    private string get_field(string? field_name)
    {
        if (field_name == null) return _node.InnerText;
        return _node.Attributes[field_name].Value;
    }

    public DateTime parse_datetime_field(string? field_name)
    {
        return DateTime.Parse(get_field(field_name));
    }

    public double parse_double_field(string? field_name)
    {
        return Double.Parse(get_field(field_name));
    }

    public int parse_int_field(string? field_name)
    {
        return Int32.Parse(get_field(field_name));
    }

    public string parse_string_field(string? field_name)
    {
        return get_field(field_name);
    }
}
