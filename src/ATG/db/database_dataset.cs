﻿namespace AtgCore;

public interface database_dataset
{
    IEnumerable<database_row> rows { get; }
    table_metadata_database? schema { get; }
}
