﻿namespace AtgCore;

public class xml_database_query_response : database_query_response
{
    public IReadOnlyDictionary<string, database_dataset> datasets { get; }
    public database_query query { get; }
    public string[]? warnings { get; }

    public xml_database_query_response(database_query p_query, string[] p_warnings, database_dataset p_dataset)
    {
        query = p_query;
        warnings = p_warnings;
        var ds = new SortedList<string, database_dataset>();
        ds.Add(p_dataset.schema?.table_name ?? "dataset1", p_dataset);
        datasets = ds;
    }

    public xml_database_query_response(database_query p_query, string[] p_warnings, IEnumerable<database_dataset> p_datasets)
    {
        query = p_query;
        warnings = p_warnings;
        var ds = new SortedList<string, database_dataset>();
        int id = 0;
        foreach (var dataset in p_datasets) ds.Add(dataset.schema?.table_name ?? $"dataset{++id}", dataset);
        datasets = ds;
    }
}
