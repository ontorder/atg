﻿using System.Xml;

namespace AtgCore
{
    public class data_set_xml_db_driver : i_data_set
    {
        private IEnumerable<XmlElement> _rows;

        public data_set_xml_db_driver(IEnumerable<XmlElement> rows) => _rows = rows;

        public IEnumerable<i_parser_db_driver> get_rows()
        {
            return _rows.Select(row => new data_row_xml_db_driver(row));
        }
    }
}
