﻿namespace AtgCore;

public class metadata_database
{
    public table_metadata_database[] tables;
    public foreign_key_database[] foreign_keys;

    public metadata_database(table_metadata_database[] p_tables, foreign_key_database[] p_foreign_keys)
    {
        tables = p_tables;
        foreign_keys = p_foreign_keys;
    }
}
