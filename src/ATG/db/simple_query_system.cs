﻿namespace AtgCore
{
    public class simple_query_system : i_query_system
    {
        private metadata_database _metadata;

        // ------------------ //

        public simple_query_system(metadata_database metadata)
        {
            _metadata = metadata;
        }

        public i_data_access_query get()
        {
            return default;
        }

        public i_data_access_query get(object id)
        {
            var query = new
            {
                type = "get",

            };
            return default;
        }

        public i_data_access_query update(object record)
        {
            return default;
        }

        public i_data_access_query update(object[] records)
        {
            return default;
        }

        public i_data_access_query create(object record)
        {
            return default;
        }

        public i_data_access_query delete(object id)
        {
            return default;
        }
    }
}
