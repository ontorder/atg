﻿namespace AtgCore;

public sealed class booter {
    public atg_controller? atg;
    public atg_form? atg_form;
    public logger? logger;
    public DateTime program_started_at;
    public atg_ui_controller? ui_ctrl;
    public restapi? webservice;

    //public const int DefaultInstance = 0;
    //public object ui_state;
    // sessione sostanzialmente è la chiave del canale di input (ad esempio una ui attiva)
    // cioè serve nei casi in cui mantengo uno stato (ad esempio se sono in ascolto degli eventi per un utente)
    //public Dictionary<object, int> session_to_instance;
    //public object[] instances;
    //public Dictionary<string, data_observer> user_to_data;

    public booter() => program_started_at = DateTime.Now;

    public async Task start_async() {
        try {
            // TODO dependency injection? moduli?
            var atg_config = atg_settings.ParseFromXml("conf.xml");
            var db_definitions = database_metadata_repository.get_db_definitions();
            var db = new xml_database(db_definitions, xml_database_settings_repository.get_atg_settings(atg_config.archive_folder));
            var parsers = new atg_parsers(db_definitions);
            var context = new atg_context(new atg_context_settings(db_definitions), db, new simple_query_system(db_definitions), parsers);

            //var query = new test_level1_join_query("termini", [new join_node(1, [new("t_gt", 2)]), new join_node(2, [new("gt_g", null)])], []);
            //var test0 = await db.execute_query_async(query);

            //var test = await context.termini.get_async();

            //var setup_gt = new gerarchia_termine_graph();
            //setup_gt.include_gerarchia();
            //var setup_termine = new termine_graph();
            //setup_termine.include_gerarchia_termine();
            //var test = await context.termini.get_test_async([setup_termine, setup_gt]);

            atg = new atg_controller(atg_config, context);
            atg.evento_log_handler.Add(InsertLog);
            atg.evento_user_changed_handler.Add(event_user_loaded);

            init_logger();

            atg.connetti_db();
            //await atg.get_active_timerpage().set_filter_async(atg.get_filter_start(), atg.get_filter_end());
            webservice = await init_api_async(context);

            atg_form = new atg_form();
            atg.carica_mapper(atg_form, InsertLog);
            ui_ctrl = new atg_ui_controller();
            ui_ctrl.add_form(atg_form);

            atg_form.Visible = true;
            //atg.UiController_carica_utente();
            Application.Run(atg_form);

            webservice.Stop();
        }
        catch (Exception config_err) {
            MessageBox.Show(exception_logger.recursive(config_err), "[Program] ECCEZIONE");
            Environment.Exit(-1);
        }
    }

    private void init_logger() {
        // TODO da scindere in queue + ui log
        logger = new logger(atg.cfg.myloglen, atg.cfg.mylogview);
        logger.insert(program_started_at, "avvio programma");
        logger.insert("caricata configurazione");
    }

    public void InsertLog(string text, bool is_error) =>
        logger.insert(text, is_error ? System.Drawing.Color.Red : System.Drawing.Color.Black);

    private void event_user_loaded(string db_type) =>
        logger.insert($"connessione db tipo '{db_type}'");

    private async Task<restapi> init_api_async(atg_context db) {
        var cfg = new restapi_settings(atg.cfg.webservice_host, atg.cfg.webservice_port, atg.cfg.webservice_path);
        var ws = new restapi(cfg, db, logger);
        await ws.StartAsync();
        logger.insert($"webserver started on {atg.cfg.webservice_host}:{atg.cfg.webservice_port}/{atg.cfg.webservice_path}");
        return ws;
    }
}
