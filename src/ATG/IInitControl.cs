﻿namespace AtgCore;

public interface IInitControl
{
    Control Init(Func<Delegate, object> pHostInvoke);
}
