namespace AtgCore;

public sealed class controlhost
{
    private readonly Form form;

    public controlhost(Form pForm) => form = pForm;

    public void AddControl(IInitControl iic) {
        var control = iic.Init(HostInvoke);
        control.Visible = false;
        form.Controls.Add(control);
    }

    public object HostInvoke(Delegate func) => form.Invoke(func);
}
