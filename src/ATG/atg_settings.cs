﻿using System.Xml;

namespace AtgCore;

public sealed class atg_settings
{
    public const char SeparatoreTermini = '\u2192';

    public string archive_folder;
    public int blocco_save_ms;
    public string file_db_folder;
    public int myloglen;
    public int mylogview;
    public string nome_autore;
    public string? ProjectOpen_address;
    public string? ProjectOpen_mapper;
    public string? ProjectOpen_pass;
    public string? ProjectOpen_user;
    public int record_per_file;
    public string webservice_host;
    public string webservice_path;
    public int webservice_port;
    private XmlDocument xml_cfg;
    private XmlNode xml_cfg_root;
    private string REMOVE_ConfigFilePath;

    // ------------------ //

    public atg_settings(
        string p_archive_folder, int p_blocco_save_ms, string p_file_db_folder, int p_myloglen, int p_mylogview, string p_nome_autore,
        XmlNode p_xml_cfg_root, XmlDocument p_xml_cfg, string configFilePath,
        string? p_projectOpen_address = null, string? p_projectOpen_mapper = null, string? p_projectOpen_user = null, string? p_projectOpen_pass = null,
        int p_record_per_file = 999, string p_webservice_host = "localhost", string p_webservice_path = "atg", int p_webservice_port = 1550
    )
    {
        archive_folder = p_archive_folder;
        blocco_save_ms = p_blocco_save_ms;
        file_db_folder = p_file_db_folder;
        myloglen = p_myloglen;
        mylogview = p_mylogview;
        nome_autore = p_nome_autore;
        ProjectOpen_address = p_projectOpen_address;
        ProjectOpen_mapper = p_projectOpen_mapper;
        ProjectOpen_user = p_projectOpen_user;
        ProjectOpen_pass = p_projectOpen_pass;
        record_per_file = p_record_per_file;
        webservice_host = p_webservice_host;
        webservice_path = p_webservice_path;
        webservice_port = p_webservice_port;
        xml_cfg_root = p_xml_cfg_root;
        xml_cfg = p_xml_cfg;
        REMOVE_ConfigFilePath = configFilePath;
    }

    public void set_autore(string autore)
    {
        xml_cfg_root["autore"].InnerText = autore;
        xml_cfg.Save(REMOVE_ConfigFilePath);
    }

    static public atg_settings ParseFromXml(string configFilePath)
    {
        var xml_cfg = new XmlDocument();
        xml_cfg.Load(configFilePath);

        var xml_cfg_root = xml_cfg["config"];
        string nome_autore = xml_cfg_root["autore"].InnerText;

        int record_per_file = Int32.Parse(xml_cfg_root["db_file_split_n_record"].InnerText);
        string file_db_folder = xml_cfg_root["db_path"].InnerText;

        int blocco_save_ms = Int32.Parse(xml_cfg_root["antiflood_salvataggio_ms"].InnerText);
        int myloglen = Int32.Parse(xml_cfg_root["loglen"].InnerText);
        int mylogview = Int32.Parse(xml_cfg_root["logview"].InnerText);

        string archive_folder = Path.Combine(file_db_folder, nome_autore);

        var po_node = xml_cfg_root["projectopen"];
        string? ProjectOpen_user = null;
        string? ProjectOpen_pass = null;
        string? ProjectOpen_address = null;
        string? ProjectOpen_mapper = null;
        if (po_node != null)
        {
            ProjectOpen_user = po_node.Attributes["user"].Value;
            ProjectOpen_pass = po_node.Attributes["pass"].Value;
            ProjectOpen_address = po_node.Attributes["address"].Value;
            var node_mapper = po_node.Attributes["mapper"];
            ProjectOpen_mapper = node_mapper?.Value;
        }

        return new atg_settings(
            archive_folder, blocco_save_ms, file_db_folder, myloglen, mylogview, nome_autore,
            xml_cfg_root, xml_cfg, configFilePath,
            ProjectOpen_address, ProjectOpen_mapper, ProjectOpen_user, ProjectOpen_pass,
            record_per_file
        );
    }
}
