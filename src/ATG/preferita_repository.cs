﻿namespace AtgCore;

public class preferita_repository
{
    private readonly atg_context _ctx;
    public bool is_modified;

    public preferita_repository(atg_context driver) => _ctx = driver;

    public async Task<gerarchia_preferita_ctx?> get_gerarchia_preferita_async(int gerarchia_id)
    {
        return await _ctx.gerarchie_preferite.get_async(gerarchia_id);
    }

    public async Task incrementa_gerarchia_preferita_async(int gerarchia_id)
    {
        var g = await _ctx.gerarchie_preferite.get_async(gerarchia_id);
        ++g.conteggio;
        await _ctx.gerarchie_preferite.update_async(g);
    }

    public async Task crea_gerarchia_preferita_async(int gerarchia_id)
        => await _ctx.gerarchie_preferite.add_async(new gerarchia_preferita_ctx(gerarchia_id, null, 1));

    public async Task<gerarchia_preferita_ctx[]> elenco_preferite_sorted_async()
    {
        var gps = await _ctx.gerarchie_preferite.get_async();
        return gps.OrderByDescending(_ => _.conteggio).ToArray();
    }
}
