﻿namespace AtgCore;

public sealed class atg_controller {
    public readonly atg_settings cfg;
    public List<Action<DateTime, DateTime>> evento_filter_handler;
    public List<Action<gerarchia_ctx[]>> evento_gerarchie_handler;
    public List<Action<string[]>> evento_gerarchie_preferite_handler;
    public List<Action<string, bool>> evento_log_handler;
    public List<Action> evento_load_default_gerarchie_handler;
    public List<Action<string>> evento_load_user;
    public List<Action<termine_ctx[]>> evento_termini_handler;
    public List<Action<string>> evento_user_changed_handler;

    private readonly atg_context _db;
    private DateTime filter_start, filter_end;
    private DateTime last_save;
    private controlbrowser? po_browser = null;
    private actionqueue? po_cmd_queue = null;
    private controlhost? po_host = null;
    private idmapper? po_mapper = null;
    private projectopen4? po_plugin;
    private timerpage_controller? tp = null;

    public atg_controller(atg_settings p_cfg, atg_context db) {
        cfg = p_cfg;
        _db = db;

        filter_start = DateTime.Today;
        filter_end = filter_start.AddDays(1).AddTicks(-1);

        evento_termini_handler = new();
        evento_gerarchie_preferite_handler = new List<Action<string[]>>();
        evento_gerarchie_handler = new();
        evento_log_handler = new List<Action<string, bool>>();
        evento_user_changed_handler = new List<Action<string>>();
        evento_filter_handler = new List<Action<DateTime, DateTime>>();
        evento_load_user = new List<Action<string>>();
        evento_load_default_gerarchie_handler = new List<Action>();
    }

    public void Stop() {
        po_cmd_queue?.Stop();
        po_plugin?.Stop();
    }

    public async Task UiController_carica_utente_async() {
        await raise_event_gerarchie_async();
        await raise_event_gerarchie_preferite_async();
        raise_event_filter_change();
        await add_gerarchie_default_async();
        raise_event_user_load();
    }

    public timerpage_controller get_active_timerpage() => tp;

    public void set_autore(string autore) =>
        cfg.set_autore(autore);

    public async Task save_struttura_temp_async() {
        // per evitare flooding di salvataggi
        if ((DateTime.Now - last_save).TotalMilliseconds < cfg.blocco_save_ms)
            return;

        await _db.salva_async();
        last_save = DateTime.Now;
    }

    public async Task save_struttura_async() =>
        await _db.salva_async();

    public async Task set_filter_async(DateTime t_start, DateTime t_end) {
        if (t_start == filter_start && t_end == filter_end) return; // potrei anche ricaricare invece di uscire
        if (t_end < t_start) throw new Exception("fine non può essere prima di inizio");

        filter_start = t_start;
        filter_end = t_end;

        await get_active_timerpage().set_filter_async(filter_start, filter_end);
    }

    public async Task aggiorna_gerarchia_preferita_async(int nuova_gerarchia) {
        var use_count = await _db.gerarchie_preferite.get_async(nuova_gerarchia);

        if (use_count == null) {
            await _db.gerarchie_preferite.add_async(new gerarchia_preferita_ctx(nuova_gerarchia, null, 0));
        }
        else {
            ++use_count.conteggio;
            await _db.gerarchie_preferite.update_async(use_count);
        }

        await raise_event_gerarchie_preferite_async();
    }

    public void connetti_db() {
        tp = new timerpage_controller(this, _db, get_filter_start, get_filter_end);
        tp.eventhandler_log.Add(raise_event_log);

        raise_event_user_changed(cfg.nome_autore);
    }

    public void carica_mapper(System.Windows.Forms.Form form, Action<string, bool> aLog) {
        if (String.IsNullOrEmpty(cfg.ProjectOpen_address)) {
            po_cmd_queue = null;
            po_plugin = null;
            po_mapper = null;
            return;
        }

        if (cfg.ProjectOpen_mapper != null) {
            po_mapper = new idmapper(cfg.ProjectOpen_mapper);
            po_mapper.LoadMappings();
        }

        po_host = new controlhost(form);
        po_browser = new controlbrowser();
        po_host.AddControl(po_browser);

        po_cmd_queue = new actionqueue();
        po_cmd_queue.Log = aLog;
        var po4_cfg = new projectopen4.configuration() {
            addr = cfg.ProjectOpen_address,
            cb = po_browser,
            user = cfg.ProjectOpen_user,
            pass = cfg.ProjectOpen_pass
        };

        po_plugin = new projectopen4(po4_cfg);
        po_plugin.Events = po4_events;

        po_cmd_queue.queue_event = dispatch_po_call;
    }

    public DateTime get_filter_start() => filter_start;

    public DateTime get_filter_end() => filter_end;

    private async Task add_gerarchie_default_async() {
        var defaults = await _db.gerarchie_default.get_async();
        var active_tp = get_active_timerpage();

        foreach (var gerarchia_id in defaults) {
            await active_tp.add_timerbar_async(gerarchia_id.gerarchia_default_id, true);
        }

        raise_event_default_gerarchie_loaded();
    }

    private void dispatch_po_call(object action) {
        switch (action) {
            case action_put_ora_projectopen apo: po_plugin.put_ora(apo); break;
            case action_put_commento_projectopen apc: po_plugin.put_commento(apc); break;

            default: {
                throw new Exception("[dispatch_po_call] comando non implementato");
            }
        }
    }

    public bool po_put_ora(DateTime pGiorno, int gerarchia_id, TimeSpan pDurata, string pNota = null) {
        if (po_cmd_queue == null)
            return false;

        if (!po_mapper.Mappings.ContainsKey(gerarchia_id))
            return false;

        var id = po_mapper == null ? gerarchia_id : po_mapper.Mappings[gerarchia_id];

        var cmd = new action_put_ora_projectopen() {
            progetto = id,
            giorno = pGiorno,
            nota = pNota,
            nota_interna = null,
            durata = pDurata
        };
        po_cmd_queue.enqueue(cmd);
        return true;
    }

    public bool po_put_commento(DateTime pGiorno, int gerarchia_id, string pNota) {
        if (po_mapper == null)
            return false;

        if (!po_mapper.Mappings.ContainsKey(gerarchia_id))
            return false;

        var cmd = new action_put_commento_projectopen() {
            progetto = po_mapper.Mappings[gerarchia_id],
            giorno = pGiorno,
            nota = pNota,
            nota_interna = null
        };
        po_cmd_queue.enqueue(cmd);
        return true;
    }

    private async Task raise_event_termini_async() {
        var termini = await _db.termini.get_async();

        foreach (var @event in evento_termini_handler)
            @event(termini.ToArray());
    }

    private async Task raise_event_gerarchie_async() {
        var gerarchie = await _db.gerarchie.get_async();

        foreach (var @event in evento_gerarchie_handler)
            @event(gerarchie.ToArray());
    }

    private async Task raise_event_gerarchie_preferite_async() {
        var prefe = await _db.gerarchie_preferite.get_async();
        var prefe_s = prefe.Select(pref => pref.gerarchia.aggrega()).ToArray();

        foreach (var @event in evento_gerarchie_preferite_handler)
            @event(prefe_s);
    }

    private void raise_event_log(string text, bool error) {
        foreach (var @event in evento_log_handler)
            @event(text, error);
    }

    private void raise_event_user_changed(string username) {
        foreach (var @event in evento_user_changed_handler)
            @event(username);
    }

    private void raise_event_filter_change() {
        foreach (var @event in evento_filter_handler)
            @event(filter_start, filter_end);
    }

    private void raise_event_user_load() {
        var user = cfg.nome_autore;

        foreach (var @event in evento_load_user)
            @event(user);
    }

    private void raise_event_default_gerarchie_loaded() {
        foreach (var @event in evento_load_default_gerarchie_handler)
            @event();
    }

    private void po4_events(projectopen4.EventBase eb) {
        switch (eb) {
            case projectopen4.EventNavigatedTo ent: {
                raise_event_log($"[po4] navigated to page {ent.Page}", false);
                break;
            }

            case projectopen4.EventLoggedId li: {
                raise_event_log($"[po4] logged in (user id {li.UserId})", false);
                break;
            }

            case projectopen4.EventPutComment pc: {
                raise_event_log($"[po4] put commento in progetto {pc.ProgettoId}", false);
                break;
            }

            case projectopen4.EventPutHour ph: {
                raise_event_log($"[po4] put ora in progetto {ph.ProgettoId}", false);
                break;
            }

            case projectopen4.EventSessionEnded _: {
                raise_event_log($"[po4] la sessione era terminata", false);
                break;
            }
        }
    }
}
