﻿namespace AtgCore
{
    public class atg_form_controller {
        private atg_controller atgctrl;
        private atg_form atgform;
        private logger logger;
        private repo _db;

        public atg_form_controller(atg_controller p_atgctrl, atg_form p_atgform, logger p_logger, repo db) {
            atgctrl = p_atgctrl;
            logger = p_logger;
            atgform = p_atgform;
            _db = db;
        }

        public void set_commands() {
            atgform.cmd_menu_istanza_click = menu_istanza_click;
            atgform.cmd_ricarica_click = ricarica_click;
            atgform.cmd_elimina_click = elimina_click;
            atgform.cmd_aggiungi_click = aggiungi_click;
            atgform.cmd_gerarchia_vuota_click = gerarchia_vuota_click;
            atgform.cmd_autore_validated = autore_validated;
            atgform.cmd_form_deactivated_async = form_deactivated_async;
            atgform.cmd_form_closing_async = form_closing_async;
            atgform.cmd_filtra_async = filtra_click_async;
            atgform.annulla_selezione_click = annulla_selezione_click;
        }

        public void menu_istanza_click(int gerarchia_id) {
            atgctrl.get_active_timerpage().add_timerbar_async(gerarchia_id);
        }

        public void ricarica_click() {
            //_db.ricarica_struttura();
            // UNDONE ricaricare tutto, riapplicare filtro
            logger.insert("db ricaricato");
        }

        public void elimina_click() {
            atgctrl.get_active_timerpage().delete_selected_timerbars();
        }

        public void aggiungi_click() {
            atgctrl.get_active_timerpage().add_timerbar_async(null);
        }

        public void gerarchia_vuota_click() {
            atgctrl.get_active_timerpage().add_timerbar_async(null);
            logger.insert("timerbar aggiunta");
        }

        public void autore_validated(string autore_name) {
            atgctrl.set_autore(autore_name);
            logger.insert($"cambiato 'autore' in '{autore_name}'");
        }

        public async Task form_deactivated_async() {
            await atgctrl.save_struttura_temp_async();
            logger.insert("salvataggio estemporaneo");
        }

        public async Task form_closing_async() {
            await atgctrl.save_struttura_async();
            atgctrl.Stop();
        }

        public async Task filtra_click_async(DateTime fltStart, DateTime fltEnd) {
            logger.insert("filtro temporale modificato: " + fltStart.ToString(intervalli_repository.datetime_format) + " a " + fltEnd.ToString(intervalli_repository.datetime_format));
            await atgctrl.set_filter_async(fltStart, fltEnd);
        }

        public void annulla_selezione_click() {
            atgctrl.get_active_timerpage().clean_selection();
        }
    }
}
