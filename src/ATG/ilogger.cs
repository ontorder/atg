﻿namespace AtgCore
{
    public interface ilogger
    {
        void insert(DateTime timestamp, string log_text, Color log_color);
        void insert(DateTime timestamp, string log_text);
        void insert(string log_text, Color log_color);
        void insert(string log_text);
        log_data[] get_all();
    }
}
