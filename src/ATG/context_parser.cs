﻿namespace AtgCore;

public interface context_parser<T>
{
    T parse(database_row row, (string[] foreign_keys, ICollection<field_remap> mappings)? include_context = null);
}
