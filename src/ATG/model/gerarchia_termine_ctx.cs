﻿namespace AtgCore;

public class gerarchia_termine_ctx
{
    public gerarchia_ctx gerarchia;
    public float peso;
    public termine_ctx termine;

    public gerarchia_termine_ctx(gerarchia_ctx p_gerarchia, float p_peso, termine_ctx p_termine)
    {
        gerarchia = p_gerarchia;
        peso = p_peso;
        termine = p_termine;
    }
}
