﻿namespace AtgCore;

public interface i_gerarchia_preferita_model {
    int conteggio { get; set; }
    int id { get; set; }
}
