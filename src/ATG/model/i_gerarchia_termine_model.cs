﻿namespace AtgCore;

public interface i_gerarchia_termine_model
{
    int gerarchia_id { get; set; }
    float peso { get; set; }
    int termine_id { get; set; }
}