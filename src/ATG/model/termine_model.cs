﻿namespace AtgCore;

public class termine_model : i_termine_model {
    public string nome { get; set; }
    public int    id { get; set; }

    public termine_model(int p_id, string p_nome)
    {
        nome = p_nome;
        id = p_id;
    }

    static public termine_model parse(i_field_parser parser) => new termine_model(parser.parse_int_field("id"), parser.parse_string_field("name"));
}
