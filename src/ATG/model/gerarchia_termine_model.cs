﻿namespace AtgCore;

public class gerarchia_termine_model : i_gerarchia_termine_model {
    public float peso { get; set; }
    public int   termine_id { get; set; }
    public int   gerarchia_id { get; set; }

    public gerarchia_termine_model(float p_peso, int p_id_termine, int p_gerarchia_id)
    {
        peso = p_peso;
        termine_id = p_id_termine;
        gerarchia_id = p_gerarchia_id;
    }
}
