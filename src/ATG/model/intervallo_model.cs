﻿namespace AtgCore;

public class intervallo_model : i_intervallo_model {
    public DateTime?      fine { get; set; }
    public string         commento { get; set; }
    public DateTime       inizio { get; set; }

    public intervallo_model(DateTime p_inizio, DateTime? p_fine, string p_commento)
    {
        inizio = p_inizio;
        fine = p_fine;
        commento = p_commento;
    }
}
