﻿namespace AtgCore;

public interface i_intervallo_model {
    DateTime  inizio { get; set; }
    DateTime? fine { get; set; }
    string    commento { get; set; }
}
