﻿namespace AtgCore
{
    public class gerarchia_repository
    {
        private atg_context _ctx;

        public gerarchia_repository(atg_context driver) => _ctx = driver;
        /*
        public gerarchia_model[] elenco_gerarchie()
        {
            var res = new List<gerarchia_model>();
            var xml_istanze = data.SelectNodes("root/istanze//gerarchia");

            foreach (XmlNode xn_gerarchia in xml_istanze)
            {
                if (xn_gerarchia.Name != "gerarchia") continue;

                var campi = new List<gerarchia_termine_model>();

                foreach (XmlNode xn_campo in xn_gerarchia.ChildNodes)
                {
                    if (xn_campo.Name != "campo") continue;
                    var xmlp = new xml_field_parser(gerarchia_termine_model.XmlFields, xn_campo);
                    var campo = gerarchia_termine_model.parse(xmlp);
                    campi.Add(campo);
                }
                var gerarchia_parser = new xml_field_parser(gerarchia_model.XmlFields, xn_gerarchia);
                var gerarchia = gerarchia_model.parse(gerarchia_parser, campi.ToArray());

                res.Add(gerarchia);
            }

            return res.ToArray();
        }

        public int? cerca_gerarchia(string[] gerarchia_test)
        {
            var gerarchie = elenco_gerarchie();
            var campi = new Dictionary<double, string>();
            int? found_id = null;
            bool cmp;

            foreach (var gerarchia in gerarchie)
            {
                int[] termini = gerarchia.campi.Select(campo => campo.id_termine).ToArray();
                if (termini.Length != gerarchia_test.Length) continue;

                cmp = true;
                for (int x = 0; x < gerarchia_test.Length; ++x)
                {
                    if (
                        data.SelectSingleNode($"root/gerarchie/termine[@id='{termini[x]}']").InnerText
                        !=
                        gerarchia_test[x]
                    )
                    {
                        cmp = false;
                        break;
                    }
                }

                if (cmp)
                {
                    found_id = gerarchia.id;
                    break;
                }
            }

            return found_id;
        }

        public int crea_gerarchia(string[] lst_gerarchia)
        {
            var gerarchie = elenco_gerarchie();
            XmlNode gerarchia = data.CreateElement("gerarchia");
            XmlAttribute xa;
            int x;
            int insert_id;

            xa = data.CreateAttribute("id");
            for (x = 0; x < gerarchie.Length; ++x)
                if (data.SelectSingleNode($"root/istanze/gerarchia[@id='{x}']") == null)
                    break;

            xa.Value = x.ToString();
            insert_id = int.Parse(xa.Value);
            gerarchia.Attributes.Append(xa);

            for (x = 0; x < lst_gerarchia.Length; ++x)
            {
                XmlNode xx = data.CreateElement("campo");
                xa = data.CreateAttribute("peso");
                xa.Value = ((x + 1) * 10).ToString();
                xx.Attributes.Append(xa);
                xa = data.CreateAttribute("id");
                xa.Value = data.SelectSingleNode($"root/gerarchie/termine[text()='{lst_gerarchia[x]}']").Attributes["id"].Value;
                xx.Attributes.Append(xa);
                gerarchia.AppendChild(xx);
            }

            data.SelectSingleNode("root/istanze").AppendChild(gerarchia);

            struttura_save();

            // bisogna eventualmente spostare i record nel nuovo db

            return insert_id;
        }

        private XmlDocument post_gerarchia(int id_gerarchia, int file_n = 0)
        {
            var xd = new XmlDocument();
            var db_path = Path.Combine(_settings.archive_folder, id_gerarchia.ToString());

            // non devo creare nodi al suo interno, vero? massì, dai
            string EMPTY_DB = $"<?xml version='1.0' encoding='UTF-8'?><root aiuto_per_i_poveri_umani='{AggregateGerarchia(id_gerarchia)}'/>";
            string DB_FILE = $"{file_n}.xml";

            if (!Directory.Exists(db_path))
                Directory.CreateDirectory(db_path);

            db_path = Path.Combine(db_path, DB_FILE);
            File.WriteAllText(db_path, EMPTY_DB);
            xd.Load(db_path);

            return xd;
        }

        /// <summary></summary>
        /// <param name="lst_gerarchia"></param>
        /// <param name="tb_gerarch"></param>
        /// <param name="gerarchie_attive"></param>
        /// <param name="filtro_inizio"></param>
        /// <param name="filtro_fine"></param>
        /// <returns>eventuale id gerarchia</returns>
        public int crea_gerarchia(string[] lst_gerarchia, int? tb_gerarch, int[] gerarchie_attive, DateTime filtro_inizio, DateTime filtro_fine)
        {
            int id = e_struttura.crea_gerarchia(lst_gerarchia);

            // questo controllo, messo qui, fa cagare
            if (gerarchie_attive.Contains(id))
            {
                return -1;
            }

            if (tb_gerarch.HasValue && tb_gerarch != id)
            {
                // uno dei punti cruciali: non prendo i valori scritti qui,
                // ma li pesco direttamente da db
                var irecord = Filtro_Intervallo(
                    tb_gerarch.Value,
                    filtro_inizio.Ticks,
                    filtro_fine.Ticks
                );

                // PER ORA faccio che usare Ore_Add;
                // non è un meccanismo veloce per spostamenti in massa, ma,
                // che cazzo, non son sicuro nemmeno lo farò mai
                foreach (var record_item in irecord)
                {
                    post_intervallo(
                        id,
                        record_item.inizio.ToBinary(),
                        record_item.fine.Value.ToBinary(),
                        record_item.commento
                    );
                }

                // elimina dal db i record
                foreach (var record_item in irecord)
                    delete_intervallo(
                        id,
                        new intervallo_model(
                            record_item.inizio.ToBinary(),
                            record_item.fine.Value.ToBinary()
                        )
                    );
            }

            return id;
        }

        public int? apri_crea_gerarchia(string[] new_gerarc)
        {
            inserisci_termini_non_esistenti(new_gerarc);

            var found_id = e_struttura.cerca_gerarchia(new_gerarc);

            if (!found_id.HasValue)
            {
                found_id = e_struttura.crea_gerarchia(new_gerarc);
                post_gerarchia(found_id.Value);
            }
            return found_id;
        }
        */
    }
}
