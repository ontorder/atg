﻿namespace AtgCore;

public class gerarchia_model : i_gerarchia_model
{
    public int gerarchia_id { get; set; }
    public string nome { get; set; }

    public gerarchia_model(int p_id, string p_nome)
    {
        nome = p_nome;
        gerarchia_id = p_id;
    }
}
