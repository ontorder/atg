﻿namespace AtgCore;

public class intervallo_ctx
{
    public int intervallo_id;
    public DateTime inizio;
    public DateTime? fine;
    public string? commento;
    public gerarchia_ctx? gerarchia;

    public intervallo_ctx(int p_intervallo_id, DateTime p_inizio, DateTime? p_fine = null, string? p_commento = null, gerarchia_ctx? p_gerarchia = null)
    {
        intervallo_id = p_intervallo_id;
        inizio = p_inizio;
        fine = p_fine;
        commento = p_commento;
        gerarchia = p_gerarchia;
    }
}
