﻿namespace AtgCore;

public interface i_gerarchia_model
{
    int gerarchia_id { get; set; }
    string nome { get; set; }
}
