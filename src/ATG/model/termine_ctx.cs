﻿namespace AtgCore;

public class termine_ctx
{
    public gerarchia_termine_ctx[]? gerarchie_termine;
    public string                   nome;
    public int                      termine_id;

    public termine_ctx(int p_termine_id, string p_nome, gerarchia_termine_ctx[]? p_gerarchie_termine = null)
    {
        termine_id = p_termine_id;
        nome = p_nome;
        gerarchie_termine = p_gerarchie_termine;
    }
}
