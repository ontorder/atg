﻿namespace AtgCore;

public class preferita_model : i_gerarchia_preferita_model {
    public int conteggio { get; set; }
    public int id { get; set; }

    public preferita_model(int p_conteggio, int p_id)
    {
        conteggio = p_conteggio;
        id = p_id;
    }
}
