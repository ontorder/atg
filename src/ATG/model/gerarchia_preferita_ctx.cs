﻿namespace AtgCore;

public class gerarchia_preferita_ctx
{
    public int conteggio;
    public gerarchia_ctx? gerarchia;
    public int gerarchia_preferita_id;

    public gerarchia_preferita_ctx(int p_gerarchia_preferita_id, gerarchia_ctx? p_gerarchia, int p_conteggio)
    {
        conteggio = p_conteggio;
        gerarchia = p_gerarchia;
        gerarchia_preferita_id = p_gerarchia_preferita_id;
    }
}
