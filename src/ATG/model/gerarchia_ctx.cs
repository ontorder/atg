﻿namespace AtgCore;

public class gerarchia_ctx
{
    public int gerarchia_id;
    public string nome;
    public gerarchia_termine_ctx[]? gerarchia_termini;
    public intervallo_ctx[]? intervalli;
    public gerarchia_preferita_ctx? gerarchia_preferita;
    public gerarchia_default_ctx? gerarchia_default;

    public string aggrega() => String.Join(atg_settings.SeparatoreTermini, gerarchia_termini.Select(gt => gt.termine.nome));
}
