﻿namespace AtgCore;

// NOTA ci sarebbe anche il profilo/autore, come lo considero, nome dello schema?
public sealed class atg_context
{
    private readonly exec_db_query _db;
    public query_context<gerarchia_ctx> gerarchie;
    public query_context<gerarchia_default_ctx> gerarchie_default;
    public query_context<gerarchia_preferita_ctx> gerarchie_preferite;
    //public query_context<gerarchia_termine_ctx> gerarchie_termini;
    public query_context<intervallo_ctx> intervalli;
    public i_query_system query;
    private readonly atg_context_settings _settings;
    public query_context<termine_ctx> termini;

    public atg_context(atg_context_settings p_settings, exec_db_query p_iedq, i_query_system p_query, atg_parsers p_parsers)
    {
        _db = p_iedq;
        _settings = p_settings;
        query = p_query;

        gerarchie = new query_context<gerarchia_ctx>(_settings.metadata, "gerarchie", p_parsers, _db);
        gerarchie_default = new query_context<gerarchia_default_ctx>(_settings.metadata, "gerarchie_default", p_parsers, _db);
        gerarchie_preferite = new query_context<gerarchia_preferita_ctx>(_settings.metadata, "gerarchie_preferite", p_parsers, _db);
        intervalli = new query_context<intervallo_ctx>(_settings.metadata, "intervalli", p_parsers, _db);
        termini = new query_context<termine_ctx>(_settings.metadata, "termini", p_parsers, _db);
    }

    public async Task salva_async()
    {
        // TODO
        //if (gerarchie.is_modified) ;
    }
}
