﻿namespace AtgCore
{
    public class log_data {
        public DateTime timestamp;
        public string message;
        public Color color;

        public log_data(DateTime p_timestamp, string p_message, Color p_color) {
            timestamp = p_timestamp;
            message = p_message;
            color = p_color;
        }
    }
}
