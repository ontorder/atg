using System.Xml;

namespace AtgCore
{
    public class idmapper {
        public  Dictionary<int, int> Mappings;
        public string MappingsFile;

        public idmapper(string pMappingsFile){
            Mappings = new Dictionary<int, int>();
            MappingsFile = pMappingsFile;
        }

        public int LoadMappings(){
            var xdoc = new XmlDocument();
            xdoc.Load(MappingsFile);
            var nodeMaps = xdoc.SelectNodes("//map");

            foreach(XmlNode map in nodeMaps){
                var srcid = int.Parse(map.Attributes["src_id"].Value);
                var dstid = int.Parse(map.Attributes["dst_id"].Value);

                Mappings.Add(srcid, dstid);
            }

            return Mappings.Count;
        }
    }
}
