﻿namespace AtgCore;

internal static class Program
{
    [STAThread]
    static async Task Main()
    {
        ApplicationConfiguration.Initialize();
        var booter = new booter();
        await booter.start_async();
    }
}
