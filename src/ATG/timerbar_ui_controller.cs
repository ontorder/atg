﻿namespace AtgCore
{
    public class timerbar_ui_controller
    {
        private TimerBar_UserControl bar;
        private atg_controller atg;
        public uint id;
        private logger logger;
        private atg_context _db;

        public timerbar_ui_controller(logger p_logger, TimerBar_UserControl p_bar, atg_context db) {
            bar = p_bar;
            logger = p_logger;
            _db = db;
        }

        public void timerbar_click() {
            atg.get_active_timerpage().get_timerbar_by_id(id).switch_selected();
        }

        public void set_commands() {
            bar.cmd_timerbar_click = timerbar_click;
            bar.cmd_intervalli_doubleclick_async = intervalli_doubleclick_async;
            bar.cmd_gerarchia_doubleclick_async = gerarchia_doubleclick_async;
            bar.cmd_elimina_click = elimina_click;
            bar.cmd_intervalli_selection_changed_async = intervalli_selection_changed_async;
            bar.cmd_salva_click_async = salva_async;
            bar.cmd_intervalli_keyup_async = intervalli_keyup_async;
            bar.cmd_playstop_click_async = playstop_click_async;
        }

        public async Task intervalli_doubleclick_async(int id_selected) {
            DateTime dt_inizio_mod, dt_fine_mod;

            var tbar = atg.get_active_timerpage().get_timerbar_by_id(id);
            var interv = tbar.get_intervallo(id_selected);
            var frm_altera = new dlg_edita_ore_form(interv.Item1, interv.Item2);

            frm_altera.ShowDialog(bar);

            if (frm_altera.dt_inizio_mod.HasValue) {
                dt_inizio_mod = frm_altera.dt_inizio_mod.Value;
                dt_fine_mod = frm_altera.dt_fine_mod.Value;

                if (dt_inizio_mod == interv.Item1 && dt_fine_mod == interv.Item2)
                    logger.insert("valori uguali, intervallo non modificato", Color.YellowGreen);
                else {
                    bool succ = await tbar.modifica_intervallo_async(interv.Item1, interv.Item2, dt_inizio_mod, dt_fine_mod);
                    if (succ) {
                        var t_src = $"{interv.Item1.ToLongTimeString()}/{interv.Item2.ToLongTimeString()}";
                        var t_dst = $"{dt_inizio_mod.ToLongTimeString()}/{dt_fine_mod.ToLongTimeString()}";
                        logger.insert($"modifica intervallo per gerarchia {id} da {t_src} -> {t_dst}");
                    }
                    else
                        logger.insert($"è successo un casino: modifica intervallo per gerarchia {id} fallita", Color.Red);
                }
            }
            else {
                logger.insert("modifica annullata", Color.GreenYellow);
            }

            frm_altera.Dispose();
        }

        public async Task gerarchia_doubleclick_async()
        {
            var frm_scelta = new dlg_gerarchie_form(_db);
            //lst_gerarchia.Items.Cast<string>().ToArray()
            await frm_scelta.carica_async(new[] { "recupera gerarchie" });
            string[]? res_gerarc;
            int?      id_gerarchia;

            do {
                frm_scelta.ShowDialog(bar);
                res_gerarc = frm_scelta.dlg_result;

                if (res_gerarc == null) {
                    return;
                }

                var test = await _db.gerarchie.where_async(res_gerarc);
                if (test.Any())
                    id_gerarchia = test.Single().gerarchia_id;
                else
                {
                    var new_g = new gerarchia_ctx() { gerarchia_termini = res_gerarc.Select(nome =>
                        new gerarchia_termine_ctx(null, 0, new termine_ctx(0, nome))).ToArray() }; // TODO ? id = 0
                    await _db.gerarchie.add_async(new_g);
                    id_gerarchia = new_g.gerarchia_id;
                }
            } while (!id_gerarchia.HasValue /* UNDONE or id_gerarchia==gerarchia attuale */);

            frm_scelta.Dispose();
            frm_scelta = null;
            res_gerarc = null;

            await atg.get_active_timerpage().get_timerbar_by_id(id).set_gerarchia_async(id_gerarchia.Value);
        }

        public void elimina_click() {
            atg.get_active_timerpage().get_timerbar_by_id(id).elimina();
        }

        public async Task intervalli_selection_changed_async(int selected_id){
            await atg
                .get_active_timerpage()
                .get_timerbar_by_id(id)
                .ui_selected_interval_async(selected_id)
            ;
        }

        public async Task salva_async(int selected_id, string text) {
            //var testo_intervallo = (string)lst_intervalli.SelectedItem;
            //var testo_split      = testo_intervallo.Split('-').Select(t => t.Trim()).ToArray();

            await atg
            .get_active_timerpage()
            .get_timerbar_by_id(id)
            .salva_commento_async(selected_id, text);

            logger.insert($"aggiornamento commento: {text.Length} byte");
            //Program.logger.insert("SALVATAGGIO COMMENTO: ERRORE", Color.Orange);
        }

        public async Task intervalli_keyup_async(intervallo_ctx record) {
            bool cancella_timerbar = false;

            var succ = await atg
                .get_active_timerpage()
                .get_timerbar_by_id(id)
                .rimuovi_intervallo_async(record, cancella_timerbar)
            ;
            if (!succ)
                logger.insert("cancellazione fallita", Color.Red);
        }

        public async Task playstop_click_async(bool restart){
            var tb = atg.get_active_timerpage().get_timerbar_by_id(id);

            if (tb.playing)
                await tb.stop_async();
            else
                await tb.play_async(restart);
        }
    }
}
