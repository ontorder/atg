﻿namespace AtgCore
{
    public class timerbar_controller {
        private const int INTERVALLO_FINE_IN_CORSO = -1;

        public uint my_id;

        timerpage_controller parent;
        atg_context db;
        Func<DateTime> get_filter_start;
        Func<DateTime> get_filter_end;

        //public List<long>  intervalli_inizio;
        //public List<long>  intervalli_fine;
        public List<intervallo_ctx> intervalli;
        public int? gerarchia;

        public bool playing;
        public bool selected;

        // eventi
        public List<Action<string[]>> evento_gerarchia;
        public List<Action<intervallo_ctx>> evento_intervallo;
        public List<Action<intervallo_ctx[]>> evento_intervalli;
        public List<Action<status_event_info_timerbar>> evento_status;
        public List<Action<bool>> evento_playstop;
        public List<Action<string, bool>> evento_log;
        public List<Action<bool>> evento_selezione;
        public List<Action<intervallo_ctx>> evento_cancellazione_intervallo;

        public timerbar_controller(uint id, timerpage_controller father, atg_context p_db, Func<DateTime> p_get_filter_start, Func<DateTime> p_get_filter_end)
        {
            my_id = id;
            parent = father;
            db = p_db;
            get_filter_start = p_get_filter_start;
            get_filter_end = p_get_filter_end;

            intervalli = new();
            gerarchia = null;

            playing = false;
            selected = false;

            evento_gerarchia = new List<Action<string[]>>();
            evento_intervallo = new();
            evento_intervalli = new();
            evento_status = new List<Action<status_event_info_timerbar>>();
            evento_playstop = new List<Action<bool>>();
            evento_log = new List<Action<string, bool>>();
            evento_selezione = new List<Action<bool>>();
            evento_cancellazione_intervallo = new List<Action<intervallo_ctx>>();
        }

        public async Task<int> EliminaIntervalliTimerbarDaDbAsync()
        {
            int x;
            int c = intervalli.Count;

            // cerca timer da rimuovere nei nodi xml
            for (x = 0; x < c; ++x)
            {
                await db.intervalli.delete_async((gerarchia.Value, intervalli[x]));
            }

            return c;
        }

        public async Task<bool> modifica_intervallo_async(DateTime t_inizio, DateTime t_fine, DateTime new_inizio, DateTime new_fine) {
            if (!gerarchia.HasValue)
                return false;

            var selected_index = 0;
            for (; selected_index < intervalli.Count; ++selected_index)
                if (intervalli[selected_index].inizio == t_inizio && intervalli[selected_index].fine == t_fine)
                    break;
            if (selected_index >= intervalli.Count)
                return false;

            // .select_record_where_datetime_and_gerarchia(gerarchia.Value, intervalli[selected_index]);
            var record_item = await db.intervalli.where_async((gerarchia.Value, intervalli[selected_index]));
            if (record_item == null) {
                return false;
            }

            intervalli[selected_index].inizio = new_inizio;
            intervalli[selected_index].fine = new_fine;

            try
            {
                // .update_datetime_where_id(record_item, new_inizio, new_fine);
                await db.intervalli.update_async((record_item, new_inizio, new_fine));
            }
            catch
            {
                return false;
            }

            await raise_event_intervalli_async();
            raise_event_status(new intervallo_ctx(0, intervalli[0].inizio));

            return true;
        }

        public async Task<bool> ui_selected_interval_async(int index) {
            if (index<0)
                return false;

            // .select_record_where_datetime_and_gerarchia(
            var record_item = await db.intervalli.where_async((gerarchia.Value, intervalli[index]));

            raise_event_interval_selected(record_item.Single());

            return true;
        }

        public async Task salva_commento_async(int id, string text) {
            // .select_record_where_datetime_and_gerarchia(
            var record_item = await db.intervalli.where_async((gerarchia.Value, intervalli[id]));

            // .put_commento(record_item, text);
            await db.intervalli.update_async((record_item, text));

            parent.father.po_put_commento(intervalli[id].inizio, gerarchia.Value, text);
        }

        public async Task ApplicaFiltroAsync() => await raise_event_intervalli_async();

        public async Task<bool> play_async(bool restart_last_interval) {
            if (!gerarchia.HasValue)
                return false;
            if (playing)
                return true;

            if (restart_last_interval){
                var intervallo_id = intervalli.Count;

                if (intervallo_id == 0){
                    raise_event_log("play ultimo intervallo: nessun intervallo", true);
                    return false;
                }

                --intervallo_id;
                // .restart_intervallo(gerarchia.Value, intervalli[intervallo_id]);
                await db.intervalli.update_async((gerarchia.Value, intervalli[intervallo_id]));
                intervalli[intervallo_id].fine = null;
            } else {
                DateTime dt_inizio = DateTime.Now;
                intervalli.Add(new intervallo_ctx(0, dt_inizio, p_fine: null));
                await db.intervalli.add_async(new intervallo_ctx(
                    0,
                    dt_inizio,
                    p_gerarchia: new gerarchia_ctx() { gerarchia_id = gerarchia.Value }
                ));
            }
            playing = true;

            raise_event_playstop();
            await raise_event_intervalli_async();
            raise_event_status();

            return playing;
        }

        public async Task<bool> stop_async(){
            if (!gerarchia.HasValue)
                return false;
            if (!playing)
                return false;

            var  dt_fine    = DateTime.Now;
            long fine_ticks = dt_fine.Ticks;

            var list_play = intervalli.Where(interv => interv.fine == null);
            if (list_play.Count() == 0) {
                raise_event_log("non ci sono intervalli da terminare!", true);
                return false;
            }

            var interv_play = list_play.First();
            interv_play.fine = dt_fine;
            playing = false;

            // .Ore_Completa_Intervallo(gerarchia.Value, interv_play.inizio, fine_ticks);
            try
            {
                await db.intervalli.update_async((gerarchia.Value, interv_play.inizio, dt_fine));
            }
            catch
            {
                raise_event_log("intervallo non trovato in salvataggio?!", true);
                return false;
            }

            var tot_time = get_tot_time();
            parent.father.po_put_ora(dt_fine, gerarchia.Value, tot_time, "(nessun commento)");

            raise_event_playstop();
            await raise_event_intervalli_async();
            raise_event_status();

            return playing;
        }

        public async Task<bool> rimuovi_intervallo_async(intervallo_ctx record, bool cancella_timerbar) {
            try
            {
                await db.intervalli.delete_async(record);
            }
            catch
            {
                return false;
            }

            raise_event_rimuovi_intervallo(record);
            await raise_event_intervalli_async();
            raise_event_status();
            // TODO nel caso in cui fosse stato cancellato l'intervallo in play dovrei resettare anche quello
            return true;
        }

        public async Task<bool> set_gerarchia_async(int id_gerarchia) {
            List<intervallo_ctx> record_sort = null;

            // dovrei controllare che non esista già una timerbar con questa gerarchia...?
            if (parent.count_timerbar_by_gerarchia(id_gerarchia)>0) {
                raise_event_log($"trovata un'altra timerbar con gerarchia {id_gerarchia}", true);
                return false;
            }

            gerarchia = id_gerarchia;
            await raise_event_gerarchia_async();

            // UNDONE ODDIO ma come faccio a farlo giusto?
            // risposta: qua dovrei avere un'entità gerarchia
            // la cui logica è interna
            // TODO Program.atg.aggiorna_gerarchia_preferita(gerarchia.Value);

            var dt_filtro_inizio = get_filter_start();
            var dt_filtro_fine   = get_filter_end();
            var filtered_record  = await db.intervalli.where_async((gerarchia.Value, dt_filtro_inizio.Ticks, dt_filtro_fine.Ticks));

            if (filtered_record!=null && filtered_record.Any()){
                var  event_intervalli_par = new List<string>();

                record_sort = filtered_record.ToList();

                record_sort.Sort(
                    new Comparison<intervallo_ctx>((record1, record2) => {
                        long discr = record1.inizio.ToBinary() - record2.inizio.ToBinary();

                        return Math.Sign(discr);
                        //return dv==0?0:dv>0?1:-1;
                    })
                );

                foreach(var record_item in record_sort){
                    intervalli.Add(new intervallo_ctx(0, record_item.inizio, record_item.fine));
                }

                await raise_event_intervalli_async();
                raise_event_status(record_sort==null ? null : record_sort.First());

                var new_play_state = intervalli.Any(@if => @if.fine == null);
                if(new_play_state != playing) {
                    playing = new_play_state;
                    raise_event_playstop();
                }
            }

            return true;
        }

        public TimeSpan get_tot_time() {
            var tot = intervalli.Where(item => item.fine != null).Select(item => (item.fine.Value - item.inizio).TotalSeconds).Sum();
            return TimeSpan.FromSeconds(tot);
        }

        public Tuple<DateTime, DateTime> get_intervallo(int id) {
            return new Tuple<DateTime, DateTime>(
                intervalli[id].inizio,
                intervalli[id].fine.Value
            );
        }

        public void switch_selected() {
            selected = !selected;

            raise_event_selezione();
        }

        public void clean_selected() {
            selected = false;

            raise_event_selezione();
        }

        public void elimina() { // TODO
            //    if(chi.bInCorso){ // TIMER ATTIVO
            //        iLog("timerbar attiva: almeno fatti lo sbatta di premere stop, prima", Color.Red);
            //        return;
            //    }
            //    if(MessageBox.Show(
            //        @"Proseguo?"+Environment.NewLine+
            //            @"-L'ELIMINAZIONE È COMPLETA, ANCHE DA DB"+Environment.NewLine+
            //            @"-ENTRO IL PERIODO ESPRESSO DAL FILTRO"+Environment.NewLine+
            //            @"-ANCHE LA GERARCHIA SE NON HA PIÙ RECORD ASSOCIATI"
            //        ,
            //        @"Eliminazione timer",
            //        MessageBoxButtons.YesNo
            //    ) == DialogResult.No){
            //        return;
            //    }
            //
            //    c=chi.EliminaIntervalliTimerbarDaDB();
            //    iLog("timerbar eliminata: "+c+" record");
        }

        private async Task raise_event_gerarchia_async()
        {
            var par =
                (await db.termini.where_async(gerarchia.Value))
                .Select(tpg => tpg.nome)
                .ToArray()
            ;

            foreach (var @event in evento_gerarchia)
            {
                @event(par);
            }
        }

        private async Task raise_event_intervalli_async()
        {
            long t_inizio = get_filter_start().Ticks;
            long t_fine = get_filter_end().Ticks;
            var filtered_record = await db.intervalli.where_async((gerarchia.Value, t_inizio, t_fine));

            if (filtered_record == null)
            {
                raise_event_log("Impossibile aprire il database", true);
                return;
            }

            // devo ripulire la lista degli intervalli, senza sputtanare l'eventuale timer in corso
            intervalli.Clear();

            foreach (var record_item in filtered_record)
            {
                var interval = new intervallo_ctx(0, record_item.inizio, record_item.fine);
                intervalli.Add(interval);
            }

            // o addirittura se becco un timer non stoppato nell'xml ci sarebbe un conflitto
            // perché cur_timer dovrebbe essere un vettore, a quel punto
            // TODO --> o semplicemente non esistere ;)
            // però comunque un intervallo che non è l'ultimo ma avviato lo posso considerare un errore...
            // faccio una finestrella (edita_ore) e faccio inserire l'ora (un po' violento)

            foreach (var @event in evento_intervalli)
            {
                @event(filtered_record.ToArray());
            }
        }

        private void raise_event_status(intervallo_ctx? primo = null)
        {
            var par = new status_event_info_timerbar();
            var tot = get_tot_time();

            par.time_total = tot.Ticks;
            par.inizio =
                primo != null ?
                    (DateTime?)primo.inizio
                :
                    intervalli.Count > 0 ?
                        intervalli[0].inizio
                    :
                        null
            ;

            foreach (var @event in evento_status)
            {
                @event(par);
            }
        }

        private void raise_event_playstop()
        {
            foreach (var @event in evento_playstop)
                @event(playing);
        }

        private void raise_event_log(string text, bool is_error)
        {
            foreach (var @event in evento_log)
                @event(text, is_error);
        }

        private void raise_event_interval_selected(intervallo_ctx record)
        {
            foreach (var @event in evento_intervallo)
                @event(record);
        }

        private void raise_event_selezione()
        {
            foreach (var @event in evento_selezione)
            {
                @event(selected);
            }
        }

        private void raise_event_rimuovi_intervallo(intervallo_ctx r)
        {
            foreach (var @event in evento_cancellazione_intervallo)
            {
                @event(r);
            }
        }
    }
}
