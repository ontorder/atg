﻿namespace AtgCore;

public sealed class atg_parsers : data_parsers
{
    public context_parser<gerarchia_ctx> gerarchia_parser;
    public context_parser<gerarchia_default_ctx> gerarchia_default_parser;
    public context_parser<gerarchia_preferita_ctx> gerarchia_preferita_parser;
    public context_parser<gerarchia_termine_ctx> gerarchia_termine_parser;
    public context_parser<intervallo_ctx> intervallo_parser;
    public context_parser<termine_ctx> termine_parser;

    public atg_parsers(metadata_database p_metadata)
    {
        gerarchia_parser = new gerarchia_ctx_context_parser(p_metadata, "gerarchie", this);
        gerarchia_default_parser = new gerarchia_default_ctx_context_parser();
        gerarchia_preferita_parser = new gerarchia_preferita_ctx_context_parser();
        intervallo_parser = new intervallo_ctx_context_parser();
        termine_parser = new termine_ctx_context_parser(p_metadata, "termini", this);
        gerarchia_termine_parser = new gerarchia_termine_ctx_context_parser(p_metadata, "gerarchie_termini", this);

        parsers = new Dictionary<string, object>
        {
            { "gerarchie", gerarchia_parser },
            { "gerarchie_default", gerarchia_default_parser },
            { "gerarchie_preferite", gerarchia_preferita_parser },
            { "intervalli", intervallo_parser },
            { "termini", termine_parser },
            { "gerarchie_termini", gerarchia_termine_parser }
        };
    }
}
