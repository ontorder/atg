﻿namespace AtgCore;

public sealed class database_metadata_repository
{
    static public metadata_database get_db_definitions()
    {
        return new metadata_database(
            new[]
            {
                new table_metadata_database(
                    "gerarchie",
                    new []{
                        new field_database("gerarchia_id", field_type_database.int32_type, field_storage_database.node_attribute, false),
                        new field_database("nome", field_type_database.string_type, field_storage_database.inner_text, false)
                    },
                    new single_field_id_xml_database("gerarchia_id")
                ),
                new table_metadata_database(
                    "termini",
                    new []{
                        new field_database("termine_id", field_type_database.int32_type, field_storage_database.node_attribute, false),
                        new field_database("nome", field_type_database.string_type, field_storage_database.inner_text, false)
                    },
                    new single_field_id_xml_database("termine_id")
                ),
                new table_metadata_database(
                    "gerarchie_termini",
                    new []{
                        new field_database("gerarchia_id", field_type_database.string_type, field_storage_database.node_attribute, false),
                        new field_database("termine_id", field_type_database.int32_type, field_storage_database.node_attribute, false),
                        new field_database("peso", field_type_database.float32_type, field_storage_database.node_attribute, false)
                    },
                    new multiple_field_id_xml_database(new[]{ "gerarchia_id", "termine_id" })
                ),
                new table_metadata_database(
                    "intervalli",
                    new []{
                        new field_database("gerarchia_id", field_type_database.string_type, field_storage_database.node_attribute, false),
                        new field_database("start", field_type_database.datetime_type, field_storage_database.node_attribute, false),
                        new field_database("end", field_type_database.datetime_type, field_storage_database.node_attribute, true),
                        new field_database("commento", field_type_database.string_type, field_storage_database.inner_text, true)
                    },
                    null
                ),
                new table_metadata_database(
                    "gerarchie_preferite",
                    new []{
                        new field_database("gerarchia_preferita_id", field_type_database.int32_type, field_storage_database.node_attribute, false),
                        new field_database("conteggio", field_type_database.int32_type, field_storage_database.node_attribute, false),
                    },
                    new single_field_id_xml_database("gerarchia_preferita_id")
                ),
                new table_metadata_database(
                    "gerarchie_default",
                    new []{
                        new field_database("gerarchia_default_id", field_type_database.int32_type, field_storage_database.node_attribute, false),
                    },
                    new single_field_id_xml_database("gerarchia_default_id")
                )
            },
            new[]
            {
                new foreign_key_database("g_gt", p_left_table_name: "gerarchie", p_left_field_name: "gerarchia_id", p_right_table_name: "gerarchie_termini", p_right_field_name: "gerarchia_id"),
                new foreign_key_database("gt_g", p_left_table_name: "gerarchie_termini", p_left_field_name: "gerarchia_id", p_right_table_name: "gerarchie", p_right_field_name: "gerarchia_id"),

                new foreign_key_database("t_gt", p_left_table_name: "termini", p_left_field_name: "termine_id", p_right_table_name: "gerarchie_termini", p_right_field_name: "termine_id"),
                new foreign_key_database("gt_t", p_left_table_name: "gerarchie_termini", p_left_field_name: "termine_id", p_right_table_name: "termini", p_right_field_name: "termine_id"),

                new foreign_key_database("gp_g", p_left_table_name: "gerarchie_preferite", p_left_field_name: "gerarchia_preferita_id", p_right_table_name: "gerarchie", p_right_field_name: "gerarchia_id"),
                new foreign_key_database("g_gp", p_left_table_name: "gerarchie", p_left_field_name: "gerarchia_id", p_right_table_name: "gerarchie_preferite", p_right_field_name: "gerarchia_preferita_id"),

                new foreign_key_database("gd_g", p_left_table_name: "gerarchie_default", p_left_field_name: "gerarchia_default_id", p_right_table_name: "gerarchie", p_right_field_name: "gerarchia_id"),
                new foreign_key_database("g_gd", p_left_table_name: "gerarchie", p_left_field_name: "gerarchia_id", p_right_table_name: "gerarchie_default", p_right_field_name: "gerarchia_default_id"),

                new foreign_key_database("g_i", p_left_table_name: "gerarchie", p_left_field_name: "gerarchia_id", p_right_table_name: "intervalli", p_right_field_name: "gerarchia_id"),
                new foreign_key_database("i_g", p_left_table_name: "intervalli", p_left_field_name: "gerarchia_id", p_right_table_name: "gerarchie", p_right_field_name: "gerarchia_id"),
            }
        );
    }
}
