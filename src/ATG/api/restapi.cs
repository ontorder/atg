﻿using System.Net;

namespace AtgCore
{
    public class restapi
    {
        private readonly atg_context _db;
        private readonly ilogger _logger;
        private tiny_rest_engine rest_dispatcher;
        public restapi_settings Settings;
        private readonly web_server webservice;

        public restapi(restapi_settings settings, atg_context db, ilogger logger)
        {
            _logger = logger;
            _db = db;
            Settings = settings;

            set_api();

            webservice = new(Settings.web_domain, Settings.port, Settings.path);
            webservice.WebServerEventAsync = webserver_event_async;
        }

        private void set_api()
        {
            rest_dispatcher = new tiny_rest_engine("atg", false, null);

            var tre_gerarchia = new tiny_rest_engine("gerarchia", true, resource_gerarchie_async);
            rest_dispatcher.AddChild(tre_gerarchia);

            var tre_termine = new tiny_rest_engine("termine", true, resource_termini_async);
            rest_dispatcher.AddChild(tre_termine);

            var tre_log = new tiny_rest_engine("log", false, resource_log_async);
            rest_dispatcher.AddChild(tre_log);
        }

        private async Task webserver_event_async(HttpListenerContext? ctx, EventType operation, SortedList<string, string> data, Func<HttpStatusCode, string, Task>? send_response_async, string? postBody)
        {
            switch (operation)
            {
                case EventType.Command:
                {
                    try
                    {
                        var frags = ctx
                            .Request
                            .Url
                            .Segments
                            .Select(frag => frag.Replace("/", ""))
                            .Where(frag => !String.IsNullOrWhiteSpace(frag))
                            .Skip(1)
                            .ToArray()
                        ;

                        var verb = (VerbsRest)Enum.Parse(typeof(VerbsRest), ctx.Request.HttpMethod);
                        var res = await webservice_command_async(ctx, verb, frags, data, postBody);

                        await send_response_async(res.Item1, res.Item2);
                    }
                    catch (Exception command_exception)
                    {
                        insert_log(exception_logger.recursive(command_exception), true);
                        await send_response_async(HttpStatusCode.InternalServerError, command_exception.GetType().ToString());
                    }
                    break;
                }

                case EventType.Log:
                {
                    var msg = "webservice: " + data[web_server.WEBSRV_MSG_Title];

                    if (data.ContainsKey(web_server.WEBSRV_MSG_Desc))
                        msg += "\n" + data[web_server.WEBSRV_MSG_Desc];

                    insert_log(msg);
                    break;
                }
            }
        }

        private async Task<Tuple<HttpStatusCode, string>> webservice_command_async(HttpListenerContext ctx, VerbsRest verb, string[] rest_path, SortedList<string, string> args, string? postBody)
        {
            var response = await rest_dispatcher.ParseDispatchAsync(ctx, verb, rest_path, args, postBody);
            string data = response.Data is string ? (string)response.Data : Newtonsoft.Json.JsonConvert.SerializeObject(response.Data);

            return new Tuple<HttpStatusCode, string>(HttpStatusCode.OK, data);
        }

        private async Task<object?> resource_gerarchie_async(HttpListenerContext ctx, RestParseState state)
        {
            object? call_res = null;

            switch (state.Verb)
            {
                case VerbsRest.DELETE: { break; }
                case VerbsRest.PUT: { break; }
                case VerbsRest.POST: { break; }
                case VerbsRest.GET:
                {
                    if (state.Parameters[0] == String.Empty)
                    {
                        var gerarchie = await _db.gerarchie.get_async();

                        var writer = new json_fuckin_writer();
                        var model = new model_writer();

                        var z = model.get_root_object();
                        var base_array = model.create_root_array(z);

                        var object_gerarchia = model.create_object(base_array, "gerarchia");
                        model.add_field(object_gerarchia, "id");

                        var array_campi = model.create_array(object_gerarchia, "campi");
                        var object_campo = model.create_object(array_campi, "campo");
                        model.add_field(object_campo, "id_termine");
                        model.add_field(object_campo, "peso");

                        call_res = model.write(writer, gerarchie);
                    }
                    else
                        call_res = "get by id not implemented";
                    break;
                }
            }

            return call_res;
        }

        private async Task<object?> resource_termini_async(HttpListenerContext ctx, RestParseState state)
        {
            object? call_res = null;

            switch (state.Verb)
            {
                case VerbsRest.DELETE: { break; }
                case VerbsRest.PUT: { break; }
                case VerbsRest.POST: { break; }
                case VerbsRest.GET:
                {
                    if (state.Parameters[0] == String.Empty)
                    {
                        var termini = await _db.termini.get_async();

                        var writer = new json_fuckin_writer();
                        var model = new model_writer();

                        var root = model.get_root_object();
                        var base_array = model.create_root_array(root);

                        var termine_object = model.create_object(base_array, "termine");
                        model.add_field(termine_object, "id");
                        model.add_field(termine_object, "nome");

                        call_res = model.write(writer, termini);
                    }
                    else
                        call_res = "get by id not implemented";
                    break;
                }
            }

            return call_res;
        }

        private async Task<object?> resource_log_async(HttpListenerContext ctx, RestParseState state)
        {
            object? call_res = null;

            switch (state.Verb)
            {
                case VerbsRest.DELETE: { break; }
                case VerbsRest.PUT: { break; }
                case VerbsRest.POST: { break; }
                case VerbsRest.GET:
                {
                    call_res = _logger.get_all();
                    break;
                }
            }

            return call_res;
        }

        public void insert_log(string text, bool is_error = false)
        {
            if (_logger != null)
            {
                // problema di uso di thread non legato all'interfaccia
                // o faccio un disaccoppiamento per tutti i messaggi, o non lo so

                //logger.insert(text, is_error ? System.Drawing.Color.Violet : System.Drawing.Color.Black);
            }
        }

        public async Task StartAsync()
        {
            await webservice.StartAsync();
            insert_log($"rest api started on {Settings.web_domain}:{Settings.port}/{Settings.path}");
        }

        public void Stop() => webservice.Stop();
    }
}
