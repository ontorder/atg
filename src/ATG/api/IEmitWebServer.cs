﻿namespace AtgCore;

public interface IEmitWebServer
{
    WebServerEventHandler? WebServerEventAsync { get; set; }
}
