﻿namespace AtgCore;

public class RestParseState
{
    private string[] DebugOriginalFragments;
    private Queue<string> FragmentsToParse;
    public List<string> Parameters;
    public string? PostData;
    public SortedList<string, string> QueryParams;
    public VerbsRest Verb;

    public RestParseState(VerbsRest pVerb, string[] frags, SortedList<string, string> pQueryParams, string? pPostData)
    {
        DebugOriginalFragments = frags;
        FragmentsToParse = new Queue<string>(frags);
        PostData = pPostData;
        Verb = pVerb;
        Parameters = new List<string>();
        QueryParams = pQueryParams;
    }

    public bool HasMoreFragments() => FragmentsToParse.Count() > 0;
    public string ConsumeFrag() => FragmentsToParse.Dequeue();
}
