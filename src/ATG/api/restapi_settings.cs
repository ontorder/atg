﻿namespace AtgCore;

public class restapi_settings
{
    public string web_domain;
    public int port;
    public string path;

    public restapi_settings(string p_web_domain, int p_port, string p_path)
    {
        web_domain = p_web_domain;
        port = p_port;
        path = p_path;
    }
}
