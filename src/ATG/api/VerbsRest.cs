﻿namespace AtgCore;

public enum VerbsRest
{
    GET, POST, PUT, DELETE
}
