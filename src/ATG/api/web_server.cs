﻿using System.Diagnostics;
using System.Net;
using System.Text;
using System.Web;

namespace AtgCore
{
    public class web_server : IEmitWebServer
    {
        public const string WEBSRV_MSG_Desc = "description";
        public const string WEBSRV_MSG_Title = "title";
        public const string WEBSRV_MSG_Uri = "__uri";
        public const int HARD_THREAD_NUM_LIMIT = 100;
        public const int BUSY_CREATION_THREAD_WAIT = 999;

        private bool                  active = false;
        private bool                  cors_allow_origin;
        private bool                  debug_mode;
        public Action<Exception>      ErrorEvent { get; set; }
        private string                inst_prefix;
        private HttpListener          listener;
        private bool                  multithreaded;
        public WebServerEventHandler? WebServerEventAsync { get; set; }

        // ---------- //

        public web_server(string prefix, bool corsEnabled = false, bool pMultithreaded = false, bool pDebug = false)
        {
            listener = new HttpListener();
            inst_prefix = prefix;
            listener.Prefixes.Add(prefix);
            cors_allow_origin = corsEnabled;
            multithreaded = pMultithreaded;
            debug_mode = pDebug;
        }

        /// <summary>http://web_domain:port/path/</summary>
        public web_server(string web_domain, int port, string path)
        {
            string url = $"http://{web_domain}:{port}/{path}";
            if (!url.EndsWith("/")) url += "/";

            listener = new HttpListener();
            listener.Prefixes.Add(url);
            inst_prefix = url;
        }

        public async Task StartAsync()
        {
            try {
                listener.Start();
                active = true;

                try {
                    _ = AcceptClientsAsync();
                } catch (Exception acceptErr) {
                    var ex_data = new SortedList<string, string>
                    {
                        [WEBSRV_MSG_Title] = "[WebServer.read_cycle] EXCEPTION starting read thread",
                        [WEBSRV_MSG_Desc] = exception_logger.recursive(acceptErr)
                    };

                    await RaiseWebServerEventAsync(null, EventType.Log, ex_data, null);
                }

                var data = new SortedList<string, string>
                {
                    [WEBSRV_MSG_Desc] = "",
                    [WEBSRV_MSG_Title] = "WebServer Started"
                };
                await RaiseWebServerEventAsync(null, EventType.Log, data, null);
            } catch (Exception start_err) {
                ErrorEvent(start_err);
                throw new Exception("oops", start_err);
            }
        }

        public void Stop()
        {
            listener.Stop();
            active = false;
        }

        public string GetAddress() => inst_prefix;

        private async Task AcceptClientsAsync()
        {
            if (!active || listener == null || !listener.IsListening)
                return;

            var newContext = await listener.GetContextAsync();
            _ = AcceptClientsAsync();
            await ProcessClientAsync(newContext);
        }

        private async Task ProcessClientAsync(HttpListenerContext resp_context)
        {
            string __method = $"[{nameof(web_server)}.{nameof(ProcessClientAsync)}]";

            // controllo che sia l'url giusto (opzionale)
            try {
                string abs_uri = resp_context.Request.Url.AbsoluteUri;

                // non è riuscito a parsarlo
                if (abs_uri == null) {
                    try {
                        // in caso di errore provo ad inviare una risposta
                        var resp_out = resp_context.Response.OutputStream;
                        string message = "cannot parse url";
                        resp_context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        resp_context.Response.ContentLength64 = message.Length;

                        resp_out.Write(Encoding.ASCII.GetBytes(message), 0, message.Length);
                        resp_out.Close();
                    } catch {
                    }

                    return;
                }

                // in qualche modo l'url non è quello giusto
                // ATTENZIONE non è intelligente, se uno usa :80 potrebbero esserci casini
                if (!abs_uri.StartsWith(inst_prefix)) {
                    try {
                        // in caso di errore provo ad inviare una risposta
                        var resp_out = resp_context.Response.OutputStream;
                        string message = "wrong url";
                        resp_context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        resp_context.Response.ContentLength64 = message.Length;

                        resp_out.Write(Encoding.ASCII.GetBytes(message), 0, message.Length);
                        resp_out.Close();
                    } catch {
                    }

                    return;
                }
            } catch (Exception check_err) {
                // non son proprio sicuro questo errore sarebbe da riportare
                var data = new SortedList<string, string>
                {
                    [WEBSRV_MSG_Title] = $"{__method} EXCEPTION checking uri",
                    [WEBSRV_MSG_Desc] = exception_logger.recursive(check_err)
                };

                await RaiseWebServerEventAsync(resp_context, EventType.Log, data, null);
                await SendResponseAsync(resp_context, HttpStatusCode.InternalServerError, "");
                return;
            }

            // prendo i parametri inviati via http
            var params_data = new SortedList<string, string>();
            try {
                var get_params = HttpUtility.ParseQueryString(resp_context.Request.Url.Query);

                foreach (string k in get_params.AllKeys)
                    if (k != null)
                        params_data.Add(k, get_params[k]);
            } catch (Exception params_err) {
                var data = new SortedList<string, string>
                {
                    [WEBSRV_MSG_Title] = $"{__method} EXCEPTION",
                    [WEBSRV_MSG_Desc] = exception_logger.recursive(params_err)
                };

                await RaiseWebServerEventAsync(resp_context, EventType.Log, data, null);

                // in caso di errore provo ad inviare una risposta
                try {
                    var resp_out = resp_context.Response.OutputStream;
                    string message = "error obtaining the GET parameters!";
                    resp_context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    resp_context.Response.ContentLength64 = message.Length;

                    resp_out.Write(Encoding.ASCII.GetBytes(message), 0, message.Length);
                    resp_out.Close();
                } catch (Exception answer_err) {
                    var data2 = new SortedList<string, string>
                    {
                        [WEBSRV_MSG_Title] = $"{__method} EXCEPTION in sending http response",
                        [WEBSRV_MSG_Desc] = exception_logger.recursive(answer_err)
                    };

                    await RaiseWebServerEventAsync(resp_context, EventType.Log, data2, null);
                }

                return;
            }

            string? post_data = null;
            try {
                var req = resp_context.Request;
                var body = req.InputStream;
                var encoding = req.ContentType!=null && req.ContentType.Contains("charset") ? req.ContentEncoding : Encoding.UTF8;
                var reader = new System.IO.StreamReader(body, encoding);

                post_data = await reader.ReadToEndAsync();
                reader.Close();
                body.Close();
            } catch {
            }

            // preparo una funzione per permettere di inviare una risposta
            var send_response_callback_async = async (HttpStatusCode  status_code, string? message) => await SendResponseAsync(resp_context, status_code, message);

            // CORS
            try {
                if (resp_context.Request.HttpMethod == "OPTIONS") {
                    // no https?
                    if (cors_allow_origin) {
                        resp_context.Response.AddHeader("Access-Control-Allow-Origin", "*");
                        resp_context.Response.AddHeader("Access-Control-Allow-Methods", "DELETE, PUT, POST, GET, OPTIONS");
                        resp_context.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type");

                        await send_response_callback_async(HttpStatusCode.OK, "");
                        return;
                    }
                }
            } catch (Exception cors_err) {
                var data = new SortedList<string, string>
                {
                    [WEBSRV_MSG_Title] = $"{__method} CORS EXCEPTION",
                    [WEBSRV_MSG_Desc] = exception_logger.recursive(cors_err)
                };

                await RaiseWebServerEventAsync(resp_context, EventType.Log, data, null);
            }

            // avviso chi sta nella catena dei messaggi che ho ricevuto un comando
            try {
                await RaiseWebServerEventAsync(resp_context, EventType.Command, params_data, send_response_callback_async, post_data);
            } catch (Exception dispatch_err) {
                // non c'è molto che posso fare
                var data = new SortedList<string, string>
                {
                    [WEBSRV_MSG_Title] = $"{__method} EXCEPTION-CEPTION",
                    [WEBSRV_MSG_Desc] = exception_logger.recursive(dispatch_err)
                };

                await RaiseWebServerEventAsync(resp_context, EventType.Log, data, null);
            }
        }

        private async Task SendResponseAsync(HttpListenerContext clientContext, HttpStatusCode statusCode, string message)
        {
            try {
                if (debug_mode) {
                    var stack = new StackTrace(true);
                    var methods = stack.GetFrames().Select(frame => frame.GetMethod().Name + " : " + frame.GetFileLineNumber());
                    string text = String.Join("\n", methods);
                    var pars = new SortedList<string, string>
                    {
                        { WEBSRV_MSG_Title, "debug http response - context " + clientContext.GetHashCode() },
                        { WEBSRV_MSG_Desc, text }
                    };
                    await RaiseWebServerEventAsync(clientContext, EventType.Log, pars, null);
                }

                var resp_context = clientContext.Response;
                var resp_out = resp_context.OutputStream;

                if (cors_allow_origin)
                    resp_context.AddHeader("Access-Control-Allow-Origin", "*");
                resp_context.StatusCode = (int)statusCode;
                resp_context.ContentLength64 = message.Length;

                await resp_out.WriteAsync(Encoding.ASCII.GetBytes(message), 0, message.Length);
                resp_out.Close();
            } catch (Exception send_err) {
                var data = new SortedList<string, string>
                {
                    [WEBSRV_MSG_Title] = "[WebServer.SendResponse] EXCEPTION in sending http response",
                    [WEBSRV_MSG_Desc] = exception_logger.recursive(send_err)
                };

                await RaiseWebServerEventAsync(clientContext, EventType.Log, data, null);
            }
        }

        // ---------------

        public async Task RaiseWebServerEventAsync(HttpListenerContext? ctx, EventType op, SortedList<string, string> data, Func<HttpStatusCode, string, Task>? send_response, string? postBody = null)
        {
            if (WebServerEventAsync != null)
                await WebServerEventAsync(ctx, op, data, send_response, postBody);
        }
    }

    public delegate Task WebServerEventHandler(HttpListenerContext? sender, EventType operation, SortedList<string, string> data, Func<HttpStatusCode, string, Task>? send_response, string? postBody = null);
}
