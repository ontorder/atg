﻿using System.Net;

namespace AtgCore
{
    public class tiny_rest_engine
    {
        public delegate Task<object?> LeafFunctionDelegate(HttpListenerContext ctx, RestParseState state);

        private readonly Dictionary<string, tiny_rest_engine> Branches;
        private int Level;
        private readonly string Fragment;
        private readonly LeafFunctionDelegate? LeafFunction;
        private readonly bool HasParameter;

        // --------------------------------- //

        public tiny_rest_engine(string pFragment, bool withParameter, LeafFunctionDelegate? pLeafFunction = null)
        {
            Level = 1;
            Fragment = pFragment;
            HasParameter = withParameter;
            Branches = new Dictionary<string, tiny_rest_engine>();
            LeafFunction = pLeafFunction;
        }

        public void AddChild(tiny_rest_engine tre)
        {
            Branches.Add(tre.Fragment, tre);
            tre.SetLevel(Level + 1);
        }

        public async Task<RestResponse> ParseDispatchAsync(HttpListenerContext ctx, VerbsRest pVerb, string[] frags, SortedList<string, string> queryParams, string? pPostData)
        {
            var state = new RestParseState(pVerb, frags, queryParams, pPostData);
            return await DispatchAsync(ctx, state);
        }

        // se ci dovrebbe essere il parametro ma non c'è -> parametro è "tutto"
        // se c'è il parametro -> restituisco quella risorsa
        // se !withParameter e non ci sono figli -> elenco funzioni figlie
        public async Task<RestResponse> DispatchAsync(HttpListenerContext ctx, RestParseState state)
        {
            if (HasParameter) {
                if (state.HasMoreFragments())
                    state.Parameters.Add(state.ConsumeFrag());
                else
                    state.Parameters.Add(string.Empty);
            }

            if (state.HasMoreFragments()) {
                var next = state.ConsumeFrag();

                if (!Branches.ContainsKey(next))
                    return new RestResponse(new { error = $"resource '{next}' not found" });

                var controller = Branches[next];
                return await controller.DispatchAsync(ctx, state);
            }

            var res = LeafFunction == null ? null : await LeafFunction(ctx, state);
            var links = from bkey in Branches.Keys select bkey;

            return new RestResponse(res, links.ToArray());
        }

        public void SetLevel(int pLevel) => Level = pLevel;
    }
}
