﻿namespace AtgCore;

public class RestResponse
{
    public object? Data;
    public string[]? Links;

    public RestResponse(object? pData, string[]? pLinks = null)
    {
        Data = pData;
        Links = pLinks;
    }
}
