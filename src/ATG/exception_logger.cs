﻿namespace AtgCore
{
    public class exception_logger {
        public static string simple(Exception e) =>
            $"{e.GetType()}: {e.Message}";

        public static string recursive(Exception e){
            var logs = new List<string>();
            _recursive(logs, e);
            return string.Join("\n", logs);
        }

        static private void _recursive(List<string> logs, Exception e){
            logs.AddRange(new[]{
               $@"type      : {e.GetType()}",
                $"message   : {e.Message}",
                $"source    : {e.Source}",
                $"stacktrace: {e.StackTrace}"
            });

            if (e.InnerException != null)
                _recursive(logs, e.InnerException);
        }
    }
}
