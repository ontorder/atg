﻿namespace AtgCore
{
    public class intervalli_repository
    {
        public const string datetime_format = "s"; // TODO per xml file db; opzione?
        private atg_context _ctx;

        public intervalli_repository(atg_context context) => _ctx = context;
        /*
        public async Task<intervallo_ctx> select_record_where_datetime_and_gerarchia_async(int id_gerarchia, intervallo_ctx interv)
        {
            // string xquery = $"root/record[@inizio='{interv.inizio.ToString(datetime_format)}']";
            // if (interv.fine > 0) xquery += $"[@fine='{interv.fine.ToString(datetime_format)}']";
            var intervallo = await _ctx.intervalli.where_async((id_gerarchia, interv.inizio, interv.fine));
            return intervallo.Single();
        }

        // TODO non sarebbe scorretto spostare tutti i record in eccesso nei file successivi
        // sempre mantenendo l'ordine
        public async Task post_intervallo_async(int id_gerarchia, long t_iniz, long t_fin, string commento = "")
        {
            XmlAttribute xa;
            XmlNode cur_timer;
            var DBOre = Apri(id_gerarchia, t_iniz);
            var vuoto = false;

            // se ho riempito l'attuale db ne faccio un altro
            // ATTENZIONE max nodi e basta, non per forza elementi record
            // UNDONE non mi piace avere più punti in cui viene creato un file
            if (DBOre["root"].ChildNodes.Count > _settings.n_split_archivio)
            {
                // se apri ha trovato il file giusto
                {
                    string db_path = Path.Combine(_settings.archive_folder, id_gerarchia.ToString());
                    int temp
                        , max = -1
                    ;
                    // ATTENZIONE non 100% sicuro
                    foreach (string sfile in Directory.EnumerateFiles(db_path, "*.xml"))
                    {
                        if (Int32.TryParse(Path.GetFileNameWithoutExtension(sfile), out temp))
                            if (temp > max)
                                max = temp;
                    }

                    ++max;

                    DBOre = post_gerarchia(id_gerarchia, max);
                }
            }

            if (DBOre["root"].ChildNodes.Count == 0)
                vuoto = true;

            // se metto l'aggiunta dopo il controllo, non creo mai un db.xml vuoto, cosa che cambia i controlli
            cur_timer = DBOre.CreateNode(XmlNodeType.Element, "record", "");
            xa = DBOre.CreateAttribute("inizio");
            xa.Value = DateTime.FromBinary(t_iniz).ToString(datetime_format);
            cur_timer.Attributes.Append(xa);
            xa = DBOre.CreateAttribute("fine");
            xa.Value = t_fin < 0 ? "" : DateTime.FromBinary(t_fin).ToString(datetime_format);
            cur_timer.Attributes.Append(xa);
            cur_timer.InnerText = commento;

            if (vuoto)
                DBOre.SelectSingleNode("root").AppendChild(cur_timer);
            else
            {
                XmlNode exn = null;

                // XXX mi converrebbe partire dalla fine
                // ATTENZIONE XXX CAZZU CAZZU CODICE ROSSO MINCHIA se questa parte non funziona come deve,
                // allora tutte le parti in cui controllo inizio/fine di primo/ultimo record sono da cambiare
                // in controlli su tutti i record
                foreach (XmlNode xn in DBOre.SelectNodes("root//record")) if (xn.NodeType == XmlNodeType.Element)
                    {
                        exn = xn; // non bellissimo
                        if (DateTime.Parse(xn.Attributes["inizio"].Value).ToBinary() <= t_iniz)
                        {
                            continue;
                        }
                        break;
                    }

                if (exn == null) // cioè se il db è vuoto
                    DBOre.SelectSingleNode("root")
                        .InsertAfter(
                            exn,
                            DBOre.SelectSingleNode("root/record[position()=1]")
                        )
                    ;
                else
                    DBOre.SelectSingleNode("root")
                        .InsertAfter(cur_timer, exn)
                    ;
            }

            string base_uri = DBOre.BaseURI;
            if (path.is_win_uri(base_uri))
            {
                base_uri = path.win_uri_to_filepath(base_uri);
            }
            DBOre.Save(base_uri);

            return cur_timer;
        }

        // ATTENZIONE mi eviterei di spostare i record per tappare i buchi...
        public void delete_intervallo(int id_gerarchia, intervallo_model interval)
        {
            XmlDocument xd = Apri_Ultimo(id_gerarchia, interval.inizio);
            string xquery =
                $"root/record[@inizio='{DateTime.FromBinary(interval.inizio).ToString(datetime_format)}'][@fine='{DateTime.FromBinary(interval.fine).ToString(datetime_format)}']"
            ;
            // FEAT qualche try/catch non guasterebbe
            // TODO cerca in archivio
            xd.RemoveChild(xd.SelectSingleNode(xquery));
        }

        public void put_commento(intervallo_model r, string commento)
        {
            r.parser.InnerText = commento;
            Salva(r.parser);
        }

        public bool delete_record_by_id(intervallo_model r)
        {
            var doc = r.parser.OwnerDocument;
            var root = doc["root"];

            try
            {
                root.RemoveChild(r.parser);
                Salva(root);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Ore_Aggiorna_Commento(int id_gerarchia, long t_iniz, long t_fin, string comm)
        {
            XmlDocument xd = Apri(id_gerarchia, t_iniz);
            string xq = $"root/record[@inizio='{DateTime.FromBinary(t_iniz).ToString(datetime_format)}']";
            if (t_fin > 0)
            {
                var what___ = "";

                if (t_fin < 0)
                    what___ = DateTime.FromBinary(t_fin).ToString(datetime_format);

                xq += $"[@fine='{what___}']";
            }
            XmlNode xn = xd.SelectSingleNode(xq);
            if (xn == null)
            {
                return;
            }
            xn.InnerText = comm;
            xd.Save(xd.BaseURI);
        }

        public bool Ore_Completa_Intervallo(int id_gerarchia, long t_iniz, long t_fin, string? comm = null)
        {
            if (t_fin < 0)
            {
                return false;
            }
            XmlDocument xd = Apri(id_gerarchia, t_iniz);
            string xq = $"root/record[@inizio='{DateTime.FromBinary(t_iniz).ToString(datetime_format)}']";
            XmlNode xn = xd.SelectSingleNode(xq);
            if (xn == null)
            {
                return false;
            }
            xn.Attributes["fine"].Value = DateTime.FromBinary(t_fin).ToString(datetime_format); // non voglio per forza impostarlo, mi basta esista l'attributo
            if (comm != null)
                xn.InnerText = comm;

            var suri = xd.BaseURI;
            if (path.is_win_uri(suri))
            {
                suri = path.win_uri_to_filepath(suri);
            }
            xd.Save(suri);

            return true;
        }

        /// <summary>ottiene un elenco di nodi filtrando per tempo d'inizio in [tI,tF]</summary>
        /// <param name="id_gerarchia">id gerarchia</param>
        /// <param name="t_iniz">tempo iniziale</param>
        /// <param name="t_fin">ATTENZIONE: è sempre relativo al tempo d'inizio</param>
        /// <returns>lista di nodi xml, ovviamente potenzialmente appartenenti a diversi file .xml</returns>
        public intervallo_model[] Filtro_Intervallo(int id_gerarchia, long p_iniz, long p_fin)
        {
            var __method = "[database.repo.Filtro_Intervallo]";
            var list = new List<intervallo_model>();
            XmlDocument doc_ore = null;
            DateTime t_iniz = default(DateTime);
            DateTime t_fin = default(DateTime);

            try
            {
                doc_ore = Apri_Ultimo(id_gerarchia, p_iniz);
                t_iniz = DateTime.FromBinary(p_iniz);
                t_fin = DateTime.FromBinary(p_fin);
            }
            catch (Exception load_err)
            {
                var msg = $"{__method} errore caricando ultimo db gerarchia {id_gerarchia}";
                throw new Exception(msg, load_err);
            }

            var records_from_file = new List<intervallo_model>();
            var records_nodes1 = doc_ore.SelectNodes("root/record");

            foreach (XmlNode record_node in records_nodes1)
            {
                try
                {
                    var parser = new xml_field_parser(intervallo_model.XmlFields, record_node);
                    records_from_file.Add(intervallo_model.parse(parser));
                }
                catch (Exception parse_err)
                {
                    var msg = $"{__method} errore db gerarchia '{id_gerarchia}' @ nodo '{record_node.OuterXml}'";
                    throw new Exception(msg, parse_err);
                }
            }

            // questo filtro dovrebbe stare in un posto dedicato
            var filtered_records =
                from record_item
                in records_from_file
                where
                    record_item.inizio >= t_iniz
                    && (
                        !record_item.fine.HasValue
                        ||
                        record_item.fine.Value <= t_fin
                    )
                select record_item
            ;

            intervallo_model last_record; //parse_record(doc_ore.SelectSingleNode("root/record[position()=last()]"))

            if (filtered_records.Count() > 0)
            {
                list = filtered_records.ToList();
                last_record = filtered_records.Last();
            }
            else
                last_record = null;

            // unisce a db successivi
            if (
                last_record == null
                ||
                last_record.inizio <= t_fin
            //&&
            //last_err == ATG_LAST_ERROR.SUCCESS_DB_ORE_APRI_NON_ULTIMO
            )
            {
                string db_path = Path.Combine(_settings.archive_folder, id_gerarchia.ToString());
                int temp;
                // questo int.parse dovrebbe stare in un posto dedicato
                int db_attuale = int.Parse(doc_ore.BaseURI.Split('/').Last().Split('.').First());
                XmlDocument db_temp = null;

                foreach (string sfile in Directory.EnumerateFiles(db_path, @"*.xml"))
                {
                    // idem come sopra, questo parsing dovrebbe stare in un posto dedicato
                    if (Int32.TryParse(sfile.Split('\\').Last().Split('.').First(), out temp))
                    {
                        if (temp > db_attuale)
                        {
                            try
                            {
                                db_temp = new XmlDocument();
                                db_temp.Load(sfile);
                            }
                            catch (Exception load2_err)
                            {
                                var msg = $"{__method} errore caricando xml: '{sfile}'";
                                throw new Exception(msg, load2_err);
                            }

                            // mi sa che sul db temp servirebbe un lock?
                            List<intervallo_model> records_from_temp = new List<intervallo_model>();

                            foreach (XmlNode record_temp in db_temp.SelectNodes("root/record"))
                            {
                                try
                                {
                                    var parser = new xml_field_parser(intervallo_model.XmlFields, record_temp);
                                    records_from_temp.Add(intervallo_model.parse(parser));
                                }
                                catch (Exception temp_err)
                                {
                                    var msg = $"{__method} errore parsando db '{sfile}' @ nodo '{record_temp.OuterXml}'";
                                    throw new Exception(msg, temp_err);
                                }
                            }
                            if (records_from_temp.Where(record_temp => record_temp.inizio > t_fin) != null)
                                break;
                        }
                    }
                }
            }

            return list.ToArray();
        }

        private XmlDocument Apri_Ultimo(int id_gerarch, long datetime_inizio_ticks) => Apri(id_gerarch, datetime_inizio_ticks);

        private XmlDocument Apri(int id_gerarchia, long t_iniz)
        {
            string db_path = Path.Combine(_settings.archive_folder, id_gerarchia.ToString());
            int max = -1;
            XmlDocument xd = new XmlDocument();

            if (!Directory.Exists(db_path))
            {
                return null;
            }

            // fa più di quanto richiesto
            //xd.PreserveWhitespace = false;

            // ATTENZIONE non 100% sicuro
            foreach (string sfile in Directory.EnumerateFiles(db_path, "*.xml"))
            {
                var filename = Path.GetFileNameWithoutExtension(sfile);
                var file_number = 0;

                if (Int32.TryParse(filename, out file_number))
                {
                    if (file_number > max)
                    {
                        max = file_number; // in realtà prendendo la stringa invece di max potrei evitare di pastrocchiare DBOre_path
                    }
                }
            }

            // eliminati controlli di datetime record rispetto a data corrente
            var db_cerca = Path.Combine(db_path, $"{max}.xml");
            xd.Load(db_cerca);

            // non serve più perché non lo metto a false
            //xd.PreserveWhitespace = true;
            return xd;
        }

        public int update_datetime_where_id(intervallo_model r, DateTime iniz, DateTime? fine)
        {
            r.parser.Attributes["inizio"].Value = iniz.ToString(repo.datetime_format);
            r.parser.Attributes["fine"].Value = fine.HasValue ? fine.Value.ToString(repo.datetime_format) : "";

            return Ordina_Salva(r.parser.OwnerDocument);
        }

        public bool restart_intervallo(int gerarchia_id, intervallo_model interval)
        {
            var interv = select_record_where_datetime_and_gerarchia_async(gerarchia_id, interval);

            // non c'è bisogno di ordinea/salva
            //update_datetime_where_id(interv, DateTime.FromBinary(inizio), null);
            interv.parser.Attributes["fine"].Value = "";

            Salva(interv.parser);
            return false;
        }

        private int Ordina_Salva(XmlDocument xd)
        {
            XmlDocument xsorted = new XmlDocument();
            xsorted.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8"" ?><root />");
            XmlElement xesorted = xsorted["root"];

            Func<XmlNode, XmlNode> ossnx = new Func<XmlNode, XmlNode>((xx) =>
            {
                var v1 = xsorted.CreateElement("record");
                var v2 = xsorted.CreateAttribute("inizio");
                v2.Value = xx.Attributes["inizio"].Value;
                v1.Attributes.Append(v2);
                var v3 = xsorted.CreateAttribute("fine");
                v3.Value = xx.Attributes["fine"].Value;
                v1.Attributes.Append(v3);
                v1.InnerText = xx.InnerText;
                return xesorted.AppendChild(v1);
            });

            var lazy_sort = (
                from XmlNode xn
                in xd.SelectNodes("root//record")
                where xn.NodeType == XmlNodeType.Element
                orderby DateTime.Parse(xn.Attributes["inizio"].Value) ascending
                select ossnx(xn)
            );
            var evaluate_count = lazy_sort.Count();

            string buri = xd.BaseURI;
            if (buri.StartsWith("file:///")) buri = buri.Substring(8);
            xsorted.Save(buri);

            return evaluate_count;
        }
        */
    }
}
