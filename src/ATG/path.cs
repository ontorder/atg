﻿namespace AtgCore
{
    public class path {
        static public string win_uri_to_filepath(string uri) {
            return uri
                .Replace(filesystem.windows.uri_prefix, "")
                .Replace('/', '\\')
            ;
        }

        static public bool is_win_uri(string uri) {
            return uri.StartsWith(filesystem.windows.uri_prefix);
        }
    }
}
