﻿namespace AtgCore;

public sealed class timerpage_controller {
    public List<Action<uint, int?>> eventhandler_add_timerbar;
    public List<Action> eventhandler_clean_selection;
    public List<Func<uint, bool>> eventhandler_del_timerbar;
    public List<Action<string, bool>> eventhandler_log;
    public List<Action<TimeSpan>> eventhandler_status;
    public List<Action> eventhandler_timerbar_modifica_intervallo;
    public List<Action> eventhandler_timerbar_play_stop;
    public List<Action> eventhandler_timerbar_switch_selected;
    public readonly atg_controller father;

    private readonly atg_context db;
    private Func<DateTime> get_filter_end;
    private Func<DateTime> get_filter_start;
    private readonly List<timerbar_controller> timerbars;
    private uint timerbars_id;

    public timerpage_controller(atg_controller pFather, atg_context p_db, Func<DateTime> p_get_filter_start, Func<DateTime> p_get_filter_end) {
        father = pFather;
        db = p_db;
        get_filter_end = p_get_filter_end;
        get_filter_start = p_get_filter_start;

        timerbars = new List<timerbar_controller>();
        eventhandler_add_timerbar = new List<Action<uint, int?>>();
        eventhandler_del_timerbar = new List<Func<uint, bool>>();
        eventhandler_log = new List<Action<string, bool>>();
        eventhandler_clean_selection = new List<Action>();
        eventhandler_status = new List<Action<TimeSpan>>();

        eventhandler_timerbar_play_stop = new List<Action>();
        eventhandler_timerbar_switch_selected = new List<Action>();
        eventhandler_timerbar_modifica_intervallo = new List<Action>();
    }

    private timerbar_controller create_timerbar() {
        var tbar = new timerbar_controller(timerbars_id, this, db, get_filter_start, get_filter_end);
        ++timerbars_id;

        tbar.evento_log.Add(raised_event_log);
        tbar.evento_selezione.Add(raise_event_timerbar_switch_selected);
        tbar.evento_playstop.Add(raise_event_timerbar_play_stop);
        tbar.evento_intervalli.Add(raise_event_modifica_intervallo);

        return tbar;
    }

    public async Task add_timerbar_async(int? id_gerarchia, bool exit_if_exists = false) {
        if (id_gerarchia.HasValue && timerbars.Any(tb => tb.gerarchia.HasValue && tb.gerarchia.Value == id_gerarchia.Value)) {
            if (exit_if_exists)
                return;

            raised_event_log($"una timerbar con gerarchia {id_gerarchia} esiste già", true);
        }

        var tbar = create_timerbar();
        timerbars.Add(tbar);

        raise_event_add_timerbar(tbar.my_id, id_gerarchia);

        if (id_gerarchia.HasValue)
            await tbar.set_gerarchia_async(id_gerarchia.Value);
    }

    public TimeSpan get_tot_time() {
        TimeSpan tot_time = new(0);

        var selected = timerbars.Where(tb => tb.selected).ToArray();

        if (selected.Length == 0)
            selected = [.. timerbars];

        foreach (var tb in selected) {
            tot_time += tb.get_tot_time();
        }

        return tot_time;
    }

    public async Task set_filter_async(DateTime t_start, DateTime t_end) {
        var my_ids = timerbars.Select(tbar => tbar.my_id).ToArray();

        foreach (var id in my_ids) {
            if (raise_event_del_timerbar(id))
                timerbars.RemoveAll(tbar => tbar.my_id == id);
        }

        var list_gerarchia = await db.gerarchie.where_async((t_start, t_end));

        foreach (var gerarchia in list_gerarchia) {
            // gestione centralizzata
            var tbar = create_timerbar();
            timerbars.Add(tbar);

            raise_event_add_timerbar(tbar.my_id, gerarchia.gerarchia_id);

            await tbar.set_gerarchia_async(gerarchia.gerarchia_id);
        }
    }

    public timerbar_controller get_timerbar_by_id(uint id) =>
        timerbars.Single(tbar => tbar.my_id == id);

    public int count_timerbar_by_gerarchia(int gerarchia_id) =>
        timerbars.Count(tb => tb.gerarchia.HasValue && tb.gerarchia.Value == gerarchia_id);

    public void clean_selection() {
        foreach (var tb in timerbars) {
            tb.clean_selected();
        }

        raise_event_clean_selection();
    }

    public void delete_selected_timerbars() {
        // TODO non esiste nemmeno l'elimina per la timerbar singola?
        var selected = timerbars.Where(tb => tb.selected).ToArray();

        // è da vedere se funziona o meno
        foreach (var tbar in selected)
            tbar.elimina();
    }

    public void raise_event_add_timerbar(uint timebar_id, int? id_gerarchia) {
        foreach (var @event in eventhandler_add_timerbar) {
            @event(timebar_id, id_gerarchia);
        }
    }

    public bool raise_event_del_timerbar(uint timebar_id) {
        // UNDONE questa cosa non ha senso, non posso fermarmi al primo no -- o tutto o niente; dovrebbe essere tipo una transazione
        foreach (var @event in eventhandler_del_timerbar) {
            if (!@event(timebar_id))
                return false;
        }
        return true;
    }

    private void raised_event_log(string text, bool is_error) {
        raise_event_log(text, is_error);
    }

    private void raise_event_log(string text, bool is_error) {
        foreach (var @event in eventhandler_log)
            @event(text, is_error);
    }

    private void raise_event_clean_selection() {
        foreach (var @event in eventhandler_clean_selection)
            @event();
    }

    private void raise_event_timerbar_switch_selected(bool unused) {
        foreach (var @event in eventhandler_timerbar_switch_selected)
            @event();

        raise_event_status();
    }

    private void raise_event_timerbar_play_stop(bool unused) {
        foreach (var @event in eventhandler_timerbar_play_stop)
            @event();

        raise_event_status();
    }

    private void raise_event_status() {
        var tot_time = get_tot_time();

        foreach (var @event in eventhandler_status)
            @event(tot_time);
    }

    private void raise_event_modifica_intervallo(intervallo_ctx[] unused) {
        foreach (var @event in eventhandler_timerbar_modifica_intervallo)
            @event();

        raise_event_status();
    }
}
