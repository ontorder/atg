namespace AtgCore;

public sealed class controlbrowser : IInitControl {
    private WebBrowser browser;
    private Func<Delegate, object> hostInvoke;
    private List<Action> navigatedCallbacks = new List<Action>();

    public Control Init(Func<Delegate, object> pHostInvoke) {
        pHostInvoke(() => {
            browser = new WebBrowser();
            browser.DocumentCompleted += NavigatedCallback;
        });

        hostInvoke = pHostInvoke;
        return browser;
    }

    public void NavigatedCallback(object sender, WebBrowserDocumentCompletedEventArgs e) {
        foreach (var nc in navigatedCallbacks)
            nc();
    }

    public void AddNavigatedCallback(Action callback) => navigatedCallbacks.Add(callback);

    public void RemoveNavigatedCallback(Action callback) => navigatedCallbacks.Remove(callback);

    public void ClearNavigatedCallbacks() => navigatedCallbacks.Clear();

    public void Navigate(string url) {
        var aNavigate = new Action(() => browser.Navigate(url));
        hostInvoke(aNavigate);
    }

    private void _Navigate(string url) => browser.Navigate(url);

    public DomElement GetElementById(string id) {
        var fGetElById = new Func<object>(() => browser.Document.GetElementById(id));
        var el = (HtmlElement)hostInvoke(fGetElById);
        return new DomElement(el);
    }

    // in teoria IfExpired dovrebbe andare bene
    // ma di project open 4 non mi fido, non sembra funzionare bene
    public void Refresh() => browser.Refresh(WebBrowserRefreshOption.Completely);

    // ------------------

    public ElementsCollection GetElementsByTagName(string name) {
        var fGetElByName = new Func<object>(() => browser.Document.GetElementsByTagName(name));
        var coll = (HtmlElementCollection)hostInvoke(fGetElByName);
        return new ElementsCollection(coll);
    }

    public class ElementsCollection {
        private HtmlElementCollection hec;
        public ElementsCollection(HtmlElementCollection pHec) => hec = pHec;
        public int Count => hec.Count;
        public DomElement Last() => new DomElement(hec[hec.Count - 1]);

        public DomElement GetFirstByName(string name) {
            var elements = hec.GetElementsByName(name);
            if (elements == null) return null;
            if (elements.Count == 0) return null;
            return new DomElement(elements[0]);
        }

        public DomElement GetFirstByClass(string @class) {
            foreach (HtmlElement he in hec) {
                if (he.GetAttribute("class") == @class)
                    return new DomElement(he);
            }

            return null;
        }
    }

    public class DomElement {
        private HtmlElement he;

        public DomElement(HtmlElement pHe) => he = pHe;

        public bool IsNull() => he == null;

        public void SetText(string text) => he.InnerText = text;

        public void Click() => he.InvokeMember("click");

        public string GetOuterHtml() => he.OuterHtml;

        public string GetAttribute(string name) => he.GetAttribute(name);

        public string GetText() => he.InnerText;

        public IEnumerator<DomElement> EnumChildren() {
            foreach (HtmlElement child in he.Children)
                yield return new DomElement(child);
        }

        public DomElement[] GetChildren() => (from HtmlElement child in he.Children select new DomElement(child)).ToArray();
    }
}
